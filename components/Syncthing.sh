
MY_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")") #full path to this dir
source "$MY_DIR/../functions/common-functions.sh"

Syncthing_description(){
    echo 'Open Source Continuous File Synchronization.'
}

Syncthing_packages(){
    [ -n "$DEBUG" ] && set -x
    local MY_NAME=$(basename $(readlink -f "${BASH_SOURCE[0]}") .sh)

    trap "exit_err $LINENO ${MY_NAME} packages" ERR
    trap 'exit_good' 0

    components_variables Syncthing

    echo "deb http://apt.syncthing.net/ syncthing release" > \
        /etc/apt/sources.list.d/syncthing.list
    wget -q -O - https://syncthing.net/release-key.txt |\
        sudo apt-key add - &>/dev/null

    PACKAGES=(
        python-bcrypt
        syncthing
    )
    check_packages ${PACKAGES[@]}
}


Syncthing_install(){
    [ -n "$DEBUG" ] && set -x
    local MY_NAME=$(basename $(readlink -f "${BASH_SOURCE[0]}") .sh)

    trap "exit_err ${LINENO} ${MY_NAME} install" ERR
    trap 'exit_good' 0

    components_variables ${MY_NAME}
    components_logging SC

    echo 0 > "$SC_PROGRESSF" # say that we are at 0%
    echo 7 > "$SC_RVF" # 7 will show on gauge as in-progress

    PACKAGES_TO_INSTALL=($(${MY_NAME}_packages))
    if [ ${#PACKAGES_TO_INSTALL[@]} -gt 0 ];
    then
        apt_get_safe update
        apt_get_safe -y install ${PACKAGES_TO_INSTALL[@]}
    fi

    create_user "${SC_USER}:${SC_USER_UID}" "$SC_DATA"

    etc_default_syncthing=(
        "DAEMON_USER=$SC_USER"
        "\nDAEMON_GROUP=$GLOBAL_PI_GROUP"
        "\nDAEMON_PID=$SC_PIDFILE"
        "\nDAEMON_LOG=$SC_DATA/syncthing.log"
        "\nDAEMON_OPTS=\"--logfile=\$DAEMON_LOG\""
    )
    echo -e "${etc_default_syncthing[*]}" > /etc/default/syncthing

    if which systemctl;
    then
        cp \
            /usr/lib/systemd/user/syncthing.service \
            /etc/systemd/system/syncthing.service
            SED=(
                's;^ExecStart=.*;ExecStart=/usr/bin/syncthing'
                "-no-browser -logflags=0 -logfile=$SC_DATA/syncthing.log"
                ';'
            )
            sed -i -e "${SED[*]}" /etc/systemd/system/syncthing.service

        SC_UNIT_CONF=(
            "\n[Service]"
            "\n\tEnvironmentFile=/etc/default/syncthing"
            "\n\tUser=${SC_USER}"
            "\n\tGroup=${GLOBAL_PI_GROUP}"
        )
        mkdir -p /etc/systemd/system/${MY_NAME,,}.service.d
        echo -e ${SC_UNIT_CONF[*]} > \
            /etc/systemd/system/${MY_NAME,,}.service.d/user-group.conf

        systemctl daemon-reload
    fi

    CONFIG=(
        "\n<configuration version=\"1\">"
        "\n\t<options>"
        "\n\t\t<globalAnnounceEnabled>false</globalAnnounceEnabled>"
        "\n\t\t<localAnnounceEnabled>false</localAnnounceEnabled>"
        "\n\t\t<relaysEnabled>false</relaysEnabled>"
        "\n\t\t<startBrowser>false</startBrowser>"
        "\n\t\t<upnpEnabled>false</upnpEnabled>"
        "\n\t\t<natEnabled>true</natEnabled>"
        "\n\t</options>"
    )

    # verify ssl certificates and enable ssl usage only
    if [ "$SC_SSL_ENABLED" == "on" ];
    then
        unset MESSAGE
        if [ -r "$SC_SSL_CERT" -a -r "$SC_SSL_KEY" ];
        then
            [ -n "$DEBUG" ] && set -x
            PASSWORD=$(python -c "import bcrypt; print bcrypt.hashpw('$SC_GUI_PASSWORD', bcrypt.gensalt());")
            CONFIG+=(
                "\n\t<gui enabled=\"true\" tls=\"true\">"
                "\n\t\t<user>admin</user>"
                "\n\t\t<password>$PASSWORD</password>"
                "\n\t\t<address>${GLOBAL_FQDN}:8384</address>"
                "\n\t</gui>"
            )
            mkdir -p $(dirname $SC_SETTINGS)
            ln -fs $SC_SSL_CERT $(dirname "$SC_SETTINGS")/https-cert.pem
            ln -fs $SC_SSL_KEY $(dirname "$SC_SETTINGS")/https-key.pem
        else # key or certificate is not readeable
            [ ! -r "$SC_SSL_CERT" ] && \
                MESSAGE=("Certificate file ($SC_SSL_CERT) is not readeable.")
            [ ! -r "$SC_SSL_KEY" ] && \
                MESSAGE=("Certificate key ($SC_SSL_CERT) is not readable.")
        fi

        # SSL was not enabled, message the user
        if [ -n "${MESSAGE[0]}" ];
        then
            MESSAGE+=("Switching off SSL for Syncthing.")
            SC_SSL_ENABLED="off"

            #simple_message_dialog \
            #    'SSL enable' 'piMediaServer -> CouchPotato' ${MESSAGE[*]}
            #return 1
        fi
    fi

    CONFIG+=("\n</configuration>")
    mkdir -p $(dirname "$SC_SETTINGS")
    chown -R "$SC_USER" "$SC_DATA"
    echo -e "${CONFIG[*]}" > "$SC_SETTINGS"

    # update firewall rules if desired
    if [ "$SC_FIREWALL_ENABLED" == "on" ];
    then
        PORT=8384
        [ "$SC_SSL_ENABLED" == "on" ] && PORT=8384
        create_fw_rules_for_ufw Syncthing $PORT
    fi

    if which systemctl;
    then
        systemctl start ${MY_NAME,,}
        systemctl status ${MY_NAME,,}
    else
        service ${MY_NAME,,} start &>/dev/null
        service ${MY_NAME,,} status
    fi

    # check that syncthing serves pages
    [ "$SC_SSL_ENABLED" == "on" ] \
        && ADDR="https://${GLOBAL_FQDN}:8384" \
        || ADDR="http://${GLOBAL_FQDN}:8384"

    export SUCCESS=0
    ADD_OPTIONS="-u $SC_GUI_USER:$SC_GUI_PASSWORD" \
        verify_component_is_running "$ADDR"

    if [ "$SUCCESS" == "1" ];
    then
        LOG_STRING="${MY_NAME} verified running on address: $ADDR "
        LOG_STRING+="with ${SC_GUI_USER}/${SC_GUI_PASSWORD}"
        log_result $LOG_STRING
        echo 3 > "$SC_RVF" # 3 will show on gauge as completed
    else
        echo Something broke.
        echo 1 > "${SC_RVF}" # 1 will show on gauge as failed
    fi

    if which systemctl;
    then
        systemctl stop ${MY_NAME,,}
        systemctl disable ${MY_NAME,,}
    else
        stop ${MY_NAME,,} &>/dev/null
        update-rc.d ${MY_NAME,,} defaults >/dev/null
    fi

    echo 100 > "$SC_PROGRESSF" # say that we are at 100%
}
