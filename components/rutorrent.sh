
MY_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")") #full path to this dir
source "$MY_DIR/../functions/common-functions.sh"

 #we need these or else
source "${MY_DIR}/lighttpd.sh"
source "${MY_DIR}/rtorrent.sh"

rutorrent_description(){
    echo 'Yet another web front-end for rTorrent'
}

rutorrent_packages(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO rutorrent packages' ERR
    trap 'exit_good' 0

    components_variables rutorrent

    PACKAGES=(
        ffmpeg
        mediainfo
        rar
        unrar-free
        unzip
        zip
    )
    PACKAGES+=($(lighttpd_packages))
    PACKAGES+=($(rtorrent_packages))

    check_packages ${PACKAGES[@]}
}

rutorrent_install(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO rutorrent install' ERR
    trap 'exit_good' 0

    components_variables rutorrent
    components_logging RUT
    echo 0 > "$RUT_PROGRESSF" # say that we are at 0%
    echo 7 > "$RUT_RVF" # 7 will show on gauge as in-progress

    # download and configure rutorrent
    [ -d "$RUT_GITCLONE" ] || \
        git clone https://github.com/Novik/ruTorrent "$RUT_GITCLONE"

    # more pluginza
    [ -d "${RUT_GITCLONE}/3rdparty-plugins" ] || \
        git clone https://github.com/nelu/rutorrent-thirdparty-plugins "${RUT_GITCLONE}/3rdparty-plugins"
    ln -fs \
        ${RUT_GITCLONE}/3rdparty-plugins/filemanager \
        ${RUT_GITCLONE}/3rdparty-plugins/fileshare \
        ${RUT_GITCLONE}/plugins/

    # new themes
    [ -d "${RUT_GITCLONE}/themes/FlatUI" ] || \
        git clone https://github.com/exetico/FlatUI "${RUT_GITCLONE}/themes/FlatUI"
    ln -fs \
        ${RUT_GITCLONE}/themes/FlatUI/FlatUI_* \
        "${RUT_GITCLONE}/plugins/theme/themes"/

    chown -R "$RUT_USER" "$RUT_GITCLONE"
    chmod 750 "$RUT_GITCLONE"

    chown -R "$RUT_WEBSERVER_USER" "${RUT_GITCLONE}/share/users"

    # adjust config
    REPLACES=(
        "s;\$log_file =.*;\$log = \"$RUT_DATA/logs\"\;;"
    )

    if [ "$RUT_SCGI_TYPE" == "socket" ];
    then
        REPLACES+=(
            "s;\$scgi_port = .*;\$scgi_port = 0;"
            "s;\$scgi_host = .*;\$scgi_host = \"unix://$RT_SOCKET\"\;;"
        )
    else
        REPLACES+=(
            "s;\$scgi_port = .*;\$scgi_port = $RUT_SCGI_IP_PORT\;;"
            "s;\$scgi_host = .*;\$scgi_host = \"127.0.0.1\"\;;"
        )
    fi
    for REPLACE in "${REPLACES[@]}"; do sed -i "$REPLACE" "$RUT_SETTINGS"; done

    lighttpd_install
    rtorrent_install

    [ -n "$DEBUG" ] && set -x
    if dpkg -s lighttpd;
    then

        if dpkg -s lighttpd &>/dev/null;
        then
            CONFIG_RUT_AUTH=(
                "\nalias.url += ( \"/rutorrent\" => \"$RUT_GITCLONE\" )"
                "\n#url.access-deny = ( \".passwd\", \"logs\" )"
                "\n"
                "\n\$HTTP[\"url\"] =~ \"^/rutorrent\" {"
                "\n\tauth.backend = \"htdigest\""
                "\n\tauth.backend.htdigest.userfile = \"$RUT_GITCLONE/.passwd\""
                "\n\tauth.require = ("
                "\n\t\t\"/rutorrent/\" => ("
                "\n\t\t\t\"method\" => \"basic\","
                "\n\t\t\t\"realm\" => \"rutorrent\","
                "\n\t\t\t\"require\" => \"valid-user\""
                "\n\t\t)"
                "\n\t)"
                "\n}"
            )
            echo -e "${CONFIG_RUT_AUTH[@]}" > /etc/lighttpd/conf-available/99-rutorrent.conf

            # create passwd file for admin authentication
            hash=$(echo -n "${RUT_GUI_USER}:rutorrent:$RUT_GUI_PASSWORD" | md5sum | cut -b -32)
            echo "${RUT_GUI_USER}:rutorrent:$hash" > ${RUT_GITCLONE}/.passwd
            chown ${RUT_WEBSERVER_USER} ${RUT_GITCLONE}/.passwd
            setfacl -m u:"$RUT_WEBSERVER_USER":--x ${RUT_GITCLONE}

            chmod 700 ${RUT_GITCLONE}/.passwd

            #link conf-available to conf-enabled
            for MOD in rutorrent auth;
            do
                lighttpd-enable-mod $MOD || rv=$? && rv=$?
                [[ $rv -eq 0 ]] || [[ $rv -eq 2 ]]
            done
        fi
    else
        echo "lighttpd must be installed prior to rutorrent."
        exit 1
    fi

    if dpkg -s lighttpd &>/dev/null;
    then
        systemctl restart lighttpd
        systemctl is-active lighttpd
    else
        service lighttpd reload
        service lighttpd status #returns 0 when running
    fi

    # check that rutorrent serves pages
    [ "$LIGHT_SSL_ENABLED" == "on" ] \
        && ADDR="https://${GLOBAL_FQDN}/rutorrent/" \
        || ADDR="http://${GLOBAL_FQDN}/rutorrent/"

    ADD_OPTIONS="-u $RUT_GUI_USER:$RUT_GUI_PASSWORD" \
        verify_component_is_running "$ADDR"
    if [ "$SUCCESS" == "1" ];
    then
        # change plugins.dat so we do not have to click that much in da gui
        if [ -f "${MY_DIR}"/../scripts/rutorrent-share-users-admin-settings-plugins.dat ];
        then
            mkdir -p "$RUT_GITCLONE"/share/users/admin/{settings,torrents}
            cp -v \
                "${MY_DIR}"/../scripts/rutorrent/* \
                "$RUT_GITCLONE"/share/users/admin/settings/
            chown -R "${RUT_WEBSERVER_USER}" "$RUT_GITCLONE"/share/users/admin/
        fi
        LOG_STRING="rutorrent verified running on address: "
        LOG_STRING+="$ADDR ($RUT_GUI_USER/$RUT_GUI_PASSWORD)"
        log_result $LOG_STRING
        echo 3 > $RUT_RVF # 3 will show on gauge as completed
    else
        echo Something broke.
        echo 1 > $RUT_RVF # 1 will show as failed
    fi
    echo 100 > $RUT_PROGRESSF # say that we are at 100%
}
