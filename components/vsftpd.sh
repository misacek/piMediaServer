#!/bin/bash

MY_DIR=$(dirname $(readlink -f ${BASH_SOURCE[0]})) #full path to this file dir

source "$MY_DIR/../functions/common-functions.sh"
[ -f "$MY_DIR/$vsftpd.sh" ] && source "$MY_DIR/global.sh"
[ -f "$MY_DIR/$vsftpd.sh" ] && source "$MY_DIR/$vsftpd.sh"

vsftpd_description(){
    echo "The \"Very Secure FTPD\" is a *nix FTP Server."
}

vsftpd_packages(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO transmission packages' ERR
    trap 'exit_good' 0

    PACKAGES=(db5.3-util vsftpd)
    check_packages ${PACKAGES[@]}
}

vsftpd_install(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO vsftpd install' ERR
    trap 'exit_good' 0

    components_variables vsftpd
    components_logging VS
    echo 0 > $VS_PROGRESSF # say that we are at 0%
    echo 7 > $VS_RVF # 7 will show on gauge as in-progress

    PACKAGES_TO_INSTALL=($(vsftpd_packages))
    if [ ${#PACKAGES_TO_INSTALL[@]} -gt 0 ];
    then
        apt_get_safe update
        apt_get_safe -y install ${PACKAGES_TO_INSTALL[@]}
    fi

    # check user, group, common group and create where necessary
    create_user "$VS_USER":${VS_USER_UID} "$VS_DATA"

    create_pi_home_structure

    if [ "$VS_USE_XINETD" == "yes" ];
    then
        :
    else
        CONFIG=(
            "\nlisten=YES"
            "\nbackground=YES"
        )
    fi

    # vsftpd.conf: replace default group
    # local_root must be accessible for vsftpd user
    CONFIG+=(
        "\nconnect_from_port_20=YES\n"
        "\nnopriv_user=$VS_USER"
        "\n"
        "\n#to use it behind nat, redirect these two ports and uncomment"
        "\n#pasv_min_port=55022"
        "\n#pasv_max_port=55023"
        "\n"
        "\nlocal_enable=YES"
        "\nguest_enable=YES"
        "\nguest_username=$VS_USER"
        "\npam_service_name=vsftpd-virtual"
        "\nlocal_root=$GLOBAL_PI_HOME"
        "\nchroot_local_user=YES"
        "\nallow_writeable_chroot=YES"
        "\nwrite_enable=YES"
        "\nvirtual_use_local_privs=TRUE"
        "\n"
        "\nhide_ids=YES"
        "\nsecure_chroot_dir=$VS_DATA/empty"
        "\n"
        "\nxferlog_enable=YES"
        "\nvsftpd_log_file=$VS_DATA/vsftpd.log"
    )
    mkdir -p "$VS_DATA/empty"
    chmod 000 "$VS_DATA/empty"

    if [ "$VS_ENABLE_ANONYMOUS" == 'yes' ];
    then
        CONFIG+=(
            "\n"
            "\nanonymous_enable=YES"
            "\nanon_root=$GLOBAL_PI_HOME"
            "\nftp_username=$VS_USER"
        )
    else
        CONFIG+=(
            "\n"
            "\nanonymous_enable=NO"
            "\nftp_username=$VS_USER"
        )
    fi

    # limit access to directory /home/pirate to file specified users

    #create db for pam with admin in $VS_DATA/virtual-users.db
    TEMPFILE=$(mktemp)
    echo -e "$VS_CLIENT_USER\n$VS_CLIENT_PASSWORD\n" > $TEMPFILE
    db5.3_load -T -t hash -f $TEMPFILE $VS_DATA/virtual-users.db
    chmod 600 $VS_DATA/virtual-users.db
    chown vsftpd $VS_DATA/virtual-users.db
    cat $TEMPFILE
    rm -f $TEMPFILE

    PAM_VSFTPD=(
        "\n"
        "\n# echo -e "\$VS_CLIENT_USER\n\$VS_CLIENT_PASSWORD\n" > $TEMPFILE"
        "\n# db5.3_load -T -t hash -f $TEMPFILE $VS_DATA/virtual-users.db"
        "\n# chmod 600 $VS_DATA/virtual-users.db"
        "\n# chown vsftpd $VS_DATA/virtual-users.db"
        "\n#"
        "\n# no db extension (added silently)"
        "\nauth required pam_userdb.so db=$VS_DATA/virtual-users"
        "\naccount required pam_userdb.so db=$VS_DATA/virtual-users"
        "\nsession required pam_loginuid.so"
    )
    [ -f /etc/pam.d/vsftpd ] && rm -f /etc/pam.d/vsftpd
    echo -e "${PAM_VSFTPD[@]}" > /etc/pam.d/vsftpd-virtual

    if [ "$VS_SSL_ENABLED" == "on" ];
    then
        unset MESSAGE
        if [ -r "$VS_SSL_CERT" -a -r "$VS_SSL_KEY" ];
        then
            CONFIG+=(
                "\n"
                "\nssl_enable=YES"
                "\nallow_anon_ssl=YES"
                "\nforce_local_logins_ssl=YES"
                "\nforce_local_data_ssl=NO"
                "\n"
                "\nssl_ciphers=HIGH"
                "\nssl_sslv2=NO"
                "\nssl_sslv3=NO"
                "\nrsa_cert_file=$VS_SSL_CERT"
                "\nrsa_private_key_file=$VS_SSL_KEY"
            )
        else # key or certificate is not readeable
            [ ! -r "$VS_SSL_CERT" ] && \
                MESSAGE=("Certificate file ($VS_SSL_CERT) is not readeable.")
            [ ! -r "$VS_SSL_KEY" ] && \
                MESSAGE=("Certificate key ($VS_SSL_CERT) is not readable.")
        fi

        # SSL was not enabled, message the user
        if [ -n "${MESSAGE[0]}" ];
        then
            MESSAGE+=("Switching off SSL for vsftpd.")
            VS_SSL_ENABLED="off"

            simple_message_dialog \
                'SSL enable' 'piMediaServer -> vsftpd' ${MESSAGE[*]}
            return 1
        fi
    fi

    echo -e "${CONFIG[@]}" > "$VS_SETTINGS"
    chmod 600 "$VS_SETTINGS"

    # The following sed command will remove any trailing space and CR
    # characters from the specified file:
    sed -i 's,\r,,;s, *$,,' "$VS_SETTINGS"

    # update firewall rules if desired
    if [ "$VS_FIREWALL_ENABLED" == "on" ];
    then
        create_fw_rules_for_ufw vsftpd 21
    fi

    if [ "$VS_USE_XINETD" == "yes" ];
    then
        XINETD_CONFIG=(
            "\nservice ftp"
            "\n{"
            "\n\tdisable          = no"
            "\n\tsocket_type      = stream"
            "\n\twait             = no"
            "\n\tuser             = root"
            "\n\tserver           = /usr/sbin/vsftpd"
            "\n\tlog_on_success  += HOST DURATION"
            "\n\tlog_on_failure  += HOST"
            "\n}"
        )
        echo -e "${XINETD_CONFIG[@]}" > /etc/xinetd.d/vsftpd

        if which systemctl;
        then
            systemctl disable vsftpd
            systemctl stop vsftpd

            systemctl restart xinetd
        else
            service vsftpd stop
            update-rc.d disable vsftpd

            service xinetd restart
        fi
    else
        if which systemctl;
        then
            systemctl restart vsftpd
            systemctl is-active vsftpd
        else
            service vsftpd restart
            service status vsftpd
        fi
    fi

    ADDR="ftp://${GLOBAL_FQDN}"
    OPTIONS="-u $VS_CLIENT_USER:$VS_CLIENT_PASSWORD"
    [ "$VS_SSL_ENABLED" == "on" ] && OPTIONS+=" --ftp-ssl-control"

    ADD_OPTIONS=$OPTIONS verify_component_is_running "$ADDR"

    if [ "$SUCCESS" -eq "1" ];
    then
        if [ "$VS_ENABLE_ANONYMOUS" != "no" ];
        then
            OPTIONS="-u ftp:ftp"
            #[ "$VS_SSL_ENABLED" == "on" ] && OPTIONS=" --ftp-ssl-control"
            ADD_OPTIONS=$OPTIONS verify_component_is_running "$ADDR"
        fi

        [ "$VS_USE_XINETD" == "yes" ] || systemctl stop vsftpd

        LOG_STRING="vsftpd verified running"
        [ "$VS_SSL_ENABLED" == "on" ] && LOG_STRING+=", with ssl-protected logins"
        [ "$VS_ENABLE_ANONYMOUS" != "no" ] && LOG_STRING+=", with anonymous access"
        LOG_STRING+="\nvsftpd: ${VS_CLIENT_USER} password is ${VS_CLIENT_PASSWORD}"
        log_result $LOG_STRING

        echo 3 > $VS_RVF # 3 will show on gauge as completed
    else
        echo "Something went wrong"
        echo 1 > $VS_RVF # will show as failed
    fi
    echo 100 > $VS_PROGRESSF # say that we are at 100%
}
