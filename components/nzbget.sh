
MY_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")") #full path to this dir
source "$MY_DIR/../functions/common-functions.sh"

nzbget_description(){
    echo 'Efficient usenet downloader'
}

nzbget_packages(){
    [ -n "$DEBUG" ] && set -x
    local MY_NAME=$(basename $(readlink -f "${BASH_SOURCE[0]}") .sh)

    trap "exit_err $LINENO ${MY_NAME} packages" ERR
    trap 'exit_good' 0

    PACKAGES=(
        unrar-free
        nzbget
    )
    check_packages ${PACKAGES[@]}
}

nzbget_install(){
    [ -n "$DEBUG" ] && set -x
    local MY_NAME=$(basename $(readlink -f "${BASH_SOURCE[0]}") .sh)

    trap "exit_err ${LINENO} ${MY_NAME} install" ERR
    trap 'exit_good' 0

    components_variables ${MY_NAME}
    components_logging NZB

    echo 0 > "$NZB_PROGRESSF" # say that we are at 0%
    echo 7 > "$NZB_RVF" # 7 will show on gauge as in-progress

    PACKAGES_TO_INSTALL=($(${MY_NAME}_packages))
    if [ ${#PACKAGES_TO_INSTALL[@]} -gt 0 ];
    then
        apt_get_safe update
        apt_get_safe -y install ${PACKAGES_TO_INSTALL[@]}
    fi

    create_user "${NZB_USER}:${NZB_USER_UID}" "$NZB_DATA"

    mkdir -p ${NZB_DATA}/logs
    chown "${NZB_USER}:${NZB_USER}" ${NZB_DATA}/logs

    echo 1 > "${NZB_PROGRESSF}"

    # create /etc/sysconfig config
    etc_default_nzb=(
          "NZB_HOME=$NZB_GITCLONE"
        "\nNZB_DATA=$NZB_DATA"
        "\nNZB_USER=$NZB_USER"
        "\nNZB_PIDFILE=$NZB_PIDFILE"
    )
    echo -e "${etc_default_nzb[*]}" > /etc/default/${MY_NAME,,} #lowercase var

    zcat /usr/share/doc/nzbget/examples/nzbget.conf.gz > "$NZB_SETTINGS"
    chown ${NZB_USER} "$NZB_SETTINGS"

    # copy/create init/systemd scripts
    if which systemctl;
    then
        NZB_UNIT=(
            "\n[Unit]"
            "\nDescription=NZBGet Daemon"
            "\nDocumentation=http://nzbget.net/Documentation"
            "\nAfter=network.target"
            "\n"
            "\n[Service]"
            "\nType=forking"
            "\nEnvironmentFile=/etc/default/${MY_NAME,,}"
            "\nExecStart=/usr/bin/nzbget -c ${NZB_SETTINGS} -D"
            "\nExecStop=/usr/bin/nzbget -Q"
            "\nExecReload=/usr/bin/nzbget -O"
            "\nKillMode=process"
            "\nRestart=on-failure"
            "\n"
            "\n[Install]"
            "\nWantedBy=multi-user.target"
        )
        echo -e "${NZB_UNIT[@]}" > /etc/systemd/system/${MY_NAME,,}.service

        NZB_UNIT_CONF=(
            "\n[Service]"
            "\n\tUser=${NZB_USER}"
            "\n\tGroup=${GLOBAL_PI_GROUP}"
        )
        mkdir -p /etc/systemd/system/${MY_NAME,,}.service.d
        echo -e ${NZB_UNIT_CONF[*]} > \
            /etc/systemd/system/${MY_NAME,,}.service.d/user-group.conf

        systemctl daemon-reload
    else
        NZB_UPSTART=(
            "\ndescription \"NZBGet upstart script\""
            "\nauthor \"DominicM @ dominicm.com\""
            "\n# Run as user"
            "\nsetuid nzbget"
            "\n# Run as group"
            "\nsetgid download"
            "\n# When to start the service"
            "\nstart on runlevel [2345]"
            "\n# When to stop the service"
            "\nstop on runlevel [016]"
            "\n# Automatically restart process if crashed"
            "\nrespawn"
            "\n# Start the process"
            "\nscript"
            "\nexec nzbget -D"
            "\nend script"
            "\n# Run before stopping the process"
            "\npre-stop script"
            "\nexec nzbget -Q"
            "\nend script"
        )
        echo -e ${NZB_UPSTART[*]} > /etc/init/nzbget.conf
    fi
    echo 95 > "${NZB_PROGRESSF}"

    # modify the listening address
    REPLACES=(
        "s/^ControlIP.*/ControlIP=0.0.0.0/"
        "s/^ControlUsername.*/ControlUsername=${NZB_GUI_USERNAME}/"
        "s/^ControlPassword.*/ControlPassword=${NZB_GUI_PASSWORD}/"
        "s/^DaemonUsername.*/DaemonUsername=$NZB_USER/"
        "s;^LogFile.*;LogFile=$NZB_DATA/logs/nzbget.log;"
        "s;^LockFile.*;LockFile=$NZB_PIDFILE;"
        "s/^UMask.*/UMask=077/"

        "s;^MainDir=.*;MainDir=$NZB_DATA;"
        "s;^DestDir=.*;DestDir=$GLOBAL_PI_HOME/download/finished;"
        "s;^WebDir=.*;WebDir=/usr/share/nzbget/webui;"
        "s;^InterDir=.*;InterDir=$GLOBAL_PI_HOME/download/in-progress;"
        "s;^NzbDir=.*;NzbDir=$GLOBAL_PI_HOME/blackhole;"
        "s;^QueueDir=.*;QueueDir=$NZB_DATA/queue;"
    )
    for REPLACE in "${REPLACES[@]}"; do sed -i "$REPLACE" "$NZB_SETTINGS"; done

    # verify ssl certificates and enable ssl usage only
    if [ "$NZB_SSL_ENABLED" == "on" ];
    then
        unset MESSAGE
        if [ -r "$NZB_SSL_CERT" -a -r "$NZB_SSL_KEY" ];
        then
            # verify that there is config to modify and change it there
            if [ -r "$NZB_SETTINGS" ];
            then
                REPLACES=(
                    "s/^SecureControl.*/SecureControl=yes/"
                    "s;^SecureCert.*;SecureCert=$NZB_SSL_CERT;"
                    "s;^SecureKey.*;SecureKey=$NZB_SSL_KEY;"
                )
                for REPLACE in "${REPLACES[@]}";
                do
                    sed -i "$REPLACE" "$NZB_SETTINGS"
                done
            else
                MESSAGE=("$NZB_SETTINGS is not readeable")
                NZB_SSL_ENABLED="off"
            fi
        else # key or certificate is not readeable
            [ ! -r "$NZB_SSL_CERT" ] && \
                MESSAGE=("Certificate file ($NZB_SSL_CERT) is not readeable.")
            [ ! -r "$NZB_SSL_KEY" ] && \
                MESSAGE=("Certificate key ($NZB_SSL_CERT) is not readable.")
        fi

        # SSL was not enabled, message the user
        if [ -n "${MESSAGE[0]}" ];
        then
            MESSAGE+=("Switching off SSL for ${MY_NAME}.")
            NZB_SSL_ENABLED="off"

            simple_message_dialog \
                "${MESSAGE[@]}" \
                'SSL enable' \
                'piMediaServer -> ${MY_NAME}'
            return 1
        fi
    fi

    # update firewall rules if desired
    if [ "$NZB_FIREWALL_ENABLED" == "on" ];
    then
        PORT=6789
        [ "$NZB_SSL_ENABLED" == "on" ] && PORT=6791
        create_fw_rules_for_ufw nzbget $PORT
    fi

    if which systemctl;
    then
        systemctl start ${MY_NAME,,}
        systemctl is-active ${MY_NAME,,}
    else
        start ${MY_NAME,,}
        status ${MY_NAME,,}
    fi

    # check that nzbget serves pages
    [ "$NZB_SSL_ENABLED" == "on" ] \
        && ADDR="https://${GLOBAL_FQDN}:6791" \
        || ADDR="http://${GLOBAL_FQDN}:6789"

    export SUCCESS=0
    ADD_OPTIONS="-u $NZB_GUI_USER:$NZB_GUI_PASSWORD" \
        verify_component_is_running "$ADDR"

    if [ "$SUCCESS" == "1" ];
    then
        LOG_STRING="${MY_NAME} verified running on address: $ADDR "
        LOG_STRING+="with ${NZB_GUI_USER}/${NZB_GUI_PASSWORD}"
        log_result $LOG_STRING
        echo 3 > "${NZB_RVF}" # 3 will show on gauge as completed
    else
        echo Something broke.
        echo 1 > "${NZB_RVF}" # 1 will show on gauge as failed
    fi

    if which systemctl;
    then
        systemctl stop ${MY_NAME,,}
        systemctl disable ${MY_NAME,,}
    else
        stop ${MY_NAME,,} &>/dev/null
        update-rc.d ${MY_NAME,,} defaults >/dev/null
    fi

    echo 100 > "${NZB_PROGRESSF}"
    set +x
}
