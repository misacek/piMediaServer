#!/bin/bash

MY_DIR=$(dirname $(readlink -f ${BASH_SOURCE[0]})) #full path to this file dir
source "$MY_DIR/../functions/common-functions.sh"

transmission_description(){
    echo "A fast, easy and free BitTorrent client."
}

transmission_packages(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO transmission packages' ERR
    trap 'exit_good' 0

    # install necessary packages
    PACKAGES=(transmission-daemon transmission-cli)
    check_packages ${PACKAGES[@]}
}

transmission_install(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO transmission install' ERR
    trap 'exit_good' 0

    components_variables transmission
    components_logging TRANS
    echo 0 > $TRANS_PROGRESSF # say that we are at 0%
    echo 7 > $TRANS_RVF # 7 will show on gauge as in-progress

    PACKAGES_TO_INSTALL=($(transmission_packages))
    if [ ${#PACKAGES_TO_INSTALL[@]} -gt 0 ];
    then
        apt_get_safe update
        apt_get_safe -y install ${PACKAGES_TO_INSTALL[@]}
    fi

    # check user, group, common group and create where necessary
    create_user "$TRANS_USER":${TRANS_USER_UID} "$TRANS_DATA"

    [ -d "$GLOBAL_PI_HOME" ] || create_pi_home_structure

    # when there is lighttpd installed create proxy to rpc
    if dpkg -s lighttpd &>/dev/null;
    then
        LIGHTTPD_PROXY=(
            "\n\$HTTP[\"url\"] =~ \"^/transmission/\" {"
            "\n\tproxy.server = ("
            "\n\t\t\"\" => (("
            "\n\t\t\t\"host\" => \"127.0.0.1\","
            "\n\t\t\t\"port\" => 9091"
            "\n\t\t))"
            "\n\t)"
            "\n}"
        )
        echo -e ${LIGHTTPD_PROXY[*]} > \
            "/etc/lighttpd/conf-available/98-transmission-proxy.conf"

        for SERVICE in transmission-proxy proxy;
        do
            ls -1 /etc/lighttpd/conf-enabled | \
            grep -q -- "[0-9]-${SERVICE}\.conf" || \
            lighttpd-enable-mod $SERVICE
        done
        /etc/init.d/lighttpd force-reload
    fi

    ETC_DEFAULT=(
        "\nENABLE_DAEMON=\"1\""
        "\nUSER=\"transmission\""
        "\nOPTIONS=\"--config-dir $(dirname $TRANS_SETTINGS)\""
    )
    echo -e "${ETC_DEFAULT[@]}" > /etc/default/transmission-daemon

    # copy unitfile to /etc/sytemd/system and also configure it if systemd 
    if which systemctl;
    then
        TRANSMISSION_UNIT=(
            "\n[Unit]"
            "\nDescription=A fast, easy and free BitTorrent client."
            "\nAfter=syslog.target network.target"
            "\n"
            "\n[Service]"
            "\nEnvironmentFile=/etc/default/transmission-daemon"
            "\nExecStart=/usr/bin/transmission-daemon -f \$OPTIONS"
            "\n"
            "\nPIDFile=/run/transmission-daemon.pid"
            "\nStandardError=syslog"
            "\n"
            "\n[Install]"
            "\nWantedBy=multi-user.target"
        )
        echo -e ${TRANSMISSION_UNIT[*]} > /etc/systemd/system/transmission.service
        systemctl daemon-reload

        # stop, disable and mask original transmission daemon unit
        if [ -f /lib/systemd/system/transmission-daemon.service ];
        then
            systemctl stop transmission-daemon
            systemctl disable transmission-daemon
            systemctl mask transmission-daemon
        fi

        # create /etc/systemd/system/transmission-daemon.d/user-group.conf
        TRANSMISSION_UNIT_CONF=(
            "\n[Service]"
            "\n\tGroup=${GLOBAL_PI_GROUP}"
            "\n\tUser=${TRANS_USER}"
        )
        mkdir -p /etc/systemd/system/transmission.service.d
        echo -e ${TRANSMISSION_UNIT_CONF[*]} > \
            /etc/systemd/system/transmission.service.d/user-group.conf
    fi

    # transmission must not be running when changing settings.conf
    if which systemctl;
    then
        systemctl stop transmission
    else
        service transmission stop
    fi

    SETTINGS_CONF=(
        "{"
        "\n\t\"alt-speed-down\": 50, "
        "\n\t\"alt-speed-enabled\": false, "
        "\n\t\"alt-speed-time-begin\": 540, "
        "\n\t\"alt-speed-time-day\": 127, "
        "\n\t\"alt-speed-time-enabled\": false, "
        "\n\t\"alt-speed-time-end\": 1020, "
        "\n\t\"alt-speed-up\": 50, "
        "\n\t\"bind-address-ipv4\": \"0.0.0.0\", "
        "\n\t\"bind-address-ipv6\": \"::\", "
        "\n\t\"blocklist-enabled\": false, "
        "\n\t\"blocklist-url\": \"http://www.example.com/blocklist\", "
        "\n\t\"cache-size-mb\": 4, "
        "\n\t\"dht-enabled\": true, "
        "\n\t\"download-dir\": \"${GLOBAL_PI_HOME}/download/finished\", "
        "\n\t\"download-limit\": 100, "
        "\n\t\"download-limit-enabled\": false, "
        "\n\t\"download-queue-enabled\": true, "
        "\n\t\"download-queue-size\": 5, "
        "\n\t\"encryption\": 1, "
        "\n\t\"idle-seeding-limit\": 30, "
        "\n\t\"idle-seeding-limit-enabled\": false, "
        "\n\t\"incomplete-dir\": \"${GLOBAL_PI_HOME}/download/in-progress\", "
        "\n\t\"incomplete-dir-enabled\": true, "
        "\n\t\"lpd-enabled\": false, "
        "\n\t\"max-peers-global\": 240, "
        "\n\t\"message-level\": 2, "
        "\n\t\"peer-congestion-algorithm\": \"\", "
        "\n\t\"peer-id-ttl-hours\": 6, "
        "\n\t\"peer-limit-global\": 200, "
        "\n\t\"peer-limit-per-torrent\": 50, "
        "\n\t\"peer-port\": 51413, "
        "\n\t\"peer-port-random-high\": 65535, "
        "\n\t\"peer-port-random-low\": 49152, "
        "\n\t\"peer-port-random-on-start\": false, "
        "\n\t\"peer-socket-tos\": \"default\", "
        "\n\t\"pex-enabled\": true, "
        "\n\t\"port-forwarding-enabled\": true, "
        "\n\t\"preallocation\": 1, "
        "\n\t\"prefetch-enabled\": 1, "
        "\n\t\"queue-stalled-enabled\": true, "
        "\n\t\"queue-stalled-minutes\": 30, "
        "\n\t\"ratio-limit\": 2, "
        "\n\t\"ratio-limit-enabled\": true, "
        "\n\t\"rename-partial-files\": true, "
        "\n\t\"rpc-authentication-required\": true, "
        "\n\t\"rpc-bind-address\": \"127.0.0.1\", "
        "\n\t\"rpc-enabled\": true, "
        "\n\t\"rpc-username\": \"$TRANS_GUI_USER\", "
        "\n\t\"rpc-password\": \"$TRANS_GUI_PASSWORD\", "
        "\n\t\"rpc-port\": 9091, "
        "\n\t\"rpc-url\": \"/transmission/\", "
        "\n\t\"rpc-whitelist\": \"127.0.0.1,192.168.1.*\", "
        "\n\t\"rpc-whitelist-enabled\": true, "
        "\n\t\"scrape-paused-torrents-enabled\": true, "
        "\n\t\"script-torrent-done-enabled\": false, "
        "\n\t\"script-torrent-done-filename\": \"\", "
        "\n\t\"seed-queue-enabled\": false, "
        "\n\t\"seed-queue-size\": 10, "
        "\n\t\"speed-limit-down\": 100, "
        "\n\t\"speed-limit-down-enabled\": false, "
        "\n\t\"speed-limit-up\": 100, "
        "\n\t\"speed-limit-up-enabled\": false, "
        "\n\t\"start-added-torrents\": true, "
        "\n\t\"trash-original-torrent-files\": false, "
        "\n\t\"umask\": 18, "
        "\n\t\"upload-limit\": 100, "
        "\n\t\"upload-limit-enabled\": false, "
        "\n\t\"upload-slots-per-torrent\": 14, "
        "\n\t\"utp-enabled\": true,"
        "\n\t\"watch-dir\": \"${GLOBAL_PI_HOME}/blackhole\","
        "\n\t\"watch-dir-enabled\": true"
        "\n}"
    )
    mkdir -p $(dirname "$TRANS_SETTINGS")
    echo -e "${SETTINGS_CONF[@]}" > "$TRANS_SETTINGS"

    # apply patch so there is free space on the disk shown next to speed
    # indicators in the gui
    if [ "$TRANS_FREESPACE_PATCH" == "yes" ];
    then
        TRANSMISSION_WEB="/usr/share/transmission/web"

        # check whether index.html is patched and if it is not patch it
        if grep index.html /var/lib/dpkg/info/transmission-common.md5sums | md5sum -c;
        then
            patch --directory=/ -p0 -i ${MY_DIR}/../scripts/transmission-freespace-index-html.patch
        fi

        if [ ! -f /usr/share/transmission/web/javascript/get.js ];
        then
            cp -v ${MY_DIR}/../scripts/get.js /usr/share/transmission/web/javascript/get.js
        fi

        CRON_D=(
            "*/5 * * * * df -h ${GLOBAL_PI_HOME}"
            "--output=avail | tail -n 1 > ${TRANSMISSION_WEB}/df-h.txt"
        )
        echo -e "${CRON_D[@]}" > /etc/cron.d/transmission
    fi
    
    # make sure transmission is running
    if which systemctl;
    then
        systemctl daemon-reload
        systemctl restart transmission
        systemctl is-active transmission
    else
        service transmission-daemon restart
        service transmission-daemon status
    fi

    ADDR='http://localhost:9091/transmission/web'
    if dpkg -s lighttpd &>/dev/null;
    then
        ADDR="http://${GLOBAL_FQDN}:9091/transmission/web"
    fi

    SUCCESS_STRING="409: Conflict" \
    ADD_OPTIONS="-u ${TRANS_GUI_USER}:${TRANS_GUI_PASSWORD}" \
        verify_component_is_running "${ADDR}"

    if which systemctl;
    then
        systemctl stop transmission
    else
        /etc/init.d/transmission-daemon stop
    fi

    if [ "$SUCCESS" -eq "1" ];
    then
        LOG_STRING="transmission verified running on $ADDR "
        LOG_STRING+="with $TRANS_GUI_USER/$TRANS_GUI_PASSWORD"
        log_result $LOG_STRING

        echo 3 > $TRANS_RVF # 3 will show on gauge as completed
    else
        echo "Something went wrong"
        echo 1 > $TRANS_RVF # will show as failed
    fi
    echo 100 > $TRANS_PROGRESSF # say that we are at 100%
}
