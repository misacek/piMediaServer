
MY_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")") #full path to this dir
source "$MY_DIR/../functions/common-functions.sh"

CouchPotatoServer_description(){
    echo "Automatic Movie Downloading via NZBs & Torrents"
}

CouchPotatoServer_packages(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO transmission packages' ERR
    trap 'exit_good' 0

    PACKAGES=(
        python
        python-lxml
        python-openssl
    )
    check_packages ${PACKAGES[@]}
}

CouchPotatoServer_install(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err ${LINENO} CouchPotatoServer install' ERR
    trap 'exit_good' 0

    components_variables CouchPotatoServer CP
    components_logging CP
    echo 0 > "$CP_PROGRESSF" # say that we are at 0%
    echo 7 > "$CP_RVF" # 7 will show on gauge as in-progress

    PACKAGES_TO_INSTALL=($(CouchPotatoServer_packages))
    if [ ${#PACKAGES_TO_INSTALL[@]} -gt 0 ];
    then
        apt_get_safe update
        apt_get_safe -y install ${PACKAGES_TO_INSTALL[@]}
    fi

    create_user "$CP_USER":${CP_USER_UID} "$CP_DATA"
    echo 1 > "${CP_PROGRESSF}"

    # we need python-openssl > 0.15
    VERSION=$(dpkg-query --show --showformat '${Version}' python-openssl)
    if dpkg --compare-versions $VERSION lt 0.15;
    then
        apt_get_safe -y install python-pip
        pip install pyopenssl --upgrade
    fi

    # clone the repo if the directory is not already there
    [ -d "$CP_GITCLONE" ] || \
    git clone \
        http://github.com/RuudBurger/CouchPotatoServer \
        "$CP_GITCLONE"
    chown -R "$CP_USER:$CP_USER" "$CP_GITCLONE"
    echo 90 > "${CP_PROGRESSF}"

    # create /etc/sysconfig config
    etc_default_couchpotato=(
          "CP_HOME=$CP_GITCLONE"
        "\nCP_DATA=$CP_DATA"
        "\nCP_USER=$CP_USER"
        "\nCP_PIDFILE=$CP_PIDFILE"
    )
    echo -e "${etc_default_couchpotato[*]}" > /etc/default/couchpotato

    if which systemctl;
    then
        CP_UNIT=(
            "\n[Unit]"
            "\nDescription=CouchPotato application instance"
            "\nAfter=network.target"
            ""
            "\n[Service]"
            "\nEnvironmentFile=/etc/default/couchpotato"
            "\nExecStart=${CP_GITCLONE}/CouchPotato.py --config_file \${CP_DATA}/settings.conf"
            "\nType=simple"
            ""
            "\n[Install]"
            "\nWantedBy=multi-user.target"
        )
        echo -e "${CP_UNIT[@]}" > /etc/systemd/system/couchpotato.service

        CP_UNIT_CONF=(
            "\n[Service]"
            "\n\tUser=${CP_USER}"
            "\n\tGroup=${GLOBAL_PI_GROUP}"
        )
        mkdir -p /etc/systemd/system/couchpotato.service.d
        echo -e ${CP_UNIT_CONF[*]} > \
            /etc/systemd/system/couchpotato.service.d/user-group.conf

        systemctl daemon-reload
    else

        cp "$CP_GITCLONE/init/ubuntu" /etc/init.d/couchpotato
        chmod +x /etc/init.d/couchpotato
    fi
    echo 95 > "${CP_PROGRESSF}"

    # start cp only to create tho home directory (restart to avoid problem when
    # already running)
    if which systemctl;
    then
        systemctl daemon-reload
        systemctl restart couchpotato
        systemctl is-active couchpotato
    else
        service couchpotato restart >/dev/null
        service couchpotato status
    fi

    # wait 10s for the creation of non-existant settings file which is created
    # on first start
    I=0
    while [ $I -lt 10 ];
    do
        if [ -f "$CP_SETTINGS" ];
        then
            SUCCESS=1
            break
        else
            I=$((I+1))
            sleep 1
        fi
    done
    [ -z "$SUCCESS" ] && return 1 #TODO: message
    echo 97 > "${CP_PROGRESSF}"

    # stop couchpotato before modifying config
    if which systemctl;
    then
        systemctl stop couchpotato
    else
        service couchpotato stop
    fi

    # change settings
    PASSWORD_HASH=$(echo -n "${CP_GUI_PASSWORD}" | md5sum | cut -b -32)
    REPLACES=(
        "s/^show_wizard = .*/show_wizard = 0/"
        "s/^launch_browser = .*/launch_browser = 0/"
        "s/^dark_theme = .*/dark_theme = 1/"
        "s/^username = .*/username = ${CP_GUI_USER}/"
        "s/^password = .*/password = ${PASSWORD_HASH})/"
        "s/^data_dir = .*/data_dir = ${CP_DATA//\//\\\/}/" #${string//substring/replacement}

    )
    for REPLACE in "${REPLACES[@]}"; do sed -i "$REPLACE" "$CP_SETTINGS"; done

    # verify ssl certificates and enable ssl usage only
    if [ "$CP_SSL_ENABLED" == "on" ];
    then
        unset MESSAGE
        if [ -r "$CP_SSL_CERT" -a -r "$CP_SSL_KEY" ];
        then
            if [ -r "$CP_SSL_CHAIN" ];
            then
                # chain and cert might be in the same file so if we have chain
                # and cert we create new file containing both an use it
                cat $CP_SSL_CHAIN $CP_SSL_CERT $CP_DATA/chain+cert.pem
                CP_SSL_CERT=$CP_DATA/chain+cert.pem
            fi
            # verify that there is config to modify and change it there
            if [ -r "$CP_SETTINGS" ];
            then
                # modify the settings to allow ssl access only
                sed -i "s+^ssl_key =.*+ssl_key = $CP_SSL_KEY+" "$CP_SETTINGS"
                sed -i "s+^ssl_cert =.*+ssl_cert = $CP_SSL_CERT+" "$CP_SETTINGS"
            else
                MESSAGE=("$CP_SETTINGS is not readeable")
                CP_SSL_ENABLED="off"
            fi
        else # key or certificate is not readeable
            [ ! -r "$CP_SSL_CERT" ] && \
                MESSAGE=("Certificate file ($CP_SSL_CERT) is not readable.")
            [ ! -r "$CP_SSL_KEY" ] && \
                MESSAGE=("Certificate key ($CP_SSL_CERT) is not readable.")
        fi

        # SSL was not enabled, message the user
        if [ -n "${MESSAGE[0]}" ];
        then
            MESSAGE+=("Switching off SSL for CouchPotatoServer.")
            CP_SSL_ENABLED="off"

            simple_message_dialog \
                "${MESSAGE[@]}" \
                'SSL enable' \
                'piMediaServer -> CouchPotato'
            return 1
        fi
    fi

    # update firewall rules if desired
    if [ "$CP_FIREWALL_ENABLED" == "on" ];
    then
        PORT=5050
        create_fw_rules_for_ufw CouchPotatoServer $PORT
    fi
    echo 98 > "${CP_PROGRESSF}"

    if which systemctl;
    then
        systemctl start couchpotato
        systemctl is-active couchpotato
    else
        service couchpotato start &>/dev/null
        service couchpotato status
    fi

    # check that couchpotato serves pages
    [ "$CP_SSL_ENABLED" == "on" ] \
        && ADDR="https://${GLOBAL_FQDN}:5050" \
        || ADDR="http://${GLOBAL_FQDN}:5050"

    export SUCCESS=0
    verify_component_is_running "$ADDR"

    if [ "$SUCCESS" == "1" ];
    then
        LOG_STRING="CouchPotato verified running on address: $ADDR "
        LOG_STRING+="with ${CP_GUI_USER}/${CP_GUI_PASSWORD}"
        log_result $LOG_STRING
        echo 3 > "${CP_RVF}" # 3 will show on gauge as completed
    else
        echo Something broke.
        echo 1 > "${CP_RVF}" # 1 will show on gauge as failed
    fi

    if which systemctl;
    then
        systemctl stop couchpotato
        systemctl disable couchpotato
    else
        service couchpotato stop &>/dev/null
        update-rc.d couchpotato defaults >/dev/null
    fi

    echo 100 > "${CP_PROGRESSF}"
}
