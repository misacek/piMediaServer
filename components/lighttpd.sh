
MY_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")") #full path to this dir
source "$MY_DIR/../functions/common-functions.sh"

lighttpd_description(){
    echo 'Installs lighttpd with fastcgi and xmlrpc.'
}

lighttpd_packages(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO lighttpdpackages' ERR
    trap 'exit_good' 0

    components_variables lighttpd

    PACKAGES=(
        lighttpd
        php5-cgi php7.0-cgi
        php5-xmlrpc php7.0-xmlrpc
    )
    check_packages ${PACKAGES[@]}
}

lighttpd_install(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO lighttpd install' ERR
    trap 'exit_good' 0

    components_variables lighttpd
    components_logging LIGHT
    echo 0 > "$LIGHT_PROGRESSF" # say that we are at 0%
    echo 7 > "$LIGHT_RVF" # 7 will show on gauge as in-progress

    PACKAGES_TO_INSTALL=($(lighttpd_packages))
    if [ ${#PACKAGES_TO_INSTALL[@]} -gt 0 ];
    then
        apt_get_safe update
        apt_get_safe -y install ${PACKAGES_TO_INSTALL[@]}
    fi

##    # make sure that lighttpd user have read/write on rtorrent socket
##    if [ -f "$RT_SOCKET" ];
##    then
##        chmod 2770 $(dirname "$RT_SOCKET")
##        su - "$LIGHT_HTTPD_USER" -s /bin/bash -c \
##            "[[ -r \"$RT_SOCKET\" ]] || [[ -w \"$RT_SOCKET\" ]]"
##    fi

    # verify ssl certificates and enable ssl usage only
    if [ "$LIGHT_SSL_ENABLED" == "on" ];
    then
        unset MESSAGE
        if [ -r "$LIGHT_SSL_CERT" -a -r "$LIGHT_SSL_KEY" ];
        then
        cat "$LIGHT_SSL_CERT" "$LIGHT_SSL_KEY" > /etc/lighttpd/cert+privkey.pem
        SSL_CONFIG=(
        "\nserver.modules += (\"mod_setenv\")"
            "\n"
            "\n\$SERVER[\"socket\"] == \":443\" {"
            "\n\tssl.engine = \"enable\""
            "\n\t#ssl.ca-file = \"/etc/letsencrypt/live/${GLOBAL_FQDN}/fullchain.pem\""
            "\n\tssl.pemfile = \"/etc/lighttpd/cert+privkey.pem\""
            "\n\tserver.document-root = \"/var/www\""
            "\n"
            "\n\tssl.honor-cipher-order = \"enable\""
            "\n\tssl.cipher-list = \"EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH\""
            "\n"
            "\n\tsetenv.add-response-header = ("
            "\n\t\t\"Strict-Transport-Security\" => \"max-age=63072000; includeSubdomains; preload\","
            "\n\t\t\"X-Frame-Options\" => \"DENY\","
            "\n\t\t\"X-Content-Type-Options\" => \"nosniff\""
            "\n\t)"
            "\n"
            "\n\tssl.use-sslv2 = \"disable\""
            "\n\tssl.use-sslv3 = \"disable\""
            "\n"
            "\n\tssl.dh-file = \"/etc/lighttpd/dhparam.pem\" "
            "\n\tssl.ec-curve = \"secp384r1\""
            "\n"
            "\n\tserver.tag = \"Microsoft-IIS/8.5 X-Powered-By: ASP.NET X-Powered-By: ARR/2.5\""
            "\n}"
        )
        echo -e "${SSL_CONFIG[@]}" > /etc/lighttpd/conf-available/99-ssl.conf

        [ -f /etc/lighttpd/dhparam.pem ] || \
            openssl dhparam -dsaparam -out /etc/lighttpd/dhparam.pem 2048
        lighttpd-enable-mod ssl || :
        lighttpd-enable-mod fastcgi || :
        lighttpd-enable-mod fastcgi-php || :

        else # key or certificate is not readeable
            [ ! -r "$LIGHT_SSL_CERT" ] && \
                MESSAGE=("Certificate file ($LIGHT_SSL_CERT) is not readeable.")
            [ ! -r "$LIGHT_SSL_KEY" ] && \
                MESSAGE=("Certificate key ($LIGHT_SSL_CERT) is not readable.")
        fi

        # SSL was not enabled, message the user
        if [ -n "${MESSAGE[0]}" ];
        then
            MESSAGE+=("Switching off SSL for lighttpd.")
            LIGHT_SSL_ENABLED="off"

            simple_message_dialog \
                "${MESSAGE[*]}" 'SSL enable' 'piMediaServer -> lighttpd' 
            return 1
        fi
    fi

    # update firewall rules if desired
    if [ "$LIGHT_FIREWALL_ENABLED" == "on" ];
    then
        PORT=80
        [ "$LIGHT_SSL_ENABLED" == "on" ] && PORT=443
        create_fw_rules_for_ufw lighttpd $PORT
    fi

    if which systemctl;
    then
        systemctl restart lighttpd
        systemctl is-active lighttpd
    else
        service lighttpd restart
        service lighttpd status #returns 0 when running
    fi

    # check that we can run php
    echo '<?php phpinfo(); ?>' > /var/www/html/info.php
    echo '<?php phpinfo(); ?>' > /var/www/info.php
    echo 'ehlo' > /var/www/index.html

    PROTOCOLS=(http)
    GENERAL_SUCCESS='yes'
    [ "$LIGHT_SSL_ENABLED" == "on" ] && PROTOCOLS+=(https)
    for type in ${PROTOCOLS[@]};
    do
        for address in \
            "$GLOBAL_FQDN/info.php" \
            "$GLOBAL_FQDN";
        do
            verify_component_is_running "${type}://${address}"
            [ "$SUCCESS" -eq "1" ] || GENERAL_SUCCESS='no'
        done
    done
    rm -f /var/www/index.html /var/www/info.php

    if [ "$GENERAL_SUCCESS" -eq "yes" ];
    then
        log_result "lighttpd verified running (php, ${PROTOCOLS[@]})"
        echo 3 > "${LIGHT_RVF}" # 1 will show on gauge as failed
    else
        echo Something broke.
        echo 1 > "${LIGHT_RVF}" # 1 will show on gauge as failed
    fi

    echo 100 > $LIGHT_PROGRESSF # say that we are at 100%
}
