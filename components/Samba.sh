#!/bin/bash

MY_DIR=$(dirname $(readlink -f ${BASH_SOURCE[0]})) #full path to this file dir
source "$MY_DIR/../functions/common-functions.sh"

Samba_description(){
    echo "Connect and share files with a Windows"
}

Samba_packages(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO Samba packages' ERR
    trap 'exit_good' 0

    components_variables Samba SMB

    PACKAGES=(samba smbclient xinetd)
    check_packages ${PACKAGES[@]}
}

Samba_client_side(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO Samba client_side' ERR
    trap 'exit_good' 0

    components_variables Samba SMB
    local ROOT=$(mktemp --directory --tmpdir=/root samba-client-XXXXXXXXXX)
    rm -f /root/samba-client && ln -fs $ROOT /root/samba-client

    local SMB_LOCAL_PORT=1391
    local SMB_CRED_DIR="$ROOT/etc/auto.smb.credentials"
    local SMB_MNT_REMOTE='/mnt/remote/samba'

    local SHARE_NAME=${GLOBAL_PI_HOME##\/} #in case it starts with '/'
    local SMB_REMOTE_SHARE=${SHARE_NAME//\//-}

    # create client config in /root/samba-client-XXXXXXXXXX
    # /etc/xinetd.d/stunnel-client
    # /etc/xinetd.d/samba
    # /etc/auto.smb
    # /etc/auto.master.d/mnt-remote-samba.autofs
    local DIRECTORIES=(
        $ROOT/etc/xinetd.d
        $ROOT/etc/stunnel
        $ROOT/etc/auto.master.d
        $ROOT/home/user/auto.smb.credentials
        $ROOT/home/user/net/${GLOBAL_FQDN}
        $ROOT/${SMB_MNT_REMOTE}
    )
    mkdir -p ${DIRECTORIES[@]}
    ln -fs \
        ${SMB_MNT_REMOTE}/localhost:${SMB_LOCAL_PORT}/${SMB_REMOTE_SHARE} \
        $ROOT/home/user/net/${GLOBAL_FQDN}/${SMB_REMOTE_SHARE}

    echo -e "username=$SMB_CLIENT_USER\npassword=$SMB_CLIENT_PASSWORD" > \
        $ROOT/home/user/auto.smb.credentials/localhost:${SMB_LOCAL_PORT}.${SMB_REMOTE_SHARE}

    # copy scripts/auto.smb to /etc/auto.smb
    if [ -f ${MY_DIR}/../scripts/auto.smb ];
    then
        cp -v ${MY_DIR}/../scripts/auto.smb $ROOT/etc/auto.smb
    fi

    STUNNEL_CLIENT=(
        "\ndebug = 7"
        "\nsocket = l:TCP_NODELAY=1"
        "\nsocket = r:TCP_NODELAY=1"
        "\nciphers = HIGH"
        "\n"
        "\nclient = yes"
        "\nconnect = ${GLOBAL_FQDN}:${SMB_STUNNEL_PORT}"
    )
    echo -e "${STUNNEL_CLIENT[*]}" > $ROOT/etc/stunnel/client-${GLOBAL_FQDN}-samba.conf

    XINETD_STUNNEL=(
        "\nservice stunnel"
        "\n{"
        "\n\tdisable         = no"
        "\n\tport            = ${SMB_LOCAL_PORT}"
        "\n\ttype            = UNLISTED"
        "\n\tflags           = REUSE"
        "\n\tsocket_type     = stream"
        "\n\twait            = no"
        "\n\tuser            = root"
        "\n\tserver          = /usr/bin/stunnel"
        "\n\tserver_args     = /etc/stunnel/client-${GLOBAL_FQDN}-samba.conf"
        "\n}"
    )
    echo -e "${XINETD_STUNNEL[*]}" > $ROOT/etc/xinetd.d/stunnel-client-${GLOBAL_FQDN}-samba

    AUTO_MASTER_D=(
        "${SMB_MNT_REMOTE} /etc/auto.smb --timeout=30"
    )
    FILENAME=${SMB_MNT_REMOTE##\/}
    FILENAME="$ROOT/etc/auto.master.d/${FILENAME//\//-}.auto"
    echo -e "${AUTO_MASTER_D[*]}" > $FILENAME

    set +x
}

Samba_install(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO Samba install' ERR
    trap 'exit_good' 0

    components_variables Samba
    components_logging SMB
    echo 0 > $SMB_PROGRESSF # say that we are at 0%
    echo 7 > $SMB_RVF # 7 will show on gauge as in-progress

    PACKAGES_TO_INSTALL=($(Samba_packages))
    if [ ${#PACKAGES_TO_INSTALL[@]} -gt 0 ];
    then
        apt_get_safe update
        apt_get_safe -y install ${PACKAGES_TO_INSTALL[@]}
    fi

    # check user, group, common group and create where necessary
    create_user "$SMB_USER":${SMB_USER_UID} "$GLOBAL_PI_HOME"

    [ -d "$GLOBAL_PI_HOME" ] || create_pi_home_structure

    # install necessary packages
    PACKAGES=(samba smbclient xinetd)
    [ "$SMB_STUNNEL" == "on" ] && PACKAGES+=(stunnel4)
    apt_get_safe -y install "${PACKAGES[@]}"

    # samba.conf: replace default group

    SMB_CONF=(
        "\n[global]"
        "\n\tnetbios name = $(hostname -s)"
        "\n\tinterfaces = ${GLOBAL_MY_IP}"
        "\n\tserver string = piMediaServer samba"
        "\n\tunix charset = UTF-8"
        "\n\tworkgroup = ${GLOBAL_PI_GROUP}"
        "\n\tbrowseable = yes"
        "\n\tdeadtime = 30"
        "\n"
        "\n\tdomain master = yes"
        "\n\tlocal master = yes"
        "\n\tpreferred master = yes"
        "\n\tencrypt passwords = true"
        "\n\tenable core files = no"
        "\n"
        "\n\tinvalid users = root"
        "\n\tmap to guest = Bad User"
        "\n\tmax protocol = SMB2"
        "\n\tmin receivefile size = 16384"
        "\n\tobey pam restrictions = yes"
        "\n\tos level = 20"
        "\n"
        "\n\tload printers = no"
        "\n\tprinting = bsd"
        "\n\tprintcap name = /dev/null"
        "\n\tdisable spoolss = yes"
        "\n"
        "\n\tsecurity = user"
        "\n\tsmb passwd file = /etc/samba/smbpasswd"
        "\n\tpassdb backend = smbpasswd"
        "\n\tprintable = no"
        "\n"
        "\n\tsocket options = TCP_NODELAY IPTOS_LOWDELAY"
        "\n\tsyslog = 3"
        "\n\tlog file = /var/log/samba.log"
        "\n\tuse sendfile = yes"
        "\n\twriteable = no"
        "\n"
    )

    # limit access to directory /home/pirate to file specified users
    # user for samba must exist in the system this needs to work
    ADD_USER="-a"
    if [ -f "${SMB_SETTINGS}" ];
    then
        grep -q "^${SMB_USER}:" /etc/samba/smbpasswd && unset ADD_USER
    fi

    (echo "$SMB_PASSWORD"; echo "$SMB_PASSWORD") | \
        smbpasswd -s -c "${SMB_SETTINGS}" ${ADD_USER} "$SMB_USER"

    share_name=${GLOBAL_PI_HOME##\/} #in case it starts with '/'
    share_name=${share_name//\//-}
    SMB_CONF_SHARES=(
        "\n[${share_name}]"
        "\n\tpath = ${GLOBAL_PI_HOME}"
        "\n\tvalid users = ${SMB_USER}"
        "\n\tread only = no"
    )

    if [ "$SMB_ENABLE_ANONYMOUS" == 'yes' ];
    then
        SMB_CONF+=(
            "\n\tguest account = nobody"
            "\n\tguest ok = yes"
        )
        SMB_CONF_SHARES+=(
            "\n\tguest ok = yes"
        )
    else
        SMB_CONF+=(
            "\n\tguest ok = no"
        )
        SMB_CONF_SHARES+=(
            "\n\tguest ok = no"
        )
    fi

    if [ "$SMB_STUNNEL" != "on" ];
    then
        SMB_CONF+=("\n\tsmb encrypt = $SMB_ENCRYPTION_MODE")
        echo -e "${SMB_CONF[*]}" > "$SMB_SETTINGS"
        echo -e "${SMB_CONF_SHARES[*]}" >> "$SMB_SETTINGS"
    else
        SMB_CONF_NOENC=(${SMB_CONF[*]})
        SMB_CONF_NOENC+=("\n\tsmb encrypt = auto")
        echo -e "${SMB_CONF_NOENC[*]}" > "${SMB_SETTINGS}-no-encryption"
        echo -e "${SMB_CONF_SHARES[*]}" >> "${SMB_SETTINGS}-no-encryption"

        SMB_CONF+=("\n\tsmb encrypt = $SMB_ENCRYPTION_MODE")
        echo -e "${SMB_CONF[*]}" > "$SMB_SETTINGS"
        echo -e "${SMB_CONF_SHARES[*]}" >> "$SMB_SETTINGS"
    fi

    # update firewall rules if desired
    if [ "$SMB_FIREWALL_ENABLED" == "on" ];
    then
        [ -f /etc/ufw/applications.d/samba ] && rm -f /etc/ufw/applications.d/samba
        if [ $SMB_STUNNEL == "on" ];
        then
            create_fw_rules_for_ufw Samba 139 ${SMB_STUNNEL_PORT}
        else
            create_fw_rules_for_ufw Samba 139
        fi
    fi

    # check whether we want xinetd or not
    if [ "$SMB_XINETD_ENABLED" == "on" ];
    then
        for service in smbd nmbd;
        do
            if which systemctl;
            then
                for action in stop disable mask; 
                do 
                    systemctl $action $service
                done
            else
                service $service stop
                update-rc.d disable $service
            fi
        done

        XINETD_CONF_SAMBA=(
            "\nservice netbios-ssn"
            "\n{"
            "\n\tdisable     = no"
            "\n\tport        = 139"
            "\n\tsocket_type = stream"
            "\n\twait        = no"
            "\n\tuser        = root"
            "\n\tserver      = /usr/sbin/smbd"
        )

        if [ "$SMB_STUNNEL" == "on" ];
        then
            # its not necessary to force encryption in samba config as it is
            #done with stunnel
            XINETD_CONF_SAMBA+=(
                "\n\tserver_args = -s ${SMB_SETTINGS}-no-encryption"
                "\n\tbind = 127.0.0.1"
                "\n}"
            )
            echo -e "${XINETD_CONF_SAMBA[*]}" > /etc/xinetd.d/samba

            STUNNEL_CONF=(
                "\nsyslog = yes"
                "\n# debug    = 7"
                "\n"
                "\nsocket = l:TCP_NODELAY=1"
                "\nsocket = r:TCP_NODELAY=1"
                "\nTIMEOUTidle = 300"
                "\n"
                "\ncert=$SMB_STUNNEL_SSL_CERT"
                "\nkey=$SMB_STUNNEL_SSL_KEY"
                "\nCApath=/etc/ssl/certs"
                "\n"
                "\noptions = NO_SSLv2"
                "\noptions = NO_SSLv3"
                "\n"
                "\nciphers = HIGH"
                "\n"
                "\nexec     = /usr/sbin/smbd"
                "\nexecArgs = /usr/sbin/smbd -s ${SMB_SETTINGS}-no-encryption"
            )
            echo -e "${STUNNEL_CONF[*]}" > \
                /etc/stunnel/stunnel-server-samba-${SMB_STUNNEL_PORT}.conf

            XINETD_CONF_STUNNEL=(
                "\nservice stunnel-samba"
                "\n{"
                "\n\tdisable         = no"
                "\n\tport            = ${SMB_STUNNEL_PORT}"
                "\n\tsocket_type     = stream"
                "\n\ttype            = UNLISTED"
                "\n\twait            = no"
                "\n\tuser            = root"
                "\n\tserver          = /usr/bin/stunnel4"
                "\n\tserver_args     = /etc/stunnel/stunnel-server-samba-${SMB_STUNNEL_PORT}.conf"
                "\n}"
            )
            echo -e "${XINETD_CONF_STUNNEL[*]}" > \
                /etc/xinetd.d/stunnel-server-samba
        else
            XINETD_CONF_SAMBA+=(
                "\n\tserver_args = -s ${SMB_SETTINGS}"
                "\n}"
            )
            echo -e "${XINETD_CONF_SAMBA[*]}" > /etc/xinetd.d/samba
        fi

        if which systemctl;
        then
            systemctl restart xinetd
            systemctl is-active xinetd
        else
            service xinetd restart
            service xinetd status
        fi
    else
        systemctl is-active smbd || systemctl start smbd
        systemctl enable smbd
    fi

    # check that samba works correctly
    [ "$SMB_ENCRYPTION" == "mandatory" ] && ENCR='-e'
    if smbclient ${ENCR} -U ${SMB_USER}\%${SMB_PASSWORD} -L \
        //${GLOBAL_FQDN}/${share_name} | grep ${share_name} -q;
    then
        LOG_STRING="samba share \"$share_name\" accessible"
        [ "$SMB_XINETD_ENABLED" == "on" ] && LOG_STRING+=" using xinetd"
        [ "$SMB_STUNNEL" == "on" ] && LOG_STRING+=" and/or stunnel on port $SMB_STUNNEL_PORT"
        log_result $LOG_STRING

        Samba_client_side
        log_result "Get configuration for client from: /root/samba-client"

        echo 3 > $SMB_RVF # 3 will show on gauge as completed
    else
        echo "Something went wrong"
        echo 1 > $SMB_RVF # will show as failed
    fi
    echo 100 > $SMB_PROGRESSF # say that we are at 100%
}

