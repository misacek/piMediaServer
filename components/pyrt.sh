
MY_DIR=$(dirname $(readlink -f ${BASH_SOURCE[0]})) #full path to this file dir
source "$MY_DIR/../functions/common-functions.sh"

 #we need these or else
source "${MY_DIR}/rtorrent.sh"

pyrt_description(){
    echo "python rtorrent webUI"
}

pyrt_packages(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO pyrt packages' ERR
    trap 'exit_good' 0

    components_variables pyrt PYRT

    # add necessary repositories and their keys
    # do not run apt-get update

    PACKAGES=(
        python-requests
        python-psutil
        python-tornado
    )
    [ "$PYRT_SSL_ENABLED" == "on" ] && PACKAGES+=(ca-certificates)
    check_packages ${PACKAGES[*]}
}

# This file must be named pyrt.sh
pyrt_install(){
    [ -n "$DEBUG" ] && set -x
    local MY_NAME=$(basename $(readlink -f "${BASH_SOURCE[0]}") .sh)

    trap 'exit_err $LINENO pyrt install' ERR
    trap 'exit_good' 0

    components_variables ${MY_NAME}
    components_logging PYRT
    echo 0 > "$PYRT_PROGRESSF" # say that we are at 0%
    echo 7 > "$PYRT_RVF"

    # install necessary packages
    PACKAGES_TO_INSTALL=($(${MY_NAME}_packages))

    if [ ${#PACKAGES_TO_INSTALL[@]} -gt 0 ];
    then
        apt_get_safe update
        apt_get_safe -y install ${PACKAGES_TO_INSTALL[@]}
    fi

    #---#
    rtorrent_install

    # check user, group, common group and create where necessary
    create_user "${PYRT_USER}:${PYRT_USER_UID}" "$PYRT_DATA"

    # clone the repo if the directory is not already there
    [ -d "$PYRT_GITCLONE" ] || \
    git clone \
        https://github.com/mountainpenguin/pyrt \
        "$PYRT_GITCLONE"
    chown -R "$PYRT_USER:$PYRT_USER" "$PYRT_GITCLONE"

    # copy initscript and create /etc/sysconfig/component config
    etc_default_pyrt=(
        "DAEMON_USER=$PYRT_USER"
        "\nDAEMON_GROUP=$GLOBAL_PI_GROUP"
        "\nDAEMON_LOG=$PYRT_DATA/logs/pyrt.log"
        "\nDAEMON_OPTS=\"\""
    )
    echo -e "${etc_default_pyrt[*]}" > /etc/default/${MY_NAME,,}

    if which systemctl;
    then
        PYRT_UNIT=(
            "\n[Unit]"
            "\nDescription=$(${MY_NAME,,}_description)"
            "\nDocumentation=https://github.com/mountainpenguin/pyrt/blob/master/README.md"
            "\nAfter=network.target"
            "\nWants=rtorrent.service"
            "\n"
            "\n[Service]"
            "\nExecStart=${PYRT_GITCLONE}/pyrt start \$DAEMON_OPTS"
            "\nExecStop=${PYRT_GITCLONE}/pyrt stop \$DAEMON_OPTS"
            "\nRestart=on-failure"
            "\n"
            "\n[Install]"
            "\nWantedBy=default.target"
        )
        echo -e "${PYRT_UNIT[*]}" > /etc/systemd/system/${MY_NAME,,}.service

        PYRT_UNIT_CONF=(
            "\n[Service]"
            "\n\tEnvironmentFile=/etc/default/${MY_NAME,,}"
            "\n\tWorkingDirectory=${PYRT_GITCLONE}"
            "\n\tUser=$PYRT_USER"
            "\n\tGroup=$GLOBAL_PI_GROUP"
        )
        mkdir -p /etc/systemd/system/${MY_NAME,,}.service.d
        echo -e ${PYRT_UNIT_CONF[*]} > \
            /etc/systemd/system/${MY_NAME,,}.service.d/user-group.conf

        systemctl daemon-reload
    fi

    # enable cp to run at startup
    PASSWORD=$(python -c "import bcrypt; print bcrypt.hashpw('$PYRT_GUI_PASSWORD', bcrypt.gensalt());")
    CONFIG=(
        "\n{"
        "\n\t#to allow reach the socke use: setfacl -m u:pyrt:rwx $PYRT_RTORRENT_SOCKET"
        "\n\t\"rtorrent_socket\":\"$PYRT_RTORRENT_SOCKET\","
        "\n\t\"port\":$PYRT_PORT,"
        "\n\t\"host\":\"$GLOBAL_FQDN\","
        "\n\t\"root_directory\":\"$GLOBAL_PI_HOME\","
        "\n\t\"logfile\":\"$PYRT_DATA/logs/pyrt.log\","
        "\n\t\"password\":\"$PASSWORD\""
    )

    # verify ssl certificates and enable ssl usage only
    if [ "$PYRT_SSL_ENABLED" == "on" ];
    then
            # verify that there is config to modify and change it there
        unset MESSAGE
        if [ -r "$PYRT_SSL_CERT" -a -r "$PYRT_SSL_KEY" ];
        then
            # modify the settings to allow ssl access only
            CONFIG+=(
                ","
                "\n\t\"ssl_certificate\":\"$PYRT_SSL_CERT\","
                "\n\t\"ssl_private_key\":\"$PYRT_SSL_KEY\","
                "\n\t\"ssl_ca_certs\":\"/etc/ssl/certs/ca-certificates.crt\""
            )
        else # key or certificate is not readeable
            [ ! -r "$SC_SSL_CERT" ] && \
                MESSAGE=("Certificate file ($PYRT_SSL_CERT) is not readeable.")
            [ ! -r "$SC_SSL_KEY" ] && \
                MESSAGE=("Certificate key ($PYRT_SSL_CERT) is not readable.")
        fi
        # SSL was not enabled, message the user
        if [ -n "${MESSAGE[0]}" ];
        then
            MESSAGE+=("Switching off SSL for Syncthing.")
            SC_SSL_ENABLED="off"

            simple_message_dialog \
                "${MESSAGE[@]}"
                'SSL enable' \
                'piMediaServer -> pyrt'
            return 1
        fi
    fi
    CONFIG+=("\n}")

    mkdir -p $(dirname $PYRT_SETTINGS) $PYRT_DATA/logs
    chown $PYRT_USER $(dirname $PYRT_SETTINGS) $PYRT_DATA/logs
    echo -e ${CONFIG[*]} > $PYRT_SETTINGS

    # update firewall rules if desired
    if [ "$PYRT_FIREWALL_ENABLED" == "on" ];
    then
        PORT=$PYRT_PORT
        create_fw_rules_for_ufw pyrt $PORT
    fi

    if which systemctl;
    then
        systemctl start ${MY_NAME,,}
        systemctl status ${MY_NAME,,}
    else
        service ${MY_NAME,,} start &>/dev/null
        service ${MY_NAME,,} status
    fi

    # check that component works correctly serves pages
    [ "$SC_SSL_ENABLED" == "on" ] \
        && ADDR="https://${GLOBAL_FQDN}:$PYRT_PORT" \
        || ADDR="http://${GLOBAL_FQDN}:$PYRT_PORT"

    export SUCCESS=0
    #ADD_OPTIONS="-u $SC_GUI_USER:$SC_GUI_PASSWORD" \
    SUCCESS_STRING='Server: TornadoServer/4.2.1' \
        verify_component_is_running "$ADDR"

    if [ "$SUCCESS" == "1" ];
    then
        LOG_STRING="${MY_NAME} verified started on address: $ADDR "
        #LOG_STRING+="with ${SC_GUI_USER}/${SC_GUI_PASSWORD}"
        log_result $LOG_STRING
        echo 3 > "$PYRT_RVF" # 3 will show on gauge as completed
    else
        echo Something broke.
        echo 1 > "${PYRT_RVF}" # 1 will show on gauge as failed
    fi

    echo 100 > $PYRT_PROGRESSF # say that we are at 100%
}
