
MY_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")") #full path to this dir
source "$MY_DIR/../functions/common-functions.sh"

Syncthing_description(){
    echo 'Open Source Continuous File Synchronization.'
}

Syncthing_install(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO Syncthing install' ERR
    trap 'exit_good' 0

    if [ -f "$MY_DIR/global.env" -a -f "$MY_DIR/Syncthing.env" ];
    then
        source "$MY_DIR/global.env"
        source "$MY_DIR/Syncthing.env"
    else
        unset MESSAGE
        [ ! -r "$MY_DIR/global.env" ] && \
            MESSAGE+=("Unreadeable $MY_DIR/global.env")
        [ ! -r "$MY_DIR/Syncthing.env" ] && \
            MESSAGE+=("Unreadeable $MY_DIR/Syncthing.env")
        MESSAGE+=("Cannot continue.")
        simple_message_dialog \
            "Env file not found"
            "piMediaServer -> Syncthing"
            "${MESSAGE[*]}"
        return
    fi

    SC_LOGFILE=${SC_LOGF:-'/tmp/piMediaServer/syncthing-log'}
    mkdir -p $(dirname $SC_LOGFILE)
    exec >  >(tee -a "$SC_LOGFILE")
    exec 2> >(tee -a "$SC_LOGFILE" >&2)


    echo 0 > "$SC_PROGRESSF" # say that we are at 100%
    echo 7 > "$SC_RVF" # 3 will show on gauge as completed

    create_user "$SC_USER" "$SC_DATA"

    if ! dpkg -s syncthing;
    then
    add-apt-repository "deb http://apt.syncthing.net/ syncthing release"
    wget -O - https://syncthing.net/release-key.txt | sudo apt-key add -
        apt_get_safe update
        apt_get_safe -y install syncthing
    fi

    # copy initscript and create /etc/sysconfig config
    cp "$MY_DIR/../scripts/syncthing-init.ubuntu" /etc/init.d/syncthing
    chmod +x /etc/init.d/syncthing


    etc_default_syncthing=(
        "DAEMON_USER=$SC_USER"
	"\nDAEMON_GROUP=$GLOBAL_PI_GROUP"
        "\nDAEMON_PID=$SC_PIDFILE"
	"\nDAEMON_LOG=$SC_DATA/syncthing.log"
	"\nDAEMON_OPTS=\"--logfile=\$DAEMON_LOG\""
    )
    echo -e "${etc_default_syncthing[*]}" > /etc/default/syncthing

    # enable cp to run at startup
    update-rc.d syncthing defaults >/dev/null

    # start only to create tho home directory (restart to avoid problem when
    # already running)
    #service syncthing start &>/dev/null
    #service syncthing stop &>/dev/null

    CONFIG=(
	"\n<configuration version=\"1\">"
	"\n\t<options>"
	"\n\t\t<globalAnnounceEnabled>false</globalAnnounceEnabled>"
	"\n\t\t<localAnnounceEnabled>false</localAnnounceEnabled>"
	"\n\t\t<relaysEnabled>false</relaysEnabled>"
	"\n\t\t<startBrowser>false</startBrowser>"
	"\n\t\t<upnpEnabled>false</upnpEnabled>"
	"\n\t</options>"
    )

    # verify ssl certificates and enable ssl usage only
    if [ "$SC_SSL_ENABLED" == "on" ];
    then
        unset MESSAGE
        if [ -r "$SC_SSL_CERT" -a -r "$SC_SSL_KEY" ];
        then
            [ -n "$DEBUG" ] && set -x
		#TODO: generate hash that will actually work, now password is password :*
	    PASSWORD=$(python -c "import crypt, getpass, pwd; print crypt.crypt('$SC_GUI_PASSWORD', '\$6\$10\$');")
	    CONFIG+=(
		"\n\t<gui enabled=\"true\" tls=\"true\">"
		"\n\t\t<user>admin</user>"
		"\n\t\t<password>\$2a\$10\$.W38KJ9ayR9bjlKjUIH6/eRu0WV77k8PPj8nyCmCSKdEJCbkcMgI2</password>"
		"\n\t\t<address>127.0.0.1:8384</address>"
		"\n\t</gui>"
	    )
		#"\n\t\t<user>$SC_GUI_USER</user>"
		#"\n\t\t<password>$PASSWORD</password>"
	    ln -fs $SC_SSL_CERT $(dirname "$SC_SETTINGS")/https-cert.pem
	    ln -fs $SC_SSL_KEY $(dirname "$SC_SETTINGS")/https-key.pem
        else # key or certificate is not readeable
            [ ! -r "$SC_SSL_CERT" ] && \
                MESSAGE=("Certificate file ($SC_SSL_CERT) is not readeable.")
            [ ! -r "$SC_SSL_KEY" ] && \
                MESSAGE=("Certificate key ($SC_SSL_CERT) is not readable.")
        fi

        # SSL was not enabled, message the user
        if [ -n "${MESSAGE[0]}" ];
        then
            MESSAGE+=("Switching off SSL for Syncthing.")
            SC_SSL_ENABLED="off"

            #simple_message_dialog \
            #    'SSL enable' 'piMediaServer -> CouchPotato' ${MESSAGE[*]}
            #return 1
        fi
    fi

    CONFIG+=("\n</configuration>")
    mkdir -p $(dirname "$SC_SETTINGS")
    chown -R "$SC_USER" "$SC_DATA"
    echo -e "${CONFIG[*]}" > "$SC_SETTINGS"

    # update firewall rules if desired
    if [ "$SC_FIREWALL_ENABLED" == "on" ];
    then
        PORT=8384
        [ "$SC_SSL_ENABLED" == "on" ] && PORT=8384
        create_fw_rules_for_ufw Syncthing $PORT
    fi

    # take into account all changes made
    service syncthing start &>/dev/null
    service syncthing status

    # check that syncthing serves pages
    [ "$SC_SSL_ENABLED" == "on" ] \
        && ADDR="https://${GLOBAL_FQDN}:8384" \
        || ADDR="http://${GLOBAL_FQDN}:8384"

    export SUCCESS=0
    verify_component_is_running "$ADDR"

    if [ "$SUCCESS" == "1" ];
    then
        echo "Syncthing verified running on address: $ADDR (admin/admin)"
        echo 3 > "$SC_RVF" # 3 will show on gauge as completed
    else
        echo Something broke.
        echo 1 > "${SC_RVF}" # 1 will show on gauge as failed
    fi

    echo 100 > "$SC_PROGRESSF" # say that we are at 100%

    service syncthing stop &>/dev/null
}


Syncthing(){
#--------------------------------------------------------------------------------------------------------------------------------
# Install syncthing
#--------------------------------------------------------------------------------------------------------------------------------
SYNCTHINGUSER=$(whiptail --inputbox "Enter the user to run Syncthing as (usually pi)" 8 78 $SYNCTHINGUSER --title "$SECTION" 3>&1 1>&2 2>&3)
exitstatus=$?; if [ $exitstatus = 1 ]; then exit 1; fi
if ! getent passwd $SYNCTHINGUSER > /dev/null; then
echo "User $SYNCTHINGUSER doesn't exist, exiting, restart the installer"
exit
fi
if !(cat /etc/apt/sources.list.d/syncthing-release.list | grep -q Syncthing > /dev/null);then
cat >> /etc/apt/sources.list.d/syncthing-release.list <<EOF
# Syncthing
deb http://apt.syncthing.net/ syncthing release
EOF
wget -O - https://syncthing.net/release-key.txt | apt-key add -

debconf-apt-progress -- apt-get update
debconf-apt-progress -- apt-get install syncthing -y
sudo -u $SYNCTHINGUSER timeout 120s syncthing
#Make syncthing webui remotely accessible
sed -i "/        <address>127.0.0.1:8384/c\        \<address>0.0.0.0:8384\<\/address\>" /home/$SYNCTHINGUSER/.config/syncthing/config.xml
cd /etc/init.d/
wget https://raw.github.com/blindpet/MediaServerInstaller/usenet/scripts/syncthing
sed -i "/DAEMON_USER=root/c\DAEMON_USER=$SYNCTHINGUSER" /etc/init.d/syncthing
chmod +x /etc/init.d/syncthing
cd /tmp
update-rc.d syncthing defaults
service syncthing start
echo Syncthing is running on $showip:8384
fi
}
