
MY_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")") #full path to this dir
source "$MY_DIR/../functions/common-functions.sh"

piwigo_description(){
    echo 'Piwigo is photo gallery software for the web'
}

piwigo_install(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO piwigo install' ERR
    trap 'exit_good' 0

    if [ -f "$MY_DIR/global.env" -a -f "$MY_DIR/piwigo.env" ];
    then
        source "$MY_DIR/global.env"
        source "$MY_DIR/piwigo.env"
    else
        unset MESSAGE
        [ ! -r "$MY_DIR/global.env" ] && \
            MESSAGE+=("Unreadeable $MY_DIR/global.env")
        [ ! -r "$MY_DIR/piwigo.env" ] && \
            MESSAGE+=("Unreadeable $MY_DIR/piwigo.env")
        MESSAGE+=("Cannot continue.")
        simple_message_dialog \
            "Env file not found"
            "piMediaServer -> piwigo"
            "${MESSAGE[*]}"
        return
    fi

    PIW_LOGFILE=${PIW_LOGF:-'/tmp/piMediaServer/piwigo-log'}
    mkdir -p $(dirname $PIW_LOGFILE)
    exec >  >(tee -a "$PIW_LOGFILE")
    exec 2> >(tee -a "$PIW_LOGFILE" >&2)

    echo 0 > "$PIW_PROGRESSF" # say that we are at 100%
    echo 7 > "$PIW_RVF" # 3 will show on gauge as completed

    [ -d $PIW_DATA ] || mkdir -p $PIW_DATA

    echo mysql-server mysql-server/root_password password root | \
        sudo debconf-set-selections
    echo mysql-server mysql-server/root_password_again password root | \
        sudo debconf-set-selections

    PACKAGES=(lighttpd php5-fpm php5-mysql php5-gd mysql-server unzip)
    TO_INSTALL=()
    for PACKAGE in ${PACKAGES[*]};
    do
        dpkg -s $PACKAGE &>/dev/null || TO_INSTALL+=($PACKAGE)
    done
    if [ ${#TO_INSTALL[*]} -gt 0 ];
    then
        apt_get_safe update
        apt_get_safe -y install ${TO_INSTALL[*]}
        apt_get_safe -y autoremove
    fi

    lighttpd-enable-mod fastcgi || :
    lighttpd-enable-mod fastcgi-php || :

    MYSQL_COMMANDS=(
        "CREATE DATABASE IF NOT EXISTS $PIW_MYSQL_DBNAME;"
        "GRANT ALL PRIVILEGES ON $PIW_MYSQL_DBNAME.* TO '$PIW_MYSQL_USER'@'localhost' \
            IDENTIFIED BY '$PIW_MYSQL_PASSWORD';"
        "FLUSH PRIVILEGES"
    )
    for COMMAND in "${MYSQL_COMMANDS[@]}";
    do
        mysql -u root --password=root -e "$COMMAND"
    done
    mysql -u $PIW_MYSQL_USER --password=$PIW_MYSQL_PASSWORD -h localhost $PIW_MYSQL_DBNAME -e 'show tables;'

    LOW_MEMORY_TUNING=(
        "\n[mysqld]"
        "\ndefault-storage-engine=MyISAM"
        "\nskip-innodb"
        "\n"
        "\nkey_buffer = 128K"
        "\n"
        "\nmax_allowed_packet = 1M"
        "\nnet_buffer_length = 2K"
        "\nread_buffer_size = 256K"
        "\nread_rnd_buffer_size = 256K"
        "\nsort_buffer_size = 64K"
        "\ntable_cache = 4"
        "\nthread_stack = 64K"
    )
    echo -e ${LOW_MEMORY_TUNING[*]} > /etc/mysql/conf.d/low-memory-tuning.conf
    systemctl restart mysql

    # download piwigo and unpack it in PIW_DATA
    wget -O $PIW_DATA/latest.zip http://piwigo.org/download/dlcounter.php?code=latest
    unzip -qod $PIW_DATA/../ $PIW_DATA/latest.zip
    chown -R www-data $PIW_DATA

##    # verify ssl certificates and enable ssl usage only
##    if [ "$PIW_SSL_ENABLED" == "on" ];
##    then
##        unset MESSAGE
##        if [ -r "$PIW_SSL_CERT" -a -r "$PIW_SSL_KEY" ];
##        then
##        cat "$PIW_SSL_CERT" "$PIW_SSL_KEY" > /etc/lighttpd/cert+privkey.pem
##        SSL_CONFIG=(
##            "\n\$SERVER[\"socket\"] == \":443\" {"
##            "\n\tssl.engine = \"enable\""
##            "\n\t#ssl.ca-file = \"/etc/lighttpd/certs/router.misackovo.eu/chain.pem\""
##            "\n\tssl.pemfile = \"/etc/lighttpd/cert+privkey.pem\""
##            "\n\tserver.document-root = \"$PIW_DATA\""
##            "\n"
##            "\n\tssl.honor-cipher-order = \"enable\""
##            "\n\tssl.cipher-list = \"EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH\""
##            "\n"
##            "\n\tsetenv.add-response-header = ("
##            "\n\t\t\"Strict-Transport-Security\" => \"max-age=63072000; includeSubdomains; preload\","
##            "\n\t\t\"X-Frame-Options\" => \"DENY\","
##            "\n\t\t\"X-Content-Type-Options\" => \"nosniff\""
##            "\n\t)"
##            "\n"
##            "\n\tssl.use-sslv2 = \"disable\""
##            "\n\tssl.use-sslv3 = \"disable\""
##            "\n"
##            "\n\tssl.dh-file = \"/etc/lighttpd/dhparam.pem\" "
##            "\n\tssl.ec-curve = \"secp384r1\""
##            "\n"
##            "\n\tserver.tag = \"Microsoft-IIS/8.5 X-Powered-By: ASP.NET X-Powered-By: ARR/2.5\""
##            "\n}"
##        )
##        echo -e "${SSL_CONFIG[@]}" > /etc/lighttpd/conf-available/99-piwigo-ssl.conf
##        [ -f /etc/lighttpd/dhparam.pem ] || \
##            openssl dhparam -out /etc/lighttpd/dhparam.pem 2048
##        lighttpd-enable-mod piwigo-ssl || :
##
##        else # key or certificate is not readeable
##            [ ! -r "$PIW_SSL_CERT" ] && \
##                MESSAGE=("Certificate file ($PIW_SSL_CERT) is not readeable.")
##            [ ! -r "$PIW_SSL_KEY" ] && \
##                MESSAGE=("Certificate key ($PIW_SSL_CERT) is not readable.")
##        fi
##
##        # SSL was not enabled, message the user
##        if [ -n "${MESSAGE[0]}" ];
##        then
##            MESSAGE+=("Switching off SSL for piwigo.")
##            PIW_SSL_ENABLED="off"
##
##            #simple_message_dialog \
##            #    'SSL enable' 'piMediaServer -> CouchPotato' ${MESSAGE[*]}
##            #return 1
##        fi
##    fi

    # update firewall rules if desired
    if [ "$PIW_FIREWALL_ENABLED" == "on" ];
    then
        PORT=80
        [ "$PIW_SSL_ENABLED" == "on" ] && PORT=443
        create_fw_rules_for_ufw piwigo $PORT
    fi

    # take into account all changes made
    service lighttpd restart


    # check that piwigo serves pages
    [ "$PIW_SSL_ENABLED" == "on" ] \
        && ADDR="https://${GLOBAL_FQDN}/piwigo/install.php" \
        || ADDR="http://${GLOBAL_FQDN}/piwigo/install.php"

    export SUCCESS=0
    verify_component_is_running "$ADDR"

    if [ "$SUCCESS" == "1" ];
    then
        echo "piwigo installation running on address: $ADDR"
        echo -n "user: $PIW_MYSQL_USER "
        echo -n "password: $PIW_MYSQL_PASSWORD "
        echo    "database: $PIW_MYSQL_DBNAME"
        echo 3 > "$PIW_RVF" # 3 will show on gauge as completed
    else
        echo Something broke.
        echo 1 > "${PIW_RVF}" # 1 will show on gauge as failed
    fi

    echo 100 > "$PIW_PROGRESSF" # say that we are at 100%

    #service lighttpd stop &>/dev/null
}
