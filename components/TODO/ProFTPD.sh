
MY_DIR=$(dirname $(readlink -f ${BASH_SOURCE[0]})) #full path to this file dir

source "$MY_DIR/../functions/common-functions.sh"
[ -f "$MY_DIR/$ProFTPD.sh" ] && source "$MY_DIR/global.sh"
[ -f "$MY_DIR/$ProFTPD.sh" ] && source "$MY_DIR/$ProFTPD.sh"

ProFTPD_description(){
    echo "Highly configurable GPL-licensed FTP server software"

}

# This file must be named ProFTPD.sh
ProFTPD_install(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO ProFTPD install' ERR
    trap 'exit_good' 0

    LOGFILE=${CMP_LOGFILE:-'/tmp/piMediaServer/proftpd-log'}
    exec >  >(tee -a "$LOGFILE")
    exec 2> >(tee -a "$LOGFILE" >&2)

    if [ -f "$MY_DIR/global.env" -a -f "$MY_DIR/ProFTPD.env" ];
    then
        source "$MY_DIR/global.env"
        source "$MY_DIR/ProFTPD.env"
    else
        unset MESSAGE
        [ ! -r "$MY_DIR/global.env" ] && \
            MESSAGE+=("Unreadeable $MY_DIR/global.env")
        [ ! -r "$MY_DIR/Component.env" ] && \
            MESSAGE+=("Unreadeable $MY_DIR/Component.env")
        MESSAGE+=("Cannot continue.")
        simple_message_dialog \
            "Env file not found"
            "piMediaServer -> Component"
            "${MESSAGE[*]}"
        return
    fi

    #---#

    echo 0 > $PRO_PROGRESSF # say that we are at 0%
    echo 7 > $PRO_RVF # 7 will show on gauge as in-progress

    # check user, group, common group and create where necessary
    create_user "$PRO_USER" "$GLOBAL_PI_HOME"

    # install necessary packages
    apt_get_safe -y install proftpd-basic

    # proftpd.conf: replace default group
    # ServerIdent Off
    REPLACES=(
        "s/Group/$GLOBAL_PI_GROUP/"
    )

    #create password for admin in /etc/proftpd/passwd
    PASSWORD=$(openssl passwd -1 -salt $(head -c 20 /dev/urandom | base64) $PRO_PASSWORD)
    echo "admin:$PASSWORD:2000:2000::$GLOBAL_PI_HOME:/bin/false" > \
        /etc/proftpd/passwd
    chmod 600 /etc/proftpd/passwd
    chown proftpd /etc/proftpd/passwd

    # create anonymous access
    ANONYMOUS_CONF=(
        "\n#ftp, proftpd and anonymous might log with any password and "
        "\n#will have no write access"
        "\n#admin: must use defined password and have full access"
        "\n"
        "\nAuthUserFile /etc/proftpd/passwd"
        "\n<Anonymous $GLOBAL_PI_HOME>"
        "\n\tUser proftpd"
        "\n"
    )
    if [ "$PRO_ENABLE_ANONYMOUS" == 'yes' ];
    then
        ANONYMOUS_CONF+=(
        "\n\tUserAlias anonymous proftpd"
        "\n\tUserAlias ftp proftpd"
        )
    fi
    ANONYMOUS_CONF+=(
        "\n\tUserAlias admin proftd"
        "\n\tRequireValidShell off"
        "\n\tAuthUsingAlias on"
        "\n\tAuthAliasOnly on"
        "\n"
        "\n\t<Directory *>"
        "\n\t\t#http://www.proftpd.org/docs/howto/Limit.html"
        "\n\t\t<Limit WRITE>"
        "\n\t\t\tAllowUser admin"
        "\n\t\t\tDenyAll"
        "\n\t\t</Limit>"
        "\n\t\tAllowRetrieveRestart on"
        "\n\t\tAllowStoreRestart on"
        "\n\t</Directory>"
        "\n</Anonymous>"
    )
    echo -e "${ANONYMOUS_CONF[@]}" > /etc/proftpd/conf.d/anonymous.conf


    if [ "$PRO_SSL_ENABLED" == "on" ];
    then
        unset MESSAGE
        if [ -r "$PRO_SSL_CERT" -a -r "$PRO_SSL_KEY" ];
        then
            TLS_CONF=(
                "\n<IfModule mod_tls.c>"
                "\n\tTLSEngine                on"
                "\n\tTLSLog                   /var/log/proftpd/tls.log"
                "\n\tTLSProtocol              TLSv1.2"
                "\n\tTLSCipherSuite           AES128+EECDH:AES128+EDH"
                "\n\tTLSRequired              auth"
                "\n\tTLSRSACertificateFile    $PRO_SSL_CERT"
                "\n\tTLSRSACertificateKeyFile $PRO_SSL_KEY"
                "\n</IfModule>"
            )
            echo -e "${TLS_CONF[@]}" > /etc/proftpd/conf.d/ssl.conf
        else # key or certificate is not readeable
            [ ! -r "$PRO_SSL_CERT" ] && \
                MESSAGE=("Certificate file ($PRO_SSL_CERT) is not readeable.")
            [ ! -r "$PRO_SSL_KEY" ] && \
                MESSAGE=("Certificate key ($PRO_SSL_CERT) is not readable.")
        fi

        # SSL was not enabled, message the user
        if [ -n "${MESSAGE[0]}" ];
        then
            MESSAGE+=("Switching off SSL for ProFTPD.")
            PRO_SSL_ENABLED="off"

            simple_message_dialog \
                'SSL enable' 'piMediaServer -> ProFTPD' ${MESSAGE[*]}
            return 1
        fi
    else
        rm -f /etc/proftpd/conf.d/ssl.conf
    fi

    # update firewall rules if desired
    if [ "$PRO_FIREWALL_ENABLED" == "on" ];
    then
        create_fw_rules_for_ufw ProFTPD 21
    fi

    # check that proftpd works correctly serves pages
    service proftpd start

    verify_component_is_running "--ftp-ssl-control ftp://${GLOBAL_FQDN}"

    service proftpd stop

    echo 3 > $PRO_RVF # 3 will show on gauge as completed
    echo 100 > $PRO_PROGRESSF # say that we are at 100%

}
