
MY_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")") #full path to this dir
source "$MY_DIR/../functions/common-functions.sh"

Headphones_description(){
    echo 'Automatic music downloader for SABnzbd.'
}

Headphones_install(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO Headphones install' ERR
    trap 'exit_good' 0

    if [ -f "$MY_DIR/global.env" -a -f "$MY_DIR/Headphones.env" ];
    then
        source "$MY_DIR/global.env"
        source "$MY_DIR/Headphones.env"
    else
        unset MESSAGE
        [ ! -r "$MY_DIR/global.env" ] && \
            MESSAGE+=("Unreadeable $MY_DIR/global.env")
        [ ! -r "$MY_DIR/Headphones.env" ] && \
            MESSAGE+=("Unreadeable $MY_DIR/Headphones.env")
        MESSAGE+=("Cannot continue.")
        simple_message_dialog \
            "Env file not found"
            "piMediaServer -> Headphones"
            "${MESSAGE[*]}"
        return
    fi

    HP_LOGFILE=${HP_LOGF:-'/tmp/piMediaServer/headphones-log'}
    exec >  >(tee -a "$HP_LOGFILE")
    exec 2> >(tee -a "$HP_LOGFILE" >&2)

    echo 0 > "$HP_PROGRESSF" # say that we are at 0%
    echo 7 > "$HP_RVF" # 7 will show on gauge as in-progress

    for group in "$GLOBAL_PI_GROUP" "$HP_USER";
    do
        if ! getent group "$group" &>/dev/null;
        then
            groupadd "$group"
            [ -n "VERBOSE" ] && echo "group $group created"
        fi
    done

    if ! getent passwd "$HP_USER" &>/dev/null;
    then
        useradd \
        --comment "Daemon user for Headphones" \
        --system \
        --gid headphones \
        --groups pirate \
        --shell /sbin/nologin \
        --home-dir "$HP_DATA" \
        --create-home \
        "$HP_USER"
        [ -n "VERBOSE" ] && echo "user $HP_USER created"
    fi
    chmod 700 "$HP_DATA"

    create_user "$HP_USER" "$HP_DATA"

    dpkg -s python &>/dev/null || \
        apt_get_safe install -y python &>/dev/null

    # clone the repo if the directory is not already there
    [ -d "$HP_GITCLONE" ] || \
        git clone https://github.com/rembo10/headphones.git "$HP_GITCLONE"
    chown -R "$HP_USER:$HP_USER" "$HP_GITCLONE"

    # copy initscript and create /etc/sysconfig config
    etc_default_headphones=(
          "HP_HOME=$HP_GITCLONE"
        "\nHP_DATA=$HP_DATA"
        "\nHP_USER=$HP_USER"
        "\nHP_PIDFILE=$HP_PIDFILE"
        "\nHP_OPTS=\"--config=$HP_SETTINGS\""
        "\nSSD_OPTS=\"--group=${GLOBAL_PI_GROUP}\""
    )
    echo -e "${etc_default_headphones[*]}" > /etc/default/headphones

    cp "$HP_GITCLONE/init-scripts/init.ubuntu" /etc/init.d/headphones
    chmod +x /etc/init.d/headphones

    # enable cp to run at startup
    update-rc.d headphones defaults >/dev/null

    # start only to create tho home directory (restart to avoid problem when
    # already running)
    service headphones restart &>/dev/null
    service headphones stop &>/dev/null

    # modify the listening address
    REPLACES=(
        "s/http_host = .*/http_host = 0.0.0.0/"
        "s/http_username = .*/http_username = admin/"
        "s/http_password = .*/http_password = admin/"
    )
    for REPLACE in "${REPLACES[@]}"; do sed -i "$REPLACE" "$HP_SETTINGS"; done

    # verify ssl certificates and enable ssl usage only
    if [ "$HP_SSL_ENABLED" == "on" ];
    then
        unset MESSAGE
        if [ -r "$HP_SSL_CERT" -a -r "$HP_SSL_KEY" ];
        then
            # verify that there is config to modify and change it there
            if [ -r "$HP_SETTINGS" ];
            then
                dpkg -s python-openssl &>/dev/null || \
                    apt_get_safe install -y python-openssl &>/dev/null
                # modify the settings to allow ssl access only
                sed -i "s+^https_key =.*+https_key = $HP_SSL_KEY+" "$HP_SETTINGS"
                sed -i "s+^https_cert =.*+https_cert = $HP_SSL_CERT+" "$HP_SETTINGS"
                sed -i "s+^enable_https =.*+enable_https = 1+" "$HP_SETTINGS"
            else
                MESSAGE=("$HP_SETTINGS is not readeable")
                HP_SSL_ENABLED="off"
            fi
        else # key or certificate is not readeable
            [ ! -r "$HP_SSL_CERT" ] && \
                MESSAGE=("Certificate file ($HP_SSL_CERT) is not readeable.")
            [ ! -r "$HP_SSL_KEY" ] && \
                MESSAGE=("Certificate key ($HP_SSL_CERT) is not readable.")
        fi

        # SSL was not enabled, message the user
        if [ -n "${MESSAGE[0]}" ];
        then
            MESSAGE+=("Switching off SSL for Headphones.")
            HP_SSL_ENABLED="off"

            simple_message_dialog \
                'SSL enable' 'piMediaServer -> CouchPotato' ${MESSAGE[*]}
            return 1
        fi
    fi

    # update firewall rules if desired
    if [ "$NZB_FIREWALL_ENABLED" == "on" ];
    then
        create_fw_rules_for_ufw nzbget 8181
    fi

    # take into account all changes made
    service headphones start &>/dev/null

    # check that headphones serves pages
    SUCCESS=0
    [ "$HP_SSL_ENABLED" == "on" ] \
        && ADDR="https://${GLOBAL_FQDN}:8181" \
        || ADDR="http://${GLOBAL_FQDN}:8181"

    export SUCCESS=0
    verify_component_is_running "$ADDR"

    if [ "$SUCCESS" == "1" ];
    then
        echo "Headphones verified running on address: $ADDR (admin/admin)"
        echo 3 > $HP_RVF # 3 will show on gauge as completed
    else
        echo Something broke.
        echo 1 > $HP_RVF # 1 will show as failed
    fi

    service headphones stop &>/dev/null
    echo 100 > $HP_PROGRESSF # say that we are at 100%

}
