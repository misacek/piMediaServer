
MY_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")") #full path to this dir
source "$MY_DIR/../functions/common-functions.sh"

Mylar_description(){
    echo -n "An automated Comic Book downloader (cbr/cbz)."
}

Mylar_install(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO Mylar install' ERR
    trap 'exit_good' 0

    if [ -f "$MY_DIR/global.env" -a -f "$MY_DIR/Mylar.env" ];
    then
        source "$MY_DIR/global.env"
        source "$MY_DIR/Mylar.env"
    else
        unset MESSAGE
        [ ! -r "$MY_DIR/global.env" ] && \
            MESSAGE+=("Unreadeable $MY_DIR/global.env")
        [ ! -r "$MY_DIR/Mylar.env" ] && \
            MESSAGE+=("Unreadeable $MY_DIR/Mylar.env")
        MESSAGE+=("Cannot continue.")
        simple_message_dialog \
            "Env file not found"
            "piMediaServer -> Mylar"
            "${MESSAGE[*]}"
        return
    fi

    ML_LOGFILE=${ML_LOGF:-'/tmp/piMediaServer/mylar-log'}
    exec >  >(tee -a "$ML_LOGFILE")
    exec 2> >(tee -a "$ML_LOGFILE" >&2)

    #--- real action starts below ---#

    echo 0 > "$ML_PROGRESSF" # say that we are at 0%
    echo 7 > "$ML_RVF" # 7 will show on gauge as in-progress

    create_user "$ML_USER" "$ML_DATA"

    dpkg -s python &>/dev/null || \
        apt_get_safe install -y python &>/dev/null

    # clone the repo if the directory is not already there
    [ -d "$ML_GITCLONE" ] || \
        git clone https://github.com/evilhero/mylar -b development /opt/Mylar
    chown -R "$ML_USER:$ML_USER" "$ML_GITCLONE"

    # copy initscript and create /etc/sysconfig config
    etc_default_mylar=(
          "MYLAR_HOME=$ML_GITCLONE"
        "\nMYLAR_DATA=$ML_DATA"
        "\nMYLAR_USER=$ML_USER"
        "\nMYLAR_PIDFILE=$ML_PIDFILE"
        "\nSSD_OPTS=\"--group=${GLOBAL_PI_GROUP}\""
    )
    echo -e "${etc_default_mylar[*]}" > /etc/default/mylar

    cp "$ML_GITCLONE/init-scripts/ubuntu.init.d" /etc/init.d/mylar
    chmod +x /etc/init.d/mylar

    # enable cp to run at startup
    update-rc.d mylar defaults >/dev/null

    # start only to create tho home directory (restart to avoid problem when
    # already running)
    service mylar restart &>/dev/null
    service mylar stop &>/dev/null

    # modify the listening address
    REPLACES=(
        "s/http_host = .*/http_host = 0.0.0.0/"
        "s/http_username = .*/http_username = admin/"
        "s/http_password = .*/http_password = admin/"
        "s/launch_browser = .*/launch_browser = 0/"
    )
    for REPLACE in "${REPLACES[@]}"; do sed -i "$REPLACE" "$ML_SETTINGS"; done

    # verify ssl certificates and enable ssl usage only
    if [ "$ML_SSL_ENABLED" == "on" ];
    then
        unset MESSAGE
        if [ -r "$ML_SSL_CERT" -a -r "$ML_SSL_KEY" ];
        then
            # verify that there is config to modify and change it there
            if [ -r "$ML_SETTINGS" ];
            then
                dpkg -s python-openssl &>/dev/null || \
                    apt_get_safe install -y python-openssl
                # modify the settings to allow ssl access only
                sed -i "s+^https_key =.*+https_key = $ML_SSL_KEY+" "$ML_SETTINGS"
                sed -i "s+^https_cert =.*+https_cert = $ML_SSL_CERT+" "$ML_SETTINGS"
                sed -i "s+^enable_https =.*+enable_https = 1+" "$ML_SETTINGS"
            else
                MESSAGE=("$ML_SETTINGS is not readeable")
                ML_SSL_ENABLED="off"
            fi
        else # key or certificate is not readeable
            [ ! -r "$ML_SSL_CERT" ] && \
                MESSAGE=("Certificate file ($ML_SSL_CERT) is not readeable.")
            [ ! -r "$ML_SSL_KEY" ] && \
                MESSAGE=("Certificate key ($ML_SSL_CERT) is not readable.")
        fi

        # SSL was not enabled, message the user
        if [ -n "${MESSAGE[0]}" ];
        then
            MESSAGE+=("Switching off SSL for Mylar.")
            ML_SSL_ENABLED="off"

            simple_message_dialog \
                "${MESSAGE[*]}" 'SSL enable' 'piMediaServer -> CouchPotato'
        fi
    fi

    # take into account all changes made
    service mylar start &>/dev/null

    # check that mylar serves pages
    [ "$ML_SSL_ENABLED" == "on" ] \
        && ADDR="https://${GLOBAL_FQDN}:8090" \
        || ADDR="http://${GLOBAL_FQDN}:8090"

    export SUCCESS=0
    verify_component_is_running "$ADDR"

    if [ "$SUCCESS" == "1" ];
    then
        echo "Mylar verified running on address: $ADDR (admin/admin)"
        echo 3 > $ML_RVF # 3 will show on gauge as completed
    else
        echo Something broke.
        echo 1 > $ML_RVF # 1 will show on gauge as failed
    fi
    echo 100 > $ML_PROGRESSF # say that we are at 100%

    service mylar stop &>/dev/null
}
