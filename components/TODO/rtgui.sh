
MY_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")") #full path to this dir
source "$MY_DIR/../functions/common-functions.sh"

rtgui_description(){
    echo 'Fork of rtgui for rtorrent 0.9+'
}

rtgui_install(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO rtgui install' ERR
    trap 'exit_good' 0

    if [ -f "$MY_DIR/global.env" -a -f "$MY_DIR/rtgui.env" ];
    then
        source "$MY_DIR/global.env"
        source "$MY_DIR/rtgui.env"
    else
        unset MESSAGE
        [ ! -r "$MY_DIR/global.env" ] && \
            MESSAGE+=("Unreadeable $MY_DIR/global.env")
        [ ! -r "$MY_DIR/rtgui.env" ] && \
            MESSAGE+=("Unreadeable $MY_DIR/rtgui.env")
        MESSAGE+=("Cannot continue.")
        simple_message_dialog \
            "Env file not found"
            "piMediaServer -> rtgui"
            "${MESSAGE[*]}"
        return
    fi

    RTGUI_LOGFILE=${RTGUI_LOGF:-'/tmp/piMediaServer/rtgui-log'}
    exec >  >(tee -a "$RTGUI_LOGFILE")
    exec 2> >(tee -a "$RTGUI_LOGFILE" >&2)

    echo 0 > "$RTGUI_PROGRESSF" # say that we are at 0%
    echo 7 > "$RTGUI_RVF" # 7 will show on gauge as in-progress

    apt_get_safe install -y lighttpd php5-cgi php5-xmlrpc

    # create passwd file for admin authentication
    hash=$(echo -n "admin:rtgui:$RTGUI_ADMIN_PASSWORD" | md5sum | cut -b -32)
    echo "admin:rtgui:$hash" > ${RTGUI_GITCLONE}/passwd

    CONFIG_RTGUI_AUTH=(
        "\nalias.url += ( \"/rtgui\" => \"$RTGUI_GITCLONE\" )"
        "\n"
        "\nauth.backend = \"htdigest\""
        "\nauth.backend.htdigest.userfile = \"$RTGUI_GITCLONE/passwd\""
        "\nauth.require = ("
        "\n\t\"/rtgui/\" => ("
        "\n\t\t\"method\" => \"basic\","
        "\n\t\t\"realm\" => \"rtgui\","
        "\n\t\t\"require\" => \"valid-user\""
        "\n\t)"
        "\n)"
    )
    echo -e "${CONFIG_RTGUI_AUTH[@]}" > /etc/lighttpd/conf-available/99-rtgui.conf

    #link conf-available to conf-enabled
    lighttpd-enable-mod rtgui || rv=$? && rv=$?
    [[ $rv -eq 0 ]] || [[ $rv -eq 2 ]]

    # download and configure rtgui
    [ -d "$RTGUI_GITCLONE" ] || \
        git clone https://github.com/rakshasa/rtgui.git "$RTGUI_GITCLONE"
    chown -R "$RTGUI_USER" "$RTGUI_GITCLONE"

    REPLACES=(
        "s;\$watchdir=.*;\$watchdir=\"$GLOBAL_PI_HOME/blackhole\"\;;"
        "s;\$downloaddir=.*;\$downloaddir=\"$GLOBAL_PI_HOME/download/in-progress\"\;;"
    )
    for REPLACE in "${REPLACES[@]}"; do sed -i "$REPLACE" "$RTGUI_SETTINGS"; done

    service lighttpd reload
    service lighttpd status #returns 0 when running

    # check that rtgui serves pages
    [ "$LIGHT_SSL_ENABLED" == "on" ] \
        && ADDR="-u admin:$RTGUI_ADMIN_PASSWORD https://${GLOBAL_FQDN}/rtgui/" \
        || ADDR="-u admin:$RTGUI_ADMIN_PASSWORD http://${GLOBAL_FQDN}/rtgui/"

    verify_component_is_running "$ADDR"
    if [ "$SUCCESS" == "1" ];
    then
        echo "rtgui verified running on address: $ADDR (admin/$RTGUI_ADMIN_PASSWORD)"
        echo 3 > $RTGUI_RVF # 3 will show on gauge as completed
    else
        echo Something broke.
        echo 1 > $RTGUI_RVF # 1 will show as failed
    fi

    echo 100 > $RTGUI_PROGRESSF # say that we are at 100%
}
