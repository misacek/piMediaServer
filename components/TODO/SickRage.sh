
MY_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")") #full path to this dir
source "$MY_DIR/../functions/common-functions.sh"

SickRage_description(){
    echo "Automatic Video Library Manager for TV Shows."
}

SickRage_packages(){
    [ -n "$DEBUG" ] && set -x
    local MY_NAME=$(basename $(readlink -f "${BASH_SOURCE[0]}") .sh)

    trap "exit_err $LINENO ${MY_NAME} packages" ERR
    trap 'exit_good' 0

    PACKAGES=(
        git
        libffi-dev
        libssl-dev
        libssl-dev
        libxml2
        libxml2-dev
        libxslt1-dev
        libxslt1.1
        python-dev
        python-pip
        unrar-free
    )
    check_packages ${PACKAGES[@]}
}

SickRage_install(){
    [ -n "$DEBUG" ] && set -x
    local MY_NAME=$(basename $(readlink -f "${BASH_SOURCE[0]}") .sh)

    trap "exit_err ${LINENO} ${MY_NAME} install" ERR
    trap 'exit_good' 0


    components_variables ${MY_NAME}
    components_logging SICK

    echo 0 > "$SICK_PROGRESSF" # say that we are at 0%
    echo 7 > "$SICK_RVF" # 7 will show on gauge as in-progress

    PACKAGES_TO_INSTALL=($(${MY_NAME}_packages))
    if [ ${#PACKAGES_TO_INSTALL[@]} -gt 0 ];
    then
        apt_get_safe update
        apt_get_safe -y install ${PACKAGES_TO_INSTALL[@]}
    fi

    create_user "$SICK_USER":${SICK_USER_UID} "$SICK_DATA"
    echo 1 > "${SICK_PROGRESSF}"

    # we need python-openssl > 0.15
    VERSION=$(dpkg-query --show --showformat '${Version}' python-openssl)
    if dpkg --compare-versions $VERSION lt 0.14;
    then
        apt_get_safe -y install python-pip
        pip install pyopenssl --upgrade
    fi

    # clone the repo if the directory is not already there
    [ -d "$SICK_GITCLONE" ] || \
    git clone \
        http://github.com/SickRage/SickRage \
        "$SICK_GITCLONE"
    chown -R "$SICK_USER:$SICK_USER" "$SICK_GITCLONE"
    echo 90 > "${SICK_PROGRESSF}"

    # create /etc/sysconfig config
    etc_default_sickrage=(
          "SICK_HOME=$SICK_GITCLONE"
        "\nSICK_DATA=$SICK_DATA"
        "\nSICK_USER=$SICK_USER"
        "\nSICK_PIDFILE=$SICK_PIDFILE"
    )
    echo -e "${etc_default_sickrage[*]}" > /etc/default/${MY_NAME,,} #lowercase var

    if which systemctl;
    then
        SICK_UNIT=(
            "\n[Unit]"
            "\nDescription=Automatic Video Library Manager for TV Shows"
            "\nAfter=network.target"
            ""
            "\n[Service]"
            "\nEnvironmentFile=/etc/default/${MY_NAME,,}"
            "\nExecStart=${SICK_GITCLONE}/SickBeard.py --config=\${SICK_DATA}/settings.conf"
            "\nType=simple"
            ""
            "\n[Install]"
            "\nWantedBy=multi-user.target"
        )
        echo -e "${SICK_UNIT[@]}" > /etc/systemd/system/${MY_NAME,,}.service

        SICK_UNIT_CONF=(
            "\n[Service]"
            "\n\tUser=${SICK_USER}"
            "\n\tGroup=${GLOBAL_PI_GROUP}"
        )
        mkdir -p /etc/systemd/system/${MY_NAME,,}.service.d
        echo -e ${SICK_UNIT_CONF[*]} > \
            /etc/systemd/system/${MY_NAME,,}.service.d/user-group.conf

        systemctl daemon-reload
    else

        cp "$SICK_GITCLONE/runscripts/init.ubuntu" /etc/init.d/sickrage
        chmod +x /etc/init.d/sickrage
    fi
    echo 95 > "${SICK_PROGRESSF}"

    # start cp only to create tho home directory (restart to avoid problem when
    # already running)
    if which systemctl;
    then
        systemctl daemon-reload
        systemctl restart ${MY_NAME,,}
        systemctl is-active ${MY_NAME,,}
    else
        service ${MY_NAME,,} restart >/dev/null
        service ${MY_NAME,,} status
    fi


    # wait 10s for the creation of non-existant settings file which is created
    # on first start
    I=0
    while [ $I -lt 10 ];
    do
        if [ -f "$SICK_SETTINGS" ];
        then
            SUCCESS=1
            break
        else
            I=$((I+1))
            sleep 1
        fi
    done
    [ -z "$SUCCESS" ] && return 1 #TODO: message
    echo 97 > "${SICK_PROGRESSF}"

    # stop service before modifying config
    if which systemctl;
    then
        systemctl stop ${MY_NAME,,}
    else
        service ${MY_NAME,,} stop
    fi

    # change settings
    PASSWORD_HASH=$(echo -n "${SICK_GUI_PASSWORD}" | md5sum | cut -b -32)
    REPLACES=(
        "s/^show_wizard = .*/show_wizard = 0/"
        "s/^launch_browser = .*/launch_browser = 0/"
        "s/^dark_theme = .*/dark_theme = 1/"
        "s/^web_username = .*/web_username = ${SICK_GUI_USER}/"
        "s/^web_password = .*/web_password = ${SICK_GUI_PASSWORD})/"
        "s/^data_dir = .*/data_dir = ${SICK_DATA//\//\\\/}/" #${string//substring/replacement}

    )
    for REPLACE in "${REPLACES[@]}"; do sed -i "$REPLACE" "$SICK_SETTINGS"; done

    # verify ssl certificates and enable ssl usage only
    if [ "$SICK_SSL_ENABLED" == "on" ];
    then
        unset MESSAGE
        if [ -r "$SICK_SSL_CERT" -a -r "$SICK_SSL_KEY" ];
        then
            if [ -r "$SICK_SSL_CHAIN" ];
            then
                # chain and cert might be in the same file so if we have chain
                # and cert we create new file containing both an use it
                cat $SICK_SSL_CHAIN $SICK_SSL_CERT $SICK_DATA/chain+cert.pem
                SICK_SSL_CERT=$SICK_DATA/chain+cert.pem
            fi
            # verify that there is config to modify and change it there
            if [ -r "$SICK_SETTINGS" ];
            then
                # modify the settings to allow ssl access only
                sed -i "s+^enable_https =.*+enable_https = 1+" "$SICK_SETTINGS"
                sed -i "s+^https_key =.*+https_key = $SICK_SSL_KEY+" "$SICK_SETTINGS"
                sed -i "s+^https_cert =.*+https_cert = $SICK_SSL_CERT+" "$SICK_SETTINGS"
            else
                MESSAGE=("$SICK_SETTINGS is not readeable")
                SICK_SSL_ENABLED="off"
            fi
        else # key or certificate is not readeable
            [ ! -r "$SICK_SSL_CERT" ] && \
                MESSAGE=("Certificate file ($SICK_SSL_CERT) is not readable.")
            [ ! -r "$SICK_SSL_KEY" ] && \
                MESSAGE=("Certificate key ($SICK_SSL_CERT) is not readable.")
        fi

        # SSL was not enabled, message the user
        if [ -n "${MESSAGE[0]}" ];
        then
            MESSAGE+=("Switching off SSL for ${MY_NAME}.")
            SICK_SSL_ENABLED="off"

            simple_message_dialog \
                "${MESSAGE[@]}" \
                'SSL enable' \
                'piMediaServer -> ${MY_NAME}'
            return 1
        fi
    fi

    # update firewall rules if desired
    if [ "$SICK_FIREWALL_ENABLED" == "on" ];
    then
        PORT=8081
        create_fw_rules_for_ufw ${MY_NAME} $PORT
    fi
    echo 98 > "${SICK_PROGRESSF}"

    if which systemctl;
    then
        systemctl start ${MY_NAME,,}
        systemctl is-active ${MY_NAME,,}
    else
        service ${MY_NAME,,} start &>/dev/null
        service ${MY_NAME,,} status
    fi

    # check that ${MY_NAME,,} serves pages
    [ "$SICK_SSL_ENABLED" == "on" ] \
        && ADDR="https://${GLOBAL_FQDN}:8081" \
        || ADDR="http://${GLOBAL_FQDN}:8081"

    export SUCCESS=0
    verify_component_is_running "$ADDR"

    if [ "$SUCCESS" == "1" ];
    then
        LOG_STRING="${MY_NAME} verified running on address: $ADDR "
        LOG_STRING+="with ${SICK_GUI_USER}/${SICK_GUI_PASSWORD}"
        log_result $LOG_STRING
        echo 3 > "${SICK_RVF}" # 3 will show on gauge as completed
    else
        echo Something broke.
        echo 1 > "${SICK_RVF}" # 1 will show on gauge as failed
    fi

    if which systemctl;
    then
        systemctl stop ${MY_NAME,,}
        systemctl disable ${MY_NAME,,}
    else
        service ${MY_NAME,,} stop &>/dev/null
        update-rc.d ${MY_NAME,,} defaults >/dev/null
    fi

    echo 100 > "${SICK_PROGRESSF}"
}
