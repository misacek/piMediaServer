
MY_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")") #full path to this dir
source "$MY_DIR/../functions/common-functions.sh"

nzbget_description(){
    echo 'Efficient usenet downloader'
}

nzbget_install(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO nzbget install' ERR
    trap 'exit_good' 0

    if [ -f "$MY_DIR/global.env" -a -f "$MY_DIR/nzbget.env" ];
    then
        source "$MY_DIR/global.env"
        source "$MY_DIR/nzbget.env"
    else
        unset MESSAGE
        [ ! -r "$MY_DIR/global.env" ] && \
            MESSAGE+=("Unreadeable $MY_DIR/global.env")
        [ ! -r "$MY_DIR/nzbget.env" ] && \
            MESSAGE+=("Unreadeable $MY_DIR/nzbget.env")
        MESSAGE+=("Cannot continue.")
        simple_message_dialog \
            "Env file not found"
            "piMediaServer -> nzbget"
            "${MESSAGE[*]}"
        return
    fi

    NZB_LOGFILE=${NZB_LOGF:-'/tmp/piMediaServer/nzbget-log'}
    exec >  >(tee -a "$NZB_LOGFILE")
    exec 2> >(tee -a "$NZB_LOGFILE" >&2)

    echo 0 > "$NZB_PROGRESSF" # say that we are at 0%
    echo 7 > "$NZB_RVF" # 7 will show on gauge as in-progress

    create_user "$NZB_USER" "$NZB_DATA"

    dpkg -s nzbget &>/dev/null || \
        apt_get_safe install -y nzbget &>/dev/null

    cp "$MY_DIR/../scripts/nzbget-init.ubuntu" /etc/init.d/nzbget
    chmod +x /etc/init.d/nzbget

    update-rc.d nzbget defaults

    zcat /usr/share/doc/nzbget/examples/nzbget.conf.gz > "$NZB_DATA/.nzbget"

    # modify the listening address
    REPLACES=(
        "s/^ControlIP.*/ControlIP=0.0.0.0/"
        "s/^ControlUsername.*/ControlUsername=admin/"
        "s/^ControlPassword.*/ControlPassword=admin/"
        "s/^DaemonUsername.*/DaemonUsername=$NZB_USER/"
        "s;^LogFile.*;LogFile=$NZB_DATA/nzbget.log;"
        "s;^LockFile.*;LockFile=$NZB_PIDFILE;"
        "s/^UMask.*/UMask=077/"

        "s;^MainDir=.*;MainDir=$NZB_DATA;"
        "s;^DestDir=.*;DestDir=$GLOBAL_PI_HOME/download/finished;"
        "s;^InterDir=.*;InterDir=$GLOBAL_PI_HOME/download/in-progress;"
        "s;^NzbDir=.*;NzbDir=$GLOBAL_PI_HOME/blackhole;"
        "s;^QueueDir=.*;QueueDir=$NZB_DATA/queue;"
    )
    for REPLACE in "${REPLACES[@]}"; do sed -i "$REPLACE" "$NZB_SETTINGS"; done

    # verify ssl certificates and enable ssl usage only
    if [ "$NZB_SSL_ENABLED" == "on" ];
    then
        unset MESSAGE
        if [ -r "$NZB_SSL_CERT" -a -r "$NZB_SSL_KEY" ];
        then
            # verify that there is config to modify and change it there
            if [ -r "$NZB_SETTINGS" ];
            then
                REPLACES=(
                    "s/^SecureControl.*/SecureControl=yes/"
                    "s;^SecureCert.*;SecureCert=$NZB_SSL_CERT;"
                    "s;^SecureKey.*;SecureKey=$NZB_SSL_KEY;"
                )
                for REPLACE in "${REPLACES[@]}";
                do
                    sed -i "$REPLACE" "$NZB_SETTINGS"
                done
            else
                MESSAGE=("$NZB_SETTINGS is not readeable")
                NZB_SSL_ENABLED="off"
            fi
        else # key or certificate is not readeable
            [ ! -r "$NZB_SSL_CERT" ] && \
                MESSAGE=("Certificate file ($NZB_SSL_CERT) is not readeable.")
            [ ! -r "$NZB_SSL_KEY" ] && \
                MESSAGE=("Certificate key ($NZB_SSL_CERT) is not readable.")
        fi

        # SSL was not enabled, message the user
        if [ -n "${MESSAGE[0]}" ];
        then
            MESSAGE+=("Switching off SSL for nzbget.")
            NZB_SSL_ENABLED="off"

            simple_message_dialog \
                'SSL enable' 'piMediaServer -> CouchPotato' ${MESSAGE[*]}
            return 1
        fi
    fi

    # update firewall rules if desired
    if [ "$NZB_FIREWALL_ENABLED" == "on" ];
    then
        PORT=6789
        [ "$NZB_SSL_ENABLED" == "on" ] && PORT=6791
        create_fw_rules_for_ufw nzbget $PORT
    fi

    # take into account all changes made
    service nzbget start &>/dev/null

    # check that nzbget serves pages
    [ "$NZB_SSL_ENABLED" == "on" ] \
        && ADDR="https://${GLOBAL_FQDN}:6791" \
        || ADDR="http://${GLOBAL_FQDN}:6789"

    export SUCCESS=0
    verify_component_is_running "$ADDR"

    if [ "$SUCCESS" == "1" ];
    then
        echo "nzbget verified running on address: $ADDR (admin/admin)"
        echo 3 > $NZB_RVF # 3 will show on gauge as completed
    else
        echo Something broke.
        echo 1 > $NZB_RVF # 1 will show as failed
    fi

    echo 100 > $NZB_PROGRESSF # say that we are at 100%

    set +x
}
