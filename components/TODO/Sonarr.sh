
MY_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")") #full path to this dir
source "$MY_DIR/../functions/common-functions.sh"

Sonarr_description(){
    echo 'Smart PVR for newsgroup and bittorrent users.'
}

Sonarr_mono_certificates(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO Sonarr mono_certificates' ERR
    trap 'exit_good' 0

    if [ -f "$MY_DIR/global.env" -a -f "$MY_DIR/Sonarr.env" ];
    then
        source "$MY_DIR/global.env"
        source "$MY_DIR/Sonarr.env"
    else
        unset MESSAGE
        [ ! -r "$MY_DIR/global.env" ] && \
            MESSAGE+=("Unreadeable $MY_DIR/global.env")
        [ ! -r "$MY_DIR/Sonarr.env" ] && \
            MESSAGE+=("Unreadeable $MY_DIR/Sonarr.env")
        MESSAGE+=("Cannot continue.")
        simple_message_dialog \
            "Env file not found"
            "piMediaServer -> Sonarr"
            "${MESSAGE[*]}"
        return
    fi

    PACKAGES=(make gcc libssl-dev)
    INSTALL_PACKAGES=()
    for package in ${PACKAGES[*]};
    do
        dpkg -s $package &>/dev/null || INSTALL_PACKAGES+=($package)
    done
    which httpcfg &>/dev/null || INSTALL_PACKAGES=(mono-devel)

    [ -n "${INSTALL_PACKAGES[*]}" ] && \
        apt_get_safe -y install ${INSTALL_PACKAGES[*]}

    PVKSRC="$MY_DIR/../src/pvksrc.tgz.bin"
    COMPILE_DIR="$MY_DIR/../src/pvk"
    [ -d "$COMPILE_DIR" ] || mkdir -p "$COMPILE_DIR"

    tar xf "$PVKSRC" -C $COMPILE_DIR
    if [ ! -f "$COMPILE_DIR/pvk" ];
    then
        make -C "$COMPILE_DIR"
        ls -l "$COMPILE_DIR/pvk"
    fi

    "$COMPILE_DIR"/pvk -in "$SO_SSL_KEY" -topvk -nocrypt -out "$SO_SSL_KEY.pvk"
    # check that we have cert for that port or add it
    if su $SO_USER \
        -s /bin/bash \
        -c "httpcfg -list" | grep -q 'Port: 9898'; 
    then
        : # already got the cert for the port
    else
        su $SO_USER \
            -s /bin/bash \
            -c "httpcfg -add -port 9898 -pvk $SO_SSL_KEY.pvk -cert $SO_SSL_CERT"
    fi

    set +x
}

Sonarr_install(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO Sonarr install' ERR
    trap 'exit_good' 0

    if [ -f "$MY_DIR/global.env" -a -f "$MY_DIR/Sonarr.env" ];
    then
        source "$MY_DIR/global.env"
        source "$MY_DIR/Sonarr.env"
    else
        unset MESSAGE
        [ ! -r "$MY_DIR/global.env" ] && \
            MESSAGE+=("Unreadeable $MY_DIR/global.env")
        [ ! -r "$MY_DIR/Sonarr.env" ] && \
            MESSAGE+=("Unreadeable $MY_DIR/Sonarr.env")
        MESSAGE+=("Cannot continue.")
        simple_message_dialog \
            "Env file not found"
            "piMediaServer -> Sonarr"
            "${MESSAGE[*]}"
        return
    fi

    SO_LOGFILE=${SO_LOGF:-'/tmp/piMediaServer/sonnarr-log'}
    mkdir -p $(dirname $SO_LOGFILE)
    exec >  >(tee -a "$SO_LOGFILE")
    exec 2> >(tee -a "$SO_LOGFILE" >&2)

    echo 0 > "$SO_PROGRESSF" # say that we are at 100%
    echo 7 > "$SO_RVF" # 3 will show on gauge as completed

    create_user "$SO_USER" "$SO_DATA"

    # install mono (problem on arm?)
#    add-apt-repository "deb http://archive.raspbian.org/raspbian wheezy main contrib non-free"
#
    if ! dpkg -s nzbdrone &>/dev/null;
    then
        add-apt-repository "deb http://apt.sonarr.tv/ master main" &>/dev/null
        apt-key adv \
            --keyserver keyserver.ubuntu.com \
            --recv-keys FDA5DFFC &>/dev/null
        # WORKAROUND in jessie: https://discourse.osmc.tv/t/cannot-install-mono-to-install-sonarr/2249
        echo -e "Package: *\nPin: release n=jessie\nPin-Priority: 998" \
            > /etc/apt/preferences.d/pin-priority
        apt_get_safe update &>>$SO_LOGFILE
        apt_get_safe -y install nzbdrone &>>$SO_LOGFILE
        # dpkg -s mono-3.0 || apt-get install -y mono-3.0
        mv /etc/apt/preferences.d/pin-priority /etc/apt/preferences.d/pin-priority-disabled
    fi

    # clone the repo if the directory is not already there
    #[ -d "$SO_GITCLONE" ] || \
    #    git clone https://github.com/Sonarr/Sonarr "$SO_GITCLONE"
    #chown -R "$SO_USER:$SO_USER" "$SO_GITCLONE"

    # copy initscript and create /etc/sysconfig config
    etc_default_sonarr=(
          "SO_HOME=$SO_GITCLONE"
        "\nSO_DATA=$SO_DATA"
        "\nSO_USER=$SO_USER"
        "\nSO_PIDFILE=$SO_PIDFILE"
        "\nSO_OPTS=\"--config=$SO_SETTINGS\""
        "\nSSD_OPTS=\"--group=${GLOBAL_PI_GROUP}\""
    )
    echo -e "${etc_default_sonarr[*]}" > /etc/default/sonarr

    cp "$MY_DIR/../scripts/sonarr" /etc/init.d/sonarr
    chmod +x /etc/init.d/sonarr

    # enable cp to run at startup
    update-rc.d sonarr defaults >/dev/null

    # start only to create tho home directory (restart to avoid problem when
    # already running)
    #service sonarr start &>/dev/null
    #service sonarr stop &>/dev/null

    # modify the listening address
    # sed -i "s/http_password = .*/http_password = admin/" "$SO_SETTINGS"
    CONFIG=(
        "<Config>\n"
            "\t<Port>8989</Port>\n"
            "\t<UrlBase>\n"
            "\t</UrlBase>\n"
            "\t<BindAddress>*</BindAddress>\n"
            "\t<SslPort>9898</SslPort>\n"
            "\t<EnableSsl>True</EnableSsl>\n"
            "\t<ApiKey>02badf639e344a8493afa0ab9969369a</ApiKey>\n"
            "\t<AuthenticationMethod>Forms</AuthenticationMethod>\n"
            "\t<LogLevel>Info</LogLevel>\n"
            "\t<Branch>master</Branch>\n"
            "\t<LaunchBrowser>False</LaunchBrowser>\n"
            "\t<SslCertHash></SslCertHash>\n"
            "\t<UpdateMechanism>BuiltIn</UpdateMechanism>\n"
            "\t<AnalyticsEnabled>False</AnalyticsEnabled>\n"
        "</Config>\n\n"
    )
    mkdir -p $(dirname "$SO_SETTINGS")
    chown -R "$SO_USER" "$SO_DATA"
    echo -e "${CONFIG[*]}" > "$SO_SETTINGS"

    # verify ssl certificates and enable ssl usage only
    if [ "$SO_SSL_ENABLED" == "on" ];
    then
        unset MESSAGE
        if [ -r "$SO_SSL_CERT" -a -r "$SO_SSL_KEY" ];
        then
            Sonarr_mono_certificates
            [ -n "$DEBUG" ] && set -x
        else # key or certificate is not readeable
            [ ! -r "$SO_SSL_CERT" ] && \
                MESSAGE=("Certificate file ($SO_SSL_CERT) is not readeable.")
            [ ! -r "$SO_SSL_KEY" ] && \
                MESSAGE=("Certificate key ($SO_SSL_CERT) is not readable.")
        fi

        # SSL was not enabled, message the user
        if [ -n "${MESSAGE[0]}" ];
        then
            MESSAGE+=("Switching off SSL for Sonarr.")
            SO_SSL_ENABLED="off"

            #simple_message_dialog \
            #    'SSL enable' 'piMediaServer -> CouchPotato' ${MESSAGE[*]}
            #return 1
        fi
    fi

    # update firewall rules if desired
    if [ "$SO_FIREWALL_ENABLED" == "on" ];
    then
        PORT=8989
        [ "$SO_SSL_ENABLED" == "on" ] && PORT=9898
        create_fw_rules_for_ufw Sonarr $PORT
    fi

    # take into account all changes made
    service sonarr start &>/dev/null

    # check that sonarr serves pages
    [ "$SO_SSL_ENABLED" == "on" ] \
        && ADDR="https://${GLOBAL_FQDN}:9898" \
        || ADDR="http://${GLOBAL_FQDN}:8989"

    export SUCCESS=0
    verify_component_is_running "$ADDR"

    if [ "$SUCCESS" == "1" ];
    then
        echo "Sonarr verified running on address: $ADDR (admin/admin)"
        echo 3 > "$SO_RVF" # 3 will show on gauge as completed
    else
        echo Something broke.
        echo 1 > "${SO_RVF}" # 1 will show on gauge as failed
    fi

    echo 100 > "$SO_PROGRESSF" # say that we are at 100%

    service sonarr stop &>/dev/null
}

