
MY_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")") #full path to this dir
source "$MY_DIR/../functions/common-functions.sh"

owncloud_description(){
    echo 'Access & share your files, calendars, contacts, mail & more'
}

owncloud_packages(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO owncloud packages' ERR
    trap 'exit_good' 0

    components_variables owncloud

    echo 'deb http://download.owncloud.org/download/repositories/9.0/Ubuntu_16.04/ /' > \
        /etc/apt/sources.list.d/owncloud.list

    wget -q -O - https://download.owncloud.org/download/repositories/9.0/Ubuntu_16.04/Release.key |\
        apt-key add - &>/dev/null

    PACKAGES=(
        owncloud-files
        owncloud
        lighttpd

        php5-cgi     php7.0-cgi
        php5-curl    php7.0-curl
        php5-fpm     php7.0-fpm
        php5-gd      php7.0-gd
        php5-imagick php-imagick
        php5-intl    php7.0-intl
        php5-json    php7.0-json
        php5-mcrypt  php7.0-mcrypt
    )
    if [ "$OC_DATA_BACKEND" == "mysql" ];
    then
        echo mysql-server mysql-server/root_password password root | \
            sudo debconf-set-selections
        echo mysql-server mysql-server/root_password_again password root | \
            sudo debconf-set-selections
        PACKAGES+=(php5-mysql php7.0-mysql)
    else
        PACKAGES+=(php5-sqlite php-sqlite3)
    fi

    check_packages ${PACKAGES[@]}
}

owncloud_manual_install(){
    # gives back commands that should be run in order to install owncloud
    # manually, add user and enable/create external mount
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO owncloud manual_install' ERR
    trap 'exit_good' 0

    components_variables owncloud

    OCC=(su - www-data -s /bin/bash -c)

    # check whether we have maintenance:install (=already installed)
    COMMAND=(${OCC[@]} "/var/www/owncloud/occ help maintenance:install")
    if "${COMMAND[@]}" &>/dev/null;
    then
       OCC_COMMAND=(/var/www/owncloud/occ maintenance:install)
       if [ "$OC_DATA_BACKEND" == "mysql" ];
       then
           OCC_COMMAND+=(
               "--database=mysql"
               "--database-name=${OC_MYSQL_DBNAME}"
               "--database-user=${OC_MYSQL_USER}"
               "--database-password=${OC_MYSQL_PASSWORD}"
           )
       else
           OCC_COMMAND+=(
               "--database=sqlite"
           )
       fi
       OCC_COMMAND+=(
           "--admin-user \"admin\""
           "--admin-pass \"$OC_ADMIN_PASSWORD\""
       )
       [ -d "$OC_DATA/data" ] && OCC_COMMAND+=("--data-dir $OC_DATA/data")

       echo ${OCC[@]} \"${OCC_COMMAND[*]}\"
       #${OCC[@]} "${OCC_COMMAND[*]}"
    else
        (>&2 echo "owncloud have been already installed before")
    fi

    # add our fqdn as trusted domain
    # first find out whether there already is our name
    COMMAND=(
        /var/www/owncloud/occ
        config:system:get
        trusted_domains
    )
    if ${OCC[@]} "${COMMAND[*]}" &>/dev/null | grep -q ${GLOBAL_FQDN};
    then
        (>&2 echo "${GLOBAL_FQDN} is already trusted domain")
    else
        COMMAND=(
            /var/www/owncloud/occ 
            config:system:set 
            trusted_domains 
            2
            --value "${GLOBAL_FQDN}"
        )
        echo ${OCC[@]} \"${COMMAND[*]}\"
    fi

    # enable external storage
    if [ -d "${OC_DATA}/external" ];
    then
        echo ${OCC[@]} \"/var/www/owncloud/occ app:enable files_external\"

        H_O_EXTERNAL="${OC_DATA}/external"
        H_O_EXTERNAL=${H_O_EXTERNAL##\/} #remove first '/'
        H_O_EXTERNAL=${H_O_EXTERNAL//\//_} #exchange / for _
        # configure external storage
        CONFIG_EXTERNAL=(
            /var/www/owncloud/occ
            files_external:create
            --config datadir=${OC_DATA}/external
            "\/$H_O_EXTERNAL"
            '\\\\OC\\\\Files\\\\Storage\\\\Local'
            'null::null'
        )
        echo ${OCC[@]} \"${CONFIG_EXTERNAL[*]}\"
    fi
}

owncloud_install(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO owncloud install' ERR
    trap 'exit_good' 0

    components_variables owncloud
    components_logging OC
    echo 0 > "$OC_PROGRESSF" # say that we are at 0%
    echo 7 > "$OC_RVF"

    PACKAGES_TO_INSTALL=($(owncloud_packages))
    if [ ${#PACKAGES_TO_INSTALL[@]} -gt 0 ];
    then
        apt_get_safe update
        apt_get_safe -y install ${PACKAGES_TO_INSTALL[@]}
    fi

    if [ "$OC_DATA_BACKEND" == "sqlite" ];
    then
        PACKAGES_TO_UNINSTALL=(
            apache2
            mysql-server
            exim4-daemon-light
            exim4-daemon-heavy
        )
        apt_get_safe -y remove ${PACKAGES_TO_UNISTALL[@]}
    else # mysql db to be created and configured
        # https://doc.owncloud.org/server/8.2/admin_manual/configuration_database/linux_database_configuration.html
        MYSQL_INI=(
            "\nextension=pdo_mysql.so"
            "\nextension=mysql.so"
            "\n"
            "\n[mysql]"
            "\nmysql.allow_local_infile=On"
            "\nmysql.allow_persistent=On"
            "\nmysql.cache_size=2000"
            "\nmysql.max_persistent=-1"
            "\nmysql.max_links=-1"
            "\nmysql.default_port="
            "\nmysql.default_socket=/var/lib/mysql/mysql.sock"
            "\nmysql.default_host="
            "\nmysql.default_user="
            "\nmysql.default_password="
            "\nmysql.connect_timeout=60"
            "\nmysql.trace_mode=Off"
        )
        mysql_ini='/etc/php5/mods-available/mysql.ini'
        [ -f "$mysql_ini" ] && cp "$mysql_ini" "${mysql_ini}-owncloud.backup"
        echo -e ${MYSQL_INI[*]} > "$mysql_ini"

        if [ "$OC_MYSQL_LOW_MEMORY_CONFIG" -eq 'on' ];
        then
            LOW_MEMORY_CONFIG=(
                "\n[mysqld] "
                "\ndefault-storage-engine=MyISAM "
                "\nskip-innodb "
                "\n"
                "\nkey_buffer = 128K "
                "\n"
                "\nmax_allowed_packet = 1M "
                "\nnet_buffer_length = 2K "
                "\nread_buffer_size = 256K "
                "\nread_rnd_buffer_size = 256K "
                "\nsort_buffer_size = 64K "
                "\ntable_cache = 4 "
                "\nthread_stack = 64K"
            )
            config='/etc/mysql/mysql.d/low-memory-config.conf'
            echo -e ${LOW_MEMORY_CONFIG[*]} > $config
        fi

        MYSQL_COMMANDS=(
            "CREATE DATABASE IF NOT EXISTS $OC_MYSQL_DBNAME;"
            "GRANT ALL PRIVILEGES ON $OC_MYSQL_DBNAME.* TO '$OC_MYSQL_USER'@'localhost' \
                IDENTIFIED BY '$OC_MYSQL_PASSWORD';"
        )

        for COMMAND in "${MYSQL_COMMANDS[@]}";
        do
            mysql -u root --password=root -e "$COMMAND"
        done
    fi

    apt_get_safe -y remove exim4-daemon-light exim4-daemon-heavy
    apt_get_safe -y autoremove

    # we want data outside of web root which we configure during manual install
    # when link we have already moved it
    if [ -d /var/www/owncloud/data ];
    then
        mv -v /var/www/owncloud/data /var/www/owncloud/data-$(date +%s)
    fi

    [ -d ${OC_DATA}/data ] || mkdir -p ${OC_DATA}/data
    [ -d ${OC_DATA}/external ] || mkdir -p ${OC_DATA}/external

    chown -R www-data ${OC_DATA}
    chmod 750 ${OC_DATA} ${OC_DATA}/data ${OC_DATA}/external

    source ${MY_DIR}/lighttpd.sh
    lighttpd_install

    if dpkg -s lighttpd;
    then
        LIGHTTPD_OWNCLOUD=(
            "\n\$HTTP[\"url\"] =~ \"^/owncloud/data/\" {"
            "\n\turl.access-deny = (\"\")"
            "\n}"
            "\n\$HTTP[\"url\"] =~ \"^/owncloud($|/)\" {"
            "\n\tdir-listing.activate = \"disable\""
            "\n}"
            "\nalias.url += ( \"/owncloud/\" => \"/var/www/owncloud/\" )"
        )
        echo -e ${LIGHTTPD_OWNCLOUD[@]} > /etc/lighttpd/conf-available/99-owncloud.conf

        lighttpd-enable-mod fastcgi || :
        lighttpd-enable-mod fastcgi-php || :
        lighttpd-enable-mod owncloud || :
    else
        echo "lighttpd must be installed before owncloud."
        exit 1
    fi

    # take into account all changes made
    if which systemctl;
    then
        systemctl restart lighttpd
        systemctl status lighttpd
    else
        service lighttpd restart
        service lighttpd status
    fi

    # install in command line mode
    while read -r LINE;
    do
        echo "run: $LINE"
        eval $LINE
    done < <(owncloud_manual_install)

    # add normal user
    OCC=(su - www-data -s /bin/bash -c)
    COMMAND=(${OCC[@]} "/var/www/owncloud/occ user:lastseen ${OC_GUI_USER}")
    if "${COMMAND[@]}" | grep -q 'does not exist'; 
    then 
       COMMAND=(${OCC[@]} "user:add --password-from-env ${OC_GUI_USER}")
       ${OCC[@]} \
           "OC_PASS=${OC_GUI_PASSWORD} /var/www/owncloud/occ user:add --password-from-env ${OC_GUI_USER}"

    else 
        (>&2 echo "User ${OC_GUI_USER} already exists.")
    fi

    # check that owncloud serves pages
    [ "$OC_SSL_ENABLED" == "on" ] \
        && ADDR="https://${GLOBAL_FQDN}/owncloud/" \
        || ADDR="http://${GLOBAL_FQDN}/owncloud/"

    export SUCCESS=0
    verify_component_is_running "$ADDR"

    if [ "$SUCCESS" == "1" ];
    then
        log_result "owncloud verified running on address: $ADDR"
        log_result "owncloud admin password: $OC_ADMIN_PASSWORD"
        log_result "owncloud user $OC_GUI_USER password: $OC_GUI_PASSWORD"
        log_result "owncloud data directory: ${OC_DATA}/external"
        if [ "${OC_DATA_BACKEND}" == 'mysql' ];
        then
            log_result "owncloud mysql db: $OC_MYSQL_DBNAME "
            log_result "owncloud mysql username: $OC_MYSQL_USER"
            log_result "owncloud mysql password: $OC_MYSQL_PASSWORD"
        fi
        echo 3 > "${OC_RVF}" # 1 will show on gauge as failed
    else
        echo Something broke.
        echo 1 > "${OC_RVF}" # 1 will show on gauge as failed
    fi
    echo 98 > "$OC_PROGRESSF" # say that we are at 100%

    SERVICES=(lighttpd)
    [ -f /etc/init.d/php5-fpm ] && SERVICES+=(php5-fpm)
    [ -f /lib/systemd/system/php7.0-fpm.service ] && SERVICES+=(php7.0-fpm)

    [ $(dpkg -s mysql-server) ] && SERVICES=(mysql-server)
    if which systemctl;
    then
        for SERVICE in ${SERVICES[@]}; do systemctl stop $SERVICE & done
    else
        for SERVICE in ${SERVICES[@]}; do service $SERVICE stop & done
    fi
    wait

    echo 100 > "$OC_PROGRESSF" # say that we are at 100%
}
