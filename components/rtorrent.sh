#!/bin/bash

MY_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")") #full path to this dir
source "$MY_DIR/../functions/common-functions.sh"

rtorrent_description(){
    echo 'rTorrent BitTorrent client.'
}

rtorrent_packages(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO rtorrent packages' ERR
    trap 'exit_good' 0

    components_variables rtorrent

    PACKAGES=(rtorrent)
    check_packages ${PACKAGES[@]}
}

rtorrent_install(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO rtorrent install' ERR
    trap 'exit_good' 0

    components_variables rtorrent
    components_logging RT
    echo 0 > "$RT_PROGRESSF" # say that we are at 0%
    echo 7 > "$RT_RVF" # 7 will show on gauge as in-progress

    PACKAGES_TO_INSTALL=($(rtorrent_packages))
    if [ ${#PACKAGES_TO_INSTALL[@]} -gt 0 ];
    then
        apt_get_safe update
        apt_get_safe -y install ${PACKAGES_TO_INSTALL[@]}
    fi

    create_user "$RT_USER":${RT_USER_UID} "$RT_DATA"
    chmod o+x "$RT_DATA" # we will clone rtorrent guis here, lighttp will need it

    [ -d "$GLOBAL_PI_HOME" ] || create_pi_home_structure

    mkdir -p ${RT_DATA}/{sessions,logs,bin,lib}
    chown -R "$RT_USER" ${RT_DATA}/{sessions,logs,bin,lib}
    setfacl -m u:${RT_WEBSERVER_USER}:x $RT_DATA $RT_DATA/sessions

    if [ "${RT_PYROSCOPE}" == "yes" ];
    then
	[ -d ${RT_DATA}/lib/pyroscope ] || \
		git clone "https://github.com/pyroscope/pyrocore.git" ${RT_DATA}/lib/pyroscope
        chown -R ${RT_USER} ${RT_DATA}/lib/pyroscope
        su - $RT_USER -s /bin/bash \
            -c "${RT_DATA}/lib/pyroscope/update-to-head.sh &>${RT_DATA}/logs/rtorrent-pyroscope-log" &
        export PYROSCOPE_BG_PID=$!
        echo 'PATH=$PATH:~/bin' >> ~/.bashrc
    fi

    #  rtorrent socket must be readable/writable by webserver daemon
    mkdir -p "$(dirname $RT_SCGI_SOCKET)"
    chown -R "$RT_USER:$RT_SCGI_SOCKET_GROUP" "$(dirname $RT_SCGI_SOCKET)"
    chmod -R 2770 "$(dirname $RT_SCGI_SOCKET)"

    if which systemctl;
    then
        RTORRENT_UNIT=(
            "\n[Unit]"
            "\nDescription=rTorrent BitTorrent client."
            "\nAfter=syslog.target network.target lighttpd.service"
            "\nWants=lighttpd.service"
            "\n"
            "\n[Service]"
            "\nEnvironmentFile=/etc/default/rtorrent"
            "\nType=forking"
            "\nKillMode=none"
            "\nExecStart=/usr/bin/screen -d -m -fa -S rtorrent /usr/bin/rtorrent"
            "\nExecStop=/usr/bin/killall -w -s 2 /usr/bin/rtorrent"
            "\n"
            "\n[Install]"
            "\nWantedBy=multi-user.target"
        )
        echo -e ${RTORRENT_UNIT[*]} > /etc/systemd/system/rtorrent.service
        systemctl daemon-reload

##         # stop, disable and mask original rtorrent daemon unit
##         if [ -f /lib/systemd/system/rtorrent.service ];
##         then
##             systemctl stop rtorrent
##             systemctl disable rtorrent
##             systemctl mask rtorrent
##         fi

        RTORRENT_UNIT_CONF=(
            "\n[Service]"
            "\n\tGroup=${GLOBAL_PI_GROUP}"
            "\n\tUser=$RT_USER"
            "\n\tWorkingDirectory=${RT_DATA}"
        )
        mkdir -p /etc/systemd/system/rtorrent.service.d
        echo -e ${RTORRENT_UNIT_CONF[*]} > \
            /etc/systemd/system/rtorrent.service.d/user-group.conf

        systemctl daemon-reload
        systemctl stop rtorrent
    else
        cp "$MY_DIR/../scripts/rtorrent-init.ubuntu" /etc/init.d/rtorrent
        chmod 755 /etc/init.d/rtorrent
        service rtorrent stop
    fi

    DEFAULT_RTORRENT=(
        "\nRT_USER=${RT_USER}"
    )
    echo -e ${DEFAULT_RTORRENT[@]} > /etc/default/rtorrent

    if [ $(dpkg-query --show --showformat='${db:Status-Status}\n' lighttpd) == 'installed' ];
    then
        CONFIG_RTORRENT_RPC=(
            "\n#https://github.com/rakshasa/lighttpd/wiki/RPC-Setup-XMLRPC"
            "\nserver.modules += ( \"mod_scgi\" )"
            "\nscgi.server = ("
            "\n\t\"/RPC2\" => ("
            "\n\t\t\"127.0.0.1\" => ("
            "\n\t\t\t\"check-local\" => \"disable\","
        )
        if [ "$RT_SCGI_TYPE" == "socket" ];
        then
            CONFIG_RTORRENT_RPC+=(
                "\n\t\t\t\"socket\" => \"$RT_SCGI_SOCKET\","
            )
        else
            CONFIG_RTORRENT_RPC+=(
                "\n\t\t\t\"host\" => \"127.0.0.1\","
                "\n\t\t\t\"port\" => \"$RT_SCGI_IP_PORT\","
            )
        fi

        CONFIG_RTORRENT_RPC+=(
            "\n\t\t\t\"disable-time\" => 0,"
            "\n\t\t)"
            "\n\t)"
            "\n)"
            "\n#url.access-deny = ( \".passwd\", \"logs\" )"
            "\n"
            "\n\$HTTP[\"url\"] =~ \"^/RPC2\" {"
            "\n\tauth.backend += \"htdigest\""
            "\n\tauth.backend.htdigest.userfile += \"$RT_DATA/.passwd\""
            "\n\tauth.require += ("
            "\n\t\t\"/RPC2\" => ("
            "\n\t\t\t\"method\" => \"basic\","
            "\n\t\t\t\"realm\" => \"rtorrent\","
            "\n\t\t\t\"require\" => \"valid-user\""
            "\n\t\t)"
            "\n\t)"
            "\n}"
        )
        echo -e "${CONFIG_RTORRENT_RPC[@]}" > /etc/lighttpd/conf-available/99-rtorrent-rpc.conf

        # create passwd file for admin authentication
        hash=$(echo -n "${RT_RPC_USER}:rtorrent:${RT_RPC_PASSWORD}" | md5sum | cut -b -32)
        echo "${RT_RPC_USER}:rtorrent:$hash" > ${RT_DATA}/.passwd

        chown ${RT_WEBSERVER_USER} ${RT_DATA}/.passwd

        for MOD in auth rtorrent-rpc;
        do
            lighttpd-enable-mod $MOD || rv=$? && rv=$?
            [[ $rv -eq 0 ]] || [[ $rv -eq 2 ]]
        done
    fi

    CONFIG_RTORRENT=(
        "\nsession = $RT_DATA/sessions"
        "\nencryption = allow_incoming,try_outgoing,enable_retry"
        "\ndht = auto"
        "\ndht_port = 6881"
        "\nport_range = 49152-49152"
        "\nport_random = no"
        "\n#https://github.com/rakshasa/rtorrent/wiki/RTorrentRatioHandling"
        "\nratio.enable="
        "\nratio.min.set=190"
        "\nratio.max.set=200"
        "\n"
        "\n"
        "\n###"
        "\n### 'download/upload as much as fast as possible' ###"
        "\n###"
        "\n# http://forums.rutorrent.org/index.php?topic=207.15"
        "\n# What seems to make sense as to 'download as much as fast as possible' without"
        "\n# overloading the pi is to set global max of peers each torrent can connect to"
        "\n# and global max of torrents that might be simultaniously active. The rest is"
        "\n# left for rtorrent to decide."
        "\n"
        "\n## # Maximum and minimum number of peers to connect per torrent."
        "\n## min_peers = 40"
        "\nmax_peers = 50"
        "\n##"
        "\n## # Same as above but for seeding completed torrents (-1 = same as downloading)"
        "\n## min_peers_seed = -1"
        "\n## max_peers_seed = -1"
        "\n"
        "\n# Maximum number of simultanious uploads and downloads (per torrent and globals)"
        "\n#max_uploads = 5"
        "\nmax_uploads_global = 30"
        "\n#max_downloads = 5"
        "\nmax_downloads_global = 10"
        "\n"
        "\n# total bandwidth in kB (1000 = 1MBit/s)"
        "\n#download_rate = 29000"
        "\n#upload_rate = 8500"
        "\n"
        "\n###"
        "\n### 'download/upload as much as fast as possible' ###"
        "\n###"
        "\n"
        "\ndirectory = $GLOBAL_PI_HOME/download/in-progress"
        "\ncheck_hash = yes"
        "\n"
        "\n#move finished torrents to dirs according to labels"
        "\n#http://unix.stackexchange.com/questions/127382/move-completed-torrents-in-rtorrent-according-to-label"
        "\n"
        "\nsystem.method.insert = d.get_finished_dir,simple,"
            "\"cat=${GLOBAL_PI_HOME}/download/finished/,\$d.get_custom1=\""
        "\nsystem.method.set_key = event.download.finished,move_complete,"
            "\"d.set_directory=\$d.get_finished_dir=;"
            "execute=mkdir,-p,\$d.get_finished_dir=;"
            "execute=mv,-u,\$d.get_base_path=,\$d.get_finished_dir=\""

        "\n"
        "\n# schedule removal of data, when origin torrent file is removed"
        "\nschedule = untied_directory, 10, 10, remove_untied="
        "\nmethod.set_key = event.download.erased,delete_erased,\"execute=rm,-rf,--,$d.get_base_path=,\""

        "\n"
        "\nschedule = watch_blackhole,0,10,\"load.start=${GLOBAL_PI_HOME}/blackhole/*.torrent\""

	"\n"
	"\n#schedule = scgi_permission,0,0,\"execute.nothrow=chmod,\\\"g+w,o=\\\",$RT_SCGI_SOCKET\""
	"\nschedule = scgi_permission,0,0,\"execute.nothrow=chmod,770,$RT_SCGI_SOCKET\""
	"\n"
    )
    if [ -n "${RT_LABELS}" ];
    then
        for LABEL in $RT_LABELS;
        do
            CONFIG_RTORRENT+=(
                "\nschedule = watch_blackhole_$LABEL,0,10,"
                "\"load_start=${GLOBAL_PI_HOME}/blackhole/$LABEL/*.torrent,"
                "d.set_custom1=$LABEL\""
            )
        done
    fi

    #logging
    CONFIG_RTORRENT+=(
        "\n"
        "\n# Levels = critical error warn notice info debug"
        "\n# Groups = connection_* dht_* peer_* rpc_* storage_* thread_* tracker_* torrent_*"
        "\nmethod.insert = log.filename, string|const|simple|private,\"cat=$RT_DATA/logs/rtorrent-,"
            '\"$system.time=\",\".log\""'
        "\nprint=(cat,\"Logging to \",(log.filename))"
        "\nlog.open_file = \"log\", (log.filename)"
        "\nlog.add_output = \"debug\", \"log\""
    )

    if [ "$RT_SCGI_TYPE" == "socket" ];
    then
        CONFIG_RTORRENT+=("\n\nscgi_local = $RT_SCGI_SOCKET")
    else
        CONFIG_RTORRENT+=("\n\nscgi_port = 127.0.0.1:${RT_SCGI_IP_PORT}")
    fi
    echo -e "${CONFIG_RTORRENT[@]}" > $RT_SETTINGS

    # update firewall rules if desired
    if [ "$RT_FIREWALL_ENABLED" == "on" ];
    then
        create_fw_rules_for_ufw rtorrent '6881:6900/udp|6881:6900/tcp'
    fi

    if which systemctl;
    then
        systemctl start rtorrent
        systemctl is-active rtorrent
    else
        service rtorrent start
        service rtorrent status
    fi

    if [ "${RT_PYROSCOPE}" == "yes" ];
    then
        wait $PYROSCOPE_BG_PID

        su - rtorrent -s /bin/bash -c "~/bin/pyroadmin --create-config"

        touch ~rtorrent/.bash_completion
        grep /\.pyroscope/ ~rtorrent/.bash_completion >/dev/null || \
                echo ". ~rtorrent/.pyroscope/bash-completion.default" >> ~rtorrent/.bash_completion
    fi

    LOG_STRING="rTorrent process is correctly attached to " 
    if [ "$RT_SCGI_TYPE" == "socket" ];
    then
        # wait ten seconds for socket to appear
        COUNT=0
        while [ $COUNT -le 100 ];
        do
            if [ -S "$RT_SCGI_SOCKET" ];
            then
                if fuser "$RT_SCGI_SOCKET";
                then
                    chmod 770 "$RT_SCGI_SOCKET"
                    #setfacl -m u:"$RTGUI_HTTPD_USER":rwx "$RT_SCGI_SOCKET"
                    break
                fi
            fi
            sleep 0.1
            COUNT=$((COUNT+1))
        done
        [ $COUNT -lt 100 ] #or exit

        PROCESS=$(fuser "$RT_SCGI_SOCKET" | cut -f 2 -d :)
        PROCESS=${PROCESS// /} #remove spaces

        #socket must be tied to a process
        [ -n "$PROCESS" ]
        #that process must be rtorrent
        [ "$(cat /proc/$PROCESS/comm)" == "rtorrent" ]

        LOG_STRING+="${RT_SCGI_SOCKET}" # correctly attached
        log_result $LOG_STRING
        echo 3 > $RT_RVF

    else #not socket, attached to ip
        # test access to rtorrent socket through lighttpd or with xmlrpc
        if [ "$(dpkg-query --show --showformat='${db:Status-Status}\n' lighttpd)" == 'installed' ];
        then
            # check that we can access rtorrent socket
            if which systemctl;
            then
                systemctl restart lighttpd
                systemctl is-active lighttpd
            else
                service restart lighttpd
                service status lighttpd
            fi
            ADD_OPTIONS="-u ${RT_RPC_USER}:${RT_RPC_PASSWORD}" \
                verify_component_is_running http://127.0.0.1/RPC2

            if [ "$SUCCESS" -eq "1" ];
            then
                LOG_STRING+="127.0.0.1:${RT_SCGI_IP_PORT}"
                LOG_STRING+=" and accessible through http://127.0.0.1/RPC2"
                log_result $LOG_STRING
                echo 3 > $RT_RVF
            else
                echo "Something went wrong"
                echo 1 > $RT_RVF # will show as failed
            fi
        else
            # lighttpd not installed
            :
        fi
    fi

    if which systemctl;
    then
        systemctl stop rtorrent
    else
        service rtorrent stop &>/dev/null
    fi

    echo 100 > $RT_PROGRESSF # say that we are at 100%
}
