
export LC_ALL=${LC_ALL:-'en_US.UTF-8'}

my_dir() {
    local MY_DIR
    MY_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")") #full path to this dir

    echo $MY_DIR
}
source "$(my_dir)/menus.sh"

exit_err() {
    [ -n "$DEBUG" ] && set +x
    local LINE_NUMBER=${1:-'Unknown'}
    local CALLER=${2:-'Lazycaller'}
    local CALLER_FUNCTION=${3:-'Unidetified function'}

    # make note that we have failed for dialog --gauge
    [ -f "$LOGZ_DIR/$CALLER-rv" ] && echo 1 > "$LOGZ_DIR/$CALLER-rv"
    [ -f "$LOGZ_DIR/$CALLER-progress" ] && echo 100 > "$LOGZ_DIR/$CALLER-progress"

    echo "exit_err()"
    echo "error trapped: $CALLER, $CALLER_FUNCTION, line: $LINE_NUMBER"
    echo "rerun with DEBUG=1 to see what happened"
    exit 1
}

# runs on good exit
exit_good() {
    set +x
    trap - ERR 0
    exec 4>&-
}

unrartest() {
    if hash unrar 2>/dev/null; then
        return
    else
        cpunum=$(nproc)
        apt_get_safe install build-essential -y
        cd /tmp
        wget rarlab.com/rar/unrarsrc-5.2.7.tar.gz
        tar -xvf unrarsrc-5.2.7.tar.gz
        cd unrar
        make -j$cpunum -f makefile
        install -v -m755 unrar /usr/bin
        cd ..
        rm -R unrar
        rm unrarsrc-5.2.7.tar.gz
    fi
}

apt_get_safe(){
    remove_lock(){
        echo "Removing lock from redirection 9"
        flock -u 9 #release lock
        # http://wiki.bash-hackers.org/howto/redirection_tutorial
        exec 9>&- #remove redirection number 9
        trap - INT TERM EXIT
    }

    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO common-functions apt_get_safe' ERR
    trap 'exit_good' 0 #runs on exit 0

    local LOGZ_DIR=${LOGZ_DIR:-'/tmp/piMediaServer'}

        SECS=0
        while [ $SECS -le 3000 ];
        do
            # http://mywiki.wooledge.org/BashFAQ/045
            exec 9>"${LOGZ_DIR}/apt-paralelization-lock"
            if flock -n 9; 
            then
                trap 'remove_lock' INT TERM EXIT
                if ! fuser /var/lib/dpkg/lock;
                then
                    apt-get $*
                    break
                fi
            fi
#           case $(($SECS % 4)) in
#               0 ) j="-" ;;
#               1 ) j="\\" ;;
#               2 ) j="|" ;;
#               3 ) j="/" ;;
#           esac
            local MESSAGE=(
                "Waiting for the lock..." 
                "(sec remaining: $((300-$((SECS/10)))))" 
            )
            echo -en "\r${MESSAGE[@]}"

            sleep 0.1
            SECS=$((SECS+1))
        done
        [ "$SECS" -lt 3000 ] #or trap
        remove_lock
}

# create all the groups that are passed as parameters
# $1: group name
#   if there is ':' in group name it means that you want it to have specific
#   gid. Example: pirate:666
create_group(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO create_group' ERR
    trap 'exit_good' 0 #runs on exit 0

    [ "$#" -ge "1" ];

    while (($#));
    do
        GROUP_NAME=${1%:*}
        unset GID
        [[ $1 == *[:]* ]] && GID=${1#*:} #does it contain ':'?

        if ! getent group "$GROUP_NAME" &>/dev/null;
        then
            if [ -n "$GID" ];
            then
                groupadd "$GROUP_NAME" --gid $GID
            else
                groupadd "$GROUP_NAME"
            fi
        else
            # user exists, change its UID to given value
            [ -n "$GID" ] && groupmod --gid $GID $GROUP_NAME
        fi
        shift

        [ "$GROUP_NAME" == "pirate" ] && create_pi_home_structure || :
    done
}

# Create the user passed as the first parameter with home directory passed as
# second parameter. The same way as with group, it might contain UID separated
# by ':'. UID and GUI must both be free if given.
create_user(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO common-functions create_user' ERR
    trap 'exit_good' 0 #runs on exit 0

    GLOBAL_PI_GROUP=${GLOBAL_PI_GROUP:-'pirate'}
    GLOBAL_PI_GROUP_GID=${GLOBAL_PI_GROUP_GID:-'5000'}

    [ "$#" -eq "2" ] #exits if untrue

    USER_NAME=${1%:*}
    unset MY_UID
    [[ $1 == *[:]* ]] && MY_UID=${1#*:} #does it contain ':'?

    MY_GROUPS=("${GLOBAL_PI_GROUP}:${GLOBAL_PI_GROUP_GID}")
    [ -n "$GID" ] && MY_GROUPS+=("${USER_NAME}:${MY_UID}") || MY_GROUPS+=(${USER_NAME})
    create_group ${MY_GROUPS[@]}

    if ! getent passwd "$USER_NAME" &>/dev/null;
    then
        COMMAND=(
            useradd
            --comment "Daemon user for $USER_NAME"
            --system
            --gid $USER_NAME
            --groups $GLOBAL_PI_GROUP
            --shell /sbin/nologin
            --home-dir $2
        )
        [ -n "$UID" ] && COMMAND+=(--uid $MY_UID)
        COMMAND+=($USER_NAME)

        "${COMMAND[@]}"
    else
        # user exists, change its UID to given value and its primary group to
        # the same id
        if [ -n $MY_UID ];
        then
            # the group might have been created earlier and not have tho
            # correct id
            create_group "${USER_NAME}:${MY_UID}"

            usermod --uid $MY_UID $USER_NAME
            usermod --gid $USER_NAME $USER_NAME
        fi
    fi

    [ -d "$2" ] || mkdir -p "$2"
    chmod 700 "$2"
    chown -R "$USER_NAME:$USER_NAME" "$2"


    # add user to pirate group for the case where it have been previously
    # created and not added to the group
    usermod -a -G "$GLOBAL_PI_GROUP" $USER_NAME

    # check that the user id, primary group id, and pirate group id are correct
    [ "$(id -u $USER_NAME)" == $MY_UID ]
    [ "$(id -g $USER_NAME)" == $MY_UID ]
    getent group ${GLOBAL_PI_GROUP_GID} | grep -q ^pirate:
}

# creates the necessary directory structure and gives it the correct
# permissions
create_pi_home_structure(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO create_pi_home_structure' ERR
    trap 'exit_good' 0 #runs on exit 0

    GLOBAL_PI_GROUP=${GLOBAL_PI_GROUP:-'pirate'}
    GLOBAL_PI_HOME=${GLOBAL_PI_HOME:-'/home/pirate'}

    apt_get_safe -y install acl

    DIRECTORIES=(
        blackhole
        download/in-progress
        download/finished
        library/moviez
        library/seriez
        library/comics
        library/audio
    )
    for DIRECTORY in ${DIRECTORIES[@]};
    do
        mkdir -p "$GLOBAL_PI_HOME/$DIRECTORY"; 
    done

    chgrp -R "$GLOBAL_PI_GROUP" "$GLOBAL_PI_HOME"
    chmod -R 2750 "$GLOBAL_PI_HOME"
    chmod +s "$GLOBAL_PI_HOME"
    setfacl -R -m d:group::rwx "$GLOBAL_PI_HOME"
}

# takes name as first argument and ports as all other parameters (one or many)
# Multiple protocols are specified as "80/udp|80/tcp" with a vertical bar
# separating them. If just "80" was specified then both udp and tcp are
# assumed.  The port can be a comma delimited list (80,443) or a range with a
# colon (81:82) or combined (80,443,81:82/udp|8080/tpc). If a range is
# specified then a separate entry for each protocol is required
# (81:82/udp|81:82/tcp).
#
create_fw_rules_for_ufw(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO create_fw_rules_for_ufw' ERR
    trap 'exit_good' 0 #runs on exit 0

    [ "$#" -ge "2" ] #or exit
    TITLE=$1
    DESCRIPTION=$($1_description)
    shift

    # update firewall rules if desired
    if [ -d "/etc/ufw/applications.d" ];
    then
        ufw=(
            "[$TITLE]"
            "\ntitle = $TITLE"
            "\ndescription = ${DESCRIPTION:-'dummy description'}"
        )

        PORTS=()
        while (($#));
        do
            [ $# -eq 1 ] && PORTS+=("$1") || PORTS+=("$1,")
            shift;
        done

        if [ ${#PORTS[@]} -lt 2 ];
        then
            ufw+=("\nports = ${PORTS[@]}")
        else
            PORTS=("\nports = ${PORTS[*]}/tcp|${PORTS[*]}/udp")
            ufw+=${PORTS// }
        fi
        echo -e "${ufw[*]}" > /etc/ufw/applications.d/$TITLE

        # if we have the binary we allow the application we have just created
        if [ -x /usr/sbin/ufw ];
        then
            /usr/sbin/ufw app update $TITLE >/dev/null
            /usr/sbin/ufw allow $TITLE >/dev/null
        fi
    fi
}

# verify that address passed is reacheable
# reachable: sets SUCCESS=1 otherwise SUCCESS=0
verify_component_is_running(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO verify_component_is_running' ERR
    trap 'exit_good' 0 #runs on exit 0

    [ "$#" -ge "1" ] #or exit

    local SUCCESS_STRINGS=(
        "^HTTP/.*200 OK" #lighttpd
        "Directory send OK." #vsftpd
        "Server: TornadoServer/4.1" #couchpotato
    )
    SUCCESS_STRINGS+=($SUCCESS_STRING)
    local ADD_OPTIONS=${ADD_OPTIONS:-''}

    I=0
    export SUCCESS=0
    OUTPUT=$(mktemp --tmpdir=/tmp tmp-verify-running-XXXXXXXXXX)
    while [ $I -lt 10 ];
    do
        COMMAND=(
            curl
            --max-time 10
            --insecure
            --silent
            --output -
            --dump-header -
            ${ADD_OPTIONS}
            $1
        )
        if eval ${COMMAND[@]} &>${OUTPUT};
        then
            for string in "${SUCCESS_STRINGS[@]}";
            do
                if grep -q "$string" $OUTPUT;
                then
                    SUCCESS=1
                    break
                fi
            done
            break
        else
            I=$((I+1))
            sleep 1
        fi
    done
}

# Make sure that our hostname in /etc/hostname resolves. This would otherwise
# cause a lot of different weird errors.
#
make_fqdn_work() {
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO set_hostname' ERR
    trap 'exit_good' 0 #runs on exit 0

    [ -r /etc/hostname ] || echo "localhost.localdomain" > /etc/hostname

    HOSTNAME="$(cat /etc/hostname)"
    if getent hosts "$HOSTNAME";
    then
        : # good
    else
        hostnamectl set-hostname "${HOSTNAME}"
    fi
    sed "s/127.0.0.1.*/127.0.0.1 $HOSTNAME localhost/" /etc/hosts > /tmp/hosts
    cp -v /tmp/hosts /etc/hosts

    # make /etc/hostname resolvable via /etc/hosts
    export GLOBAL_MY_IP=${GLOBAL_MY_IP:-$(ip route get 8.8.8.8 | head -n 1 | awk '{ print $NF; }')}
    export GLOBAL_FQDN=${GLOBAL_FQDN:-$(hostname --fqdn)}
}

#
# Install needed packages.
#
update_check() {
    trap 'exit_err $LINENO update_check' ERR
    [ -n "$DEBUG" ] && set -x
    ALL_PACKAGES=(
        apt-transport-https
        curl
        debconf-utils
        dialog
        dnsutils
        git
        net-tools
        sudo
        unzip
        dialog
        software-properties-common
    )
    # build-essential

##     ALL_PACKAGES="\
##         alsa-base alsa-utils apt-transport-https build-essential curl \
##         debconf-utils dialog dnsutils git html2text net-tools stunnel4 sudo \
##         unzip dialog \
##     "

    PACKAGES_TO_INSTALL=()
    for pkg in ${ALL_PACKAGES[@]}; 
    do
        dpkg -s "$pkg" &>/dev/null || PACKAGES_TO_INSTALL+=($pkg)
    done

    if [ -n "${PACKAGES_TO_INSTALL[0]}" ];
    then
        apt_get_safe -y install ${PACKAGES_TO_INSTALL[*]}
        apt_get_safe upgrade -y
    else
        DIALOG=${DIALOG:-'dialog'}
        MESSAGE=(
            "All the necessary packages are already installed."
            "Wait five seconds or press ok to continue."
        )
        #simple_message_dialog \
        #    "${MESSAGE[*]}" "Packages check" "piMediaServer --> Packages check"
    fi
    set +x
}

#
# generate self-signed certificates for our hostname, if they do not exist,
# save them to /home/htpc/certs/<hostname>.{key,crt} and update the
# configuration
#
generate_self_signed_certs(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO certificates_generation' ERR
    trap 'exit_good' 0


    GLOBAL_FQDN=${GLOBAL_FQDN:-$(hostname --fqdn)}
    GLOBAL_SSL_CERT=${GLOBAL_SSL_CERT:-"/etc/ssl/certs/$(hostname -s)-cert.pem"}
    GLOBAL_SSL_KEY=${GLOBAL_SSL_KEY:-"/etc/ssl/certs/$(hostname -s)-key.pem"}
    GLOBAL_PI_GROUP=${GLOBAL_PI_GROUP:-'pirate'}
    GLOBAL_PI_GROUP_GID=${GLOBAL_PI_GROUP_GID:-'5000'}

    create_group ${GLOBAL_PI_GROUP}:${GLOBAL_PI_GROUP_GID}
    which openssl &>/dev/null || { echo "No openssl found."; return; }

    unset MESSAGE
    if [ -f "${GLOBAL_SSL_CERT}" -o -f "${GLOBAL_SSL_KEY}" ];
    then
        [ -f "${GLOBAL_SSL_CERT}" ] && \
            MESSAGE+=("$GLOBAL_SSL_CERT exists.")
        [ -f "${GLOBAL_SSL_KEY}" ] && \
            MESSAGE+=("$GLOBAL_SSL_KEY exists.")
        MESSAGE=("Not generating ssl keys.")
    else
        openssl req \
            -new -x509 -nodes -days 3650 -newkey rsa:4096 \
            -keyout "$GLOBAL_SSL_KEY" \
            -out "$GLOBAL_SSL_CERT" \
            -subj "/CN=$GLOBAL_FQDN" \
            2>&1

    fi
    chgrp ${GLOBAL_PI_GROUP} "$GLOBAL_SSL_KEY" "$GLOBAL_SSL_CERT"
    chmod 750 "$GLOBAL_SSL_KEY" "$GLOBAL_SSL_CERT"
}

components_variables(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO common-functions components_variables' ERR
    trap 'exit_good' 0

    [ -n "$1" ] #component name (owncloud)

    # this is $ROOT/functions
    local THIS_SCRIPT_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")
    local MY_DIR="${THIS_SCRIPT_DIR}/../components/"

    if [ -f "$MY_DIR/global.env" -a -f "$MY_DIR/$1.env" ];
    then
        source "$MY_DIR/global.env"
        source "$MY_DIR/$1.env"
    else
        unset MESSAGE
        [ ! -r "$MY_DIR/global.env" ] && \
            MESSAGE+=("Unreadeable $MY_DIR/global.env")
        [ ! -r "$MY_DIR/$1.env" ] && \
            MESSAGE+=("Unreadeable $MY_DIR/$1.env")
        MESSAGE+=("Cannot continue.")
        simple_message_dialog \
            "${MESSAGE[*]}" \
            "Env file not found" \
            "piMediaServer -> $1"
        echo ${MESSAGE[*]}
        exit 1
    fi
}

components_logging(){
    [ -n "$1" ] #component prefix (OC)

    local MY_DIR=$(my_dir)
    [ -f $MY_DIR/../components/global.env ] && \
        source $MY_DIR/../components/global.env

    local LOGDIR=${LOGDIR:-'/tmp/piMediaServer'}

    PREFIX=$1
    eval ${PREFIX}_LOGFILE="\$${PREFIX}_LOGF"
    if eval [ -z "\$${PREFIX}_LOGFILE" ];
    then
        ${PREFIX}_LOGFILE="${LOGDIR}/$1-log"
    fi
    export ${PREFIX}_LOGFILE
    eval LOGFILE=\$${PREFIX}_LOGFILE

    mkdir -p $(dirname "$LOGFILE")
    exec >  >(tee -a "$LOGFILE")
    exec 2> >(tee -a "$LOGFILE" >&2)
}

log_result(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO common-functions log_result' ERR
    trap 'exit_good' 0

    [ -n "$1" ]

    local LOGDIR=${LOGZ_DIR:-'/tmp/piMediaServer'}
    #exec 4> >(tee -a "$LOGDIR/success-log")
    echo $*
    echo $(date) $* >> "${LOGDIR}/result-log"
}

# takes a list of packages and return back the list of packages that are not
# installed and at the same time do exist (can be installed)
check_packages(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO common-functions check_packages' ERR
    trap 'exit_good' 0

    [ -n "$1" ]
    local TMPDIR=${LOGZ_DIR:-'/tmp/piMediaServer'}
    local TEMPF=$(mktemp --tmpdir=${TMPDIR} tmp-check-packages-XXXXXXXXXX)

    PACKAGES_TO_INSTALL=()
    for PACKAGE in $*;
    do
        if [ -n "$PARALLEL" ];
        then
            if ! dpkg -s $PACKAGE &>/dev/null;
            then
                apt-cache search $PACKAGE | grep -q "^$PACKAGE " && \
                    echo $PACKAGE >> ${TEMPF}
            fi &
        else
            if ! dpkg -s $PACKAGE &>/dev/null;
            then
                apt-cache search $PACKAGE | grep -q "^$PACKAGE " && \
                    echo $PACKAGE >> ${TEMPF}
            fi
        fi
    done
    [ -n "$PARALLEL" ] && wait

    sort $TEMPF | uniq
    rm -f $TEMPF
}
