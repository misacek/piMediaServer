
# set all components status to off
all_components_off(){
    #cloud tools
    export BitTorrentSync='off'
    export Syncthing='off'

    # search and automation tools
    export CouchPotato='off'
    export Headphones='off'
    export Mylar='off'
    export SickRage='off'

    # downloaders
    export NZBGet='off'
    export Sabnzbd='off'
    export Transmission='off'

    # content streaming
    export CherryMusic='off'
    export Kodi='off'
    export Plex='off'
    export TVheadend='off'
    export Ubooquity='off'
    export miniDLNA='off'

    # utilities
    export pyLoad='off'
    export HTPCManager='off'
    export Rpimonitor='off'

    # file sharing
    export NFS='off'
    export Samba='off'
    export VSFTPD='off'
}

# Discover the components available (as components/$COMPONENT.sh) and
# components that have the configuration available.
# This will become global ALL_COMPONENTS
discover_all_components() {
    trap exit_err ERR
    [ -n "$DEBUG" ] && set -x

    # file where all the components to be installed are listed in the form as
    # they are presented to the user in menu_main_choose_components
    export COMPONENTS_AVAILABLE_FILE=${COMPONENTS_AVAILABLE_FILE:-$(mktemp --tmpdir=/tmp components-XXXXXXXX)}
    unset ALL_COMPONENTS

    # find all possible components
    export COMPONENTS_DIR=${COMPONENTS_DIR:-"$(dirname ${BASH_SOURCE[0]})/../components"} 
    for filename in $COMPONENTS_DIR/*.sh;
    do
        . "$filename"
        COMPONENT=$(basename "$filename" .sh)
        ALL_COMPONENTS="$ALL_COMPONENTS ${COMPONENT}"
    done
    export ALL_COMPONENTS

    set +x
    trap - ERR
}

# Shows ok-message.
# Arguments: $0 $MESSAGE $TITLE $BACKTITLE
simple_message_dialog() {
    trap 'exit_err $LINENO common_functions simple_message_dialog' ERR

    if [ "$#" -lt 2 ];
    then
        echo 'You neeed to pass three arguments: message, title, backtitle.'
        exit 1
    else
        MESSAGE=$1
        TITLE=$2
        BACKTITLE=$3
    fi
    DIALOG=${DIALOG:-'dialog'}

    $DIALOG \
        --title "$TITLE" \
        --backtitle "$BACKTITLE" \
        --pause "$MESSAGE" \
        15 70 5
}

# inputbox that lets you change ip address (GLOBAL_MY_IP)
menu_main_ip() {
    [ -n "$DEBUG" ] && set -x

    DIALOG=${DIALOG:-'dialog'}
    GLOBAL_MY_IP=$(ip route get 8.8.8.8 | head -n 1 | awk '{ print $NF; }')

    USER_GIVEN_IP=$(\
        $DIALOG \
        --backtitle "piMediaServer -> Change ip address" \
        --title "Change ip address" \
        --inputbox "\nIp address of this machine: " \
        10 50 \
        "$GLOBAL_MY_IP" \
        3>&1 1>&2 2>&3)

    [ $? == 0 ] && export GLOBAL_MY_IP="$USER_GIVEN_IP"
    echo "menu_main_ip: GLOBAL_MY_IP=$GLOBAL_MY_IP"

    set +x
}

# inputbox that lets you change long hostname (GLOBAL_FQDN)
menu_main_fqdn() {
    [ -n "$DEBUG" ] && set -x

    DIALOG=${DIALOG:-'dialog'}
    GLOBAL_FQDN=$(hostname -f)

    USER_GIVEN_FQDN=$(\
        $DIALOG \
        --backtitle "piMediaServer --> Change FQDN" \
        --title "Change FQDN" \
        --inputbox "\nFQDN of this machine: " \
            10 50 \
            "$GLOBAL_FQDN" \
        3>&1 1>&2 2>&3)

    if [ $? == 0 ];
    then
        GLOBAL_FQDN="$USER_GIVEN_FQDN"
        HOSTNAME_SHORT=${GLOBAL_FQDN//.*/ }
    fi

    echo "menu_main_fqdn: longname=$GLOBAL_FQDN shortname: $HOSTNAME_SHORT"

    set +x
}

# inputbox that lets you change common pi group (GLOBAL_PI_GROUP)
menu_main_pi_group() {
    [ -n "$DEBUG" ] && set -x

    DIALOG=${DIALOG:-'dialog'}
    GLOBAL_PI_GROUP=${GLOBAL_PI_GROUP:-'pirate'}

    USER_GIVEN_IP=$(\
        $DIALOG \
        --backtitle "piMediaServer -> Change common pi group" \
        --title "Change common pi group" \
        --inputbox "\nEnter the name of the common pi group." \
        10 50 \
        "$GLOBAL_PI_GROUP" \
        3>&1 1>&2 2>&3)

    [ $? == 0 ] && export GLOBAL_PI_GROUP="$USER_GIVEN_IP"
    echo "menu_main_pi_group: GLOBAL_PI_GROUP=$GLOBAL_PI_GROUP"

    set +x
}

# checkbox for firewall and ssl (GLOBAL_SSL_ENABLED, GLOBAL_FIREWALL_ENABLED)
menu_main_ssl_and_f() {
    [ -n "$DEBUG" ] && set -x

    DIALOG=${DIALOG:-'dialog'}
    TEMPFILE=/tmp/test$$
    TEMPFILE=$(mktemp --tmpdir=/tmp/ $(basename $0).XXXXXXXXXX) #better
    export GLOBAL_SSL_ENABLED=${GLOBAL_SSL_ENABLED:-'off'}
    export GLOBAL_FIREWALL_ENABLED=${GLOBAL_FIREWALL_ENABLED:-'off'}

    $DIALOG \
        --backtitle "piMediaServer --> Configure firewall and ssl" \
        --title "Configure firewall and ssl" \
        --checklist \
            "Check your desire and select Done" 15 60 4 \
            "SSL" "Enable ssl for the applications." "$GLOBAL_SSL_ENABLED" \
            "Firewall" "Create ufw application rules." "$GLOBAL_FIREWALL_ENABLED" \
        2>"$TEMPFILE"

    if [ $? == 0 ];
    then
        GLOBAL_SSL_ENABLED='off'
        GLOBAL_FIREWALL_ENABLED='off'

        for choice in $(cat $TEMPFILE);
        do
            case $choice in
                SSL|'"SSL"') #dialog saves "SSL", dialog SSL
                    echo choice is SSL
                    GLOBAL_SSL_ENABLED='on'
                ;;
                Firewall|'"Firewall"')
                    echo choice is firewall
                    GLOBAL_FIREWALL_ENABLED='on'
                ;;
            esac
        done
    fi

    echo -n "menu_main_ssl_and_f: "
    echo "SSL: $GLOBAL_SSL_ENABLED firewall: $GLOBAL_FIREWALL_ENABLED"

    set +x
}

menu_main_generate_certs(){
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err $LINENO commmon-functions generate_certs' ERR
    trap 'exit_good' 0

    export GLOBAL_SSL_CERT=${GLOBAL_SSL_CERT:-"/etc/ssl/certs/$(hostname -s)-cert.pem"}
    export GLOBAL_SSL_KEY=${GLOBAL_SSL_KEY:-"/etc/ssl/certs/$(hostname -s)-key.pem"}
    DIALOG=${DIALOG:-'dialog'}

    if [ -r $GLOBAL_SSL_CERT ] || [ -r $GLOBAL_SSL_KEY ];
    then
        MESSAGE=(
            "Certificate and/or key that I would create seems to already exist."
            "Doing nothing further."
            "\n"
        )
        [ -r "$GLOBAL_SSL_CERT" ] && \
            MESSAGE+=("\n$GLOBAL_SSL_CERT exists")
        [ -r "$GLOBAL_SSL_KEY" ] && \
            MESSAGE+=("\n$GLOBAL_SSL_KEY exists")

        simple_message_dialog \
            "${MESSAGE[*]}" 'piMediaServer' 'piMediaServer -> Generate certs'

    else
        MESSAGE=(
            "Generating ssl key/certyficate pair."
            "\n\nCertificate: $GLOBAL_SSL_CERT"
            "\nKey: $GLOBAL_SSL_KEY"
        )
        generate_self_signed_certs 2>&1 | \
            $DIALOG \
                --title "Generate certificates." \
                --backtitle "piMediaServer -> Generate certs" \
                --programbox "${MESSAGE[*]}" 20 78
    fi
    exit_good
}

# main menu where all the information from user are gathered
main_menu (){
    # trap ommited because of the cancel button in dialog
    [ -n "$DEBUG" ] && set -x
    trap 'exit_err commmon-functions main_menu' ERR
    trap 'exit_good' 0

    export GLOBAL_MY_IP=$(ip route get 8.8.8.8 | head -n 1 | awk '{ print $NF; }')
    export GLOBAL_FQDN=$(hostname -f)

    export GLOBAL_SSL_ENABLED=${GLOBAL_SSL_ENABLED:-'off'}
    export GLOBAL_SSL_CERT=${GLOBAL_SSL_CERT:-"/etc/ssl/certs/$(hostname -s)-cert.pem"}
    export GLOBAL_SSL_KEY=${GLOBAL_SSL_KEY:-"/etc/ssl/certs/$(hostname -s)-key.pem"}

    export GLOBAL_FIREWALL_ENABLED=${GLOBAL_FIREWALL_ENABLED:-'off'}

    # all components get this group as secondary to make it possible to access
    # common shared storage and certificates
    export GLOBAL_PI_GROUP=${GLOBAL_PI_GROUP:-'pirate'}

    # file where all the components to be installed are listed in the form as
    # they are presented to the user in menu_main_choose_components
    export GLOBAL_COMPONENTS=${GLOBAL_COMPONENTS:-$(mktemp --tmpdir=/tmp components-XXXXXXXX)}

    DIALOG=${DIALOG:-'dialog'}
    TEMPFILE=/tmp/test$$
    TEMPFILE=$(mktemp --tmpdir=/tmp $(basename $0).XXXXXXXXXX)


    while /bin/true;
    do
        if [ -r $GLOBAL_SSL_CERT ] && [ -r $GLOBAL_SSL_KEY ];
        then
            CERTIFICATES_CREATION="Certificates exists."
        else
            CERTIFICATES_CREATION="Generate self-signed certificates."
        fi

        OPTION=$(\
        $DIALOG \
            --backtitle "piMediaServer" \
            --title "Main menu" \
            --default-item "Choose components" \
            --menu \
            'Change as necessary and choose "Install" when done.' \
            19 78 10 \
                'Change ip address'    "Current IP address is: $GLOBAL_MY_IP" \
                'Change FQDN'          "Current FQDN is: $GLOBAL_FQDN" \
                'Pi group'             "Current shared group: $GLOBAL_PI_GROUP" \
                'SSL and firewall'     "ssl ($GLOBAL_SSL_ENABLED), firewall ($GLOBAL_FIREWALL_ENABLED)" \
                    'Certificates'         "$CERTIFICATES_CREATION" \
                'Choose components'    "Change what you want to install" \
                'Install'              "" \
                3>&1 1>&2 2>&3 \
        )

        if [ $? == 0 ];
        then
            case "$OPTION" in
                'Change ip address') menu_main_ip ;;
                'Change FQDN') menu_main_fqdn ;;
                'Pi group') menu_main_pi_group ;;
                'SSL and firewall') menu_main_ssl_and_f ;;
                'Certificates') menu_main_generate_certs ;;
                'Choose components') menu_main_choose_components ;;
                'Install') break ;;
            esac
        else
            break
        fi
    done

    echo "Proceeding with the following values: "
    echo "IP: $GLOBAL_MY_IP"
    echo "FQDN long: $GLOBAL_FQDN short: ${GLOBAL_FQDN//.*/}"
    echo "SSL: $GLOBAL_SSL_ENABLED"
    echo "FIREWALL: $GLOBAL_FIREWALL_ENABLED"
    echo "Components: "
    cat "$GLOBAL_COMPONENTS"

    exit_good
}

# checkbox to choose installed components
menu_main_choose_components() {
    #source components/*.sh and fill in ALL_COMPONENTS variable
    discover_all_components

    [ -n "$DEBUG" ] && set -x


    # file where all the components to be installed are listed in the form as
    # they are presented to the user in menu_main_choose_components
    export GLOBAL_COMPONENTS=${GLOBAL_COMPONENTS:-$(mktemp --tmpdir=/tmp components-XXXXXXXX)}

    DIALOG=${DIALOG:-'dialog'}

    # set the components that are in the file to on the rest to off
    #all_components_off
    #while read choice; do eval "$choice"='on'; done < "$GLOBAL_COMPONENTS"

    # create the menu from available components
    # http://mywiki.wooledge.org/BashFAQ/040
    unset MENU_OF_COMPONENTS
    for component in $ALL_COMPONENTS;
    do
        description=' '
        declare -F ${component}_description >/dev/null &&
            description=$(${component}_description)

        status='off'
        for c in $(cat $GLOBAL_COMPONENTS);
        do
            [ "$c" == "$component" ] && { status='on'; break; }
        done

        MENU_OF_COMPONENTS+=($component "$description" $status)
    done

    $DIALOG \
        --backtitle "piMediaServer --> Choose components" \
        --title "Components chooser" \
        --separate-output \
        --checklist \
        "\nChoose what you want to install:" 19 78 9 \
        "${MENU_OF_COMPONENTS[@]}" \
        2>"$GLOBAL_COMPONENTS"
#    "BitTorrentSync" "Personal cloud" "$BitTorrentSync" \
#    "Syncthing" "Personal cloud" "$Syncthing" \
#    \
#    "CouchPotato" "Video Automation Finder" "$CouchPotato" \
#    "Headphones" "Music Automation Finder" "$Headphones" \
#    "Mylar" "Comic Automation Finder" "$Mylar" \
#    "SickRage" "Python Show Automation Finder" "$SickRage" \
#    "Sonarr" ".NET Show Automation Finder" "$Sonarr" \
#    \
#    "NZBGet" "Usenet Downloader written in C++" "$NZBGet" \
#    "Sabnzbd" "Usenet Downloader written in Python" "$Sabnzbd" \
#    "Transmission" "Torrent downloading" "$Transmission" \
#    "Deluge" "Torrent downloading" "$Transmission" \
#    \
#    "CherryMusic" "Personal Grooveshark Server" "$CherryMusic" \
#    "Kodi" "Raspberry Pi only        " "$Kodi" \
#    "Plex" "Plex Media Server        " "$Plex" \
#    "TVheadend" "TV streaming / proxy" "$TVheadend" \
#    "Ubooquity" "eBook Management" "$Ubooquity" \
#    "miniDLNA" "ReadyMedia miniDLNA 1.1.4" "$miniDLNA" \
#    \
#    "HTPCManager" "HTPC Management system" "$HTPCManager" \
#    "Rpimonitor" "Status page and statistics" "$Rpimonitor" \
#    "pyLoad" "Online locker downloader" "$pyLoad" \
#    \
#    "NFS" "Windows not compatible file sharing." "$NFS" \
#    "Samba" "Windows compatible file sharing." "$Samba" \
#    "VSFTPD" "Le ftp sharing" "$VSFTPD" \

    while read choice; do eval "$choice"='on'; done < "$GLOBAL_COMPONENTS"

    set +x
}

# show menu for all the components
menu_main_configure_components() {
    # trap ommited because of the cancel button in dialog

    #source all components, exports ALL_COMPONENTS var
    discover_all_components

    [ -n "$DEBUG" ] && set -x
    DIALOG=${DIALOG:-'dialog'}

    # http://mywiki.wooledge.org/BashFAQ/040
    unset menu_components
    for component in $ALL_COMPONENTS;
    do
        declare -F ${component}_menu_main >/dev/null && \
            menu_components=($menu_components $component "")
    done

    if [ -z "$menu_components" ];
    then
        MESSAGE=(
            "There is no configurable component. For the component to be"
            "recognized as configurble there must be bash defined with"
            "name '\$COMPONENT_menu_main'."
        )
        $DIALOG \
            --backtitle "piMediaServer --> Components configuration" \
            --title "Components configuration menu" \
            --msgbox "${MESSAGE[*]}" \
            10 58
    else
        TEMPFILE=/tmp/test$$
        TEMPFILE=$(mktemp --tmpdir=/tmp $(basename $0).XXXXX)
        while /bin/true;
        do
            $DIALOG \
                --backtitle "piMediaServer --> Components configuration" \
                --title "Components configuration menu" \
                --menu "Only components that are configurable are shown." \
                20 38 9 \
                "Done" "" \
                "${menu_components[@]}" \
                2>"$TEMPFILE"

            if [ $? == 0 ];
            then
                OPTION="$(cat $TEMPFILE)"
                case "$OPTION" in
                    'Done')
                        break # out of while
                    ;;
                    *)
                        #run $OPTION_menu
                        "$OPTION"_menu_main
                    ;;
                esac
            else
                break #out of while loop
            fi
        done
    fi

    rm -f "$TEMPFILE"
}
