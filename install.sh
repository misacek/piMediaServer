#!/bin/bash
#
# Debian pi Media Server
# original author and idea: Igor Pecovnik
#
[ -n "$DEBUG" ] && set -x

help(){
    echo "Usage:"
    echo "    -d, --debug: set '-x' to the scrpipt"
    echo "    -n, --non-interactive: skip main menu"
    echo "    -g, --generate-ssl-keys: generate self-signed ssl keys"
    echo "    -p, --parallel: run installation of components in parallel"
    echo "    -h, --help: this screen"
    echo
    exit
}

process_commandline(){
    trap 'exit_err $LINENO install.sh process_commandlin' ERR

    LONG_OPTS=(
        debug
        generate-ssl-keys
        non-interactive
        parallel
    )
    IFS=, eval 'OPTIONS="${LONG_OPTS[*]}"'
    TEMP=$(getopt -o ndgh --long ${OPTIONS} -- "$@")
    eval set -- "$TEMP"

    unset DEBUG GENERATE_CERTS NON_INTERACTIVE PARALLEL
    while true; do
      case "$1" in
        --non-interactive | -n)
            export NON_INTERACTIVE=1;
            shift
        ;;
        --debug | -d)
            export DEBUG=1;
            shift
        ;;
        --generate-ssl-keys | -g)
            export GENERATE_CERTS=1
            shift
        ;;
        --parallel | -p)
            export PARALLEL=1
            shift
        ;;
        -h )
            help
        ;;
        -- )
            shift
            break
        ;;
        *)
            break
        ;;
      esac
    done
}

MY_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")") #full path to this dir
source "$MY_DIR/components/global.env"
[ -f $HOME/.pienv ] && source $HOME/.pienv

source "$MY_DIR/functions/common-functions.sh"
for c in ${MY_DIR}/components/*.sh; do source $c; done

trap 'exit_err $LINENO install.sh __main__' ERR # trap right after we source exit_err

[ -n "$1" ] && process_commandline "$@"

LOGZ_DIR=${LOGZ_DIR:-'/tmp/piMediaServer'}
[ -d "$LOGZ_DIR" ] || mkdir -p "$LOGZ_DIR"

[ "$USER" == "root" ] || { echo 'You must be root.'; exit 1;  }
[ -f /etc/debian_version ] || { echo 'This script works on debian only.'; exit 1; }
[ -f /run/ramlog.lock ] && { echo 'Disable ramlog first.'; exit 1; }

make_fqdn_work
log_result "install: make_fqdn_work correctly finished"
update_check
log_result "install: update_check finished"

[ -z "$NON_INTERACTIVE" ] && main_menu

[ -n "$DEBUG" ] && set -x
[ -n "$GENERATE_CERTS" ] && generate_self_signed_certs

PACKAGES_TO_INSTALL=()
if [ -f "$GLOBAL_COMPONENTS" ];
then
    COMPONENTS=() #array of chosen components
    while read component;
    do
        [ -z "$component" ] && continue #skip empty lines

        COMPONENTS+=($component)
        # for each component set log, progress and rv file
        eval ${component}_LOGF="$LOGZ_DIR/$component-log"; logvar="${component}_LOGF"
        export ${logvar}
        eval ${component}_PROGRESSF="$LOGZ_DIR/$component-progress"; progressvar="${component}_PROGRESSF"
        export ${progressvar}
        eval ${component}_RVF="$LOGZ_DIR/$component-rv"; rvvar="${component}_RVF"
        export ${rvvar}
        ECHO=(
            "$component "
            "\n\tlogfile: ${!logvar}"
            "\n\tprogress file: ${!progressvar}"
            "\n\trv file: ${!rvvar}"
        )
        echo -e ${ECHO[*]}

        PACKAGES_TO_INSTALL+=($(eval ${component}_packages))
    done < <(sort "$GLOBAL_COMPONENTS" | uniq)
    if [ ${#COMPONENTS[@]} -lt 0 ];
    then
       echo "No components present in $GLOBAL_COMPONENTS file, cannot continue."
       exit 1
    fi
else
    echo "There is no $GLOBAL_COMPONENTS file, cannot continue."
    exit 1
fi

# install packages needed for all components
IFS=$'\n' SORTED_PACKAGES=($(sort <<<"${PACKAGES_TO_INSTALL[*]}"))
unset IFS

if [ ${#SORTED_PACKAGES[@]} -gt 0 ];
then
    apt_get_safe update
    apt_get_safe -y install ${SORTED_PACKAGES[@]}
fi
log_result "install: installed packages ${SORTED_PACKAGES[@]}"

PIDS=() #components[0]='abdc' -> pid of 'abcd' = pids[0]

I=0
RESULTS=()
GENERAL_RESULT='good'
for COMPONENT in ${COMPONENTS[*]};
do
    if [ -n "$PARALLEL" ];
    then
        # run in background
        ${COMPONENT}_install &>$LOGZ_DIR/${COMPONENT}-install-output &
        PIDS+=($!)
    else
        # run in foreground
        echo -n "Waiting for ${COMPONENT} to finish... "
        ${COMPONENT}_install &>$LOGZ_DIR/${COMPONENT}-install-output || :

        rvvar="${COMPONENT}_RVF"
        if [ ! -f "${!rvvar}" ];
        then
            GENERAL_RESULT='bad' #file does not exist
        else
            if [ "$(cat ${!rvvar})" -ne "3" ];
            then
                GENERAL_RESULT='bad'
                logvar="${COMPONENT}_LOGF"
                log_result "install: ${COMPONENT} installation failed. logfile: ${!logvar}"
            fi
        fi

        I=$((I+1))
    fi
done
# wait for all processes running in background
if [ -n "$PARALLEL" ];
then
    echo "Running installation of ${COMPONENTS[*]} in parallel."
    for pid in ${PIDS[*]};
    do
        echo -n "Waiting for ${COMPONENTS[$I]} to finish... "
        wait $pid || : #ignore because it migt have 127 if already finished

        rvvar="${COMPONENTS[$I]}_RVF"
        if [ ! -f "${!rvvar}" ];
        then
            RESULTS[$I]=0 #failure
            GENERAL_RESULT='bad'
        else
            if [ "$(cat ${!rvvar})" -eq "3" ];
            then
                RESULTS[$I]='good'
            else
                RESULTS[$I]='bad'
                GENERAL_RESULT='bad'
            fi
        fi

        I=$((I+1))
    done

    I=$((I-1)) # array is indexed from 0
fi

if [ "$GENERAL_RESULT" == "good" ];
then
    log_result "install final status: SUCCESS"
else
    if [ -n "$PARALLEL" ];
    then # we want to list what succeded and what failed
        echo $I
        while [ $I -ge 0 ];
        do
            if [ "${RESULTS[$I]}" == 'bad' ];
            then
                logvar="${COMPONENTS[$I]}_LOGF"
                log_result "install: ${COMPONENTS[$I]} installation failed. logfile: ${!logvar}"
            else
                log_result "install: ${COMPONENTS[$I]} installation successful."
            fi
            I=$((I-1))
        done
    fi
    log_result "install final status: FAILURE"
fi

[ -f ${LOGZ_DIR}/result-log ] && cat ${LOGZ_DIR}/result-log
