
.DEFAULT_GOAL := all
MYSELF := piMediaServer

ifndef IMAGE_NAME
	IMAGE_NAME := pimediaserver-test
endif

ifndef IMAGE_TAG
	IMAGE_TAG := jessie
endif

ALL_CONTAINER_IDS=$(shell sudo docker ps -a -q)

help:
	@echo "Possible targets: test, image, start, test, stop, run-command"
	@echo
	@echo "Env variables:"
	@echo "	IMAGE_NAME ($(IMAGE_NAME))"
	@echo "	IMAGE_TAG ($(IMAGE_TAG))"

test: image start-image run-command
	@echo $(shell date) $(MYSELF): success: $@ target

image: docker-image
docker-image:
	@sudo docker build --tag $(IMAGE_NAME):$(IMAGE_TAG) .
	@echo $(shell date) $(MYSELF): success: $@ target

start: start-image
start-image: stop
	@mkdir -p docker-logz
	@sudo docker run \
		--env 'container=docker' \
		--privileged=true \
		--security-opt seccomp:unconfined \
		--cap-add=SYS_ADMIN \
		-v /sys/fs/cgroup:/sys/fs/cgroup \
		-v $(shell pwd):/piMediaServer \
		-v $(shell pwd)/docker-logz:/logz \
		--entrypoint /sbin/init \
		--name $(IMAGE_NAME) \
		--detach \
		$(IMAGE_NAME):$(IMAGE_TAG)
	@echo $(shell date) $(MYSELF): success: $@ target

# remove all running containers
stop: stop-image
stop-image:
	[ -n "$(ALL_CONTAINER_IDS)" ] && sudo docker rm -f $(ALL_CONTAINER_IDS) || :
	@echo $(shell date) $(MYSELF): success: $@ target
.PHONY: stop stop-image

run-command:
	@sudo docker exec $(IMAGE_NAME) /piMediaServer/docker-entrypoint.sh
	@echo $(shell date) $(MYSELF): success: $@ target
