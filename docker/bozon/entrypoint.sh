#!/bin/bash -x

VOLUMES=(/BoZoN-master)

NEW_UID=${NEW_UID:-'1000'}
NEW_GID=${NEW_GID:-'1000'}
OLD_NAME=${OLD_NAME:-'www-data'}
OLD_GROUP=${OLD_GROUP:-'www-data'}
OLD_UID=${OLD_UID:-$(id --user ${OLD_NAME})}
OLD_GID=${OLD_GID:-$(id --group ${OLD_GROUP})}


usermod -u ${NEW_UID} ${OLD_NAME}
groupmod -g ${NEW_GID} ${OLD_GROUP}
find / -type d \( -path /sys -o -path /proc \) -prune -o -user ${OLD_UID} -exec chown -h ${NEW_UID} {} \;
find / -type d \( -path /sys -o -path /proc \) -prune -o -user ${OLD_GID} -exec chgrp -h ${NEW_GID} {} \;

exec "$@"
