
FROM debian:8
MAINTAINER Michal Novacek <michal.novacek@gmail.com>

# adds metadata to image, key value pairs
#LABEL a=b \
#   b=c

ARG PACKAGES="\
    apt-transport-https \
    apt-utils \
    curl \
    debconf-utils \
    dialog \
    dnsutils \
    git \
    locales \
    net-tools \
    software-properties-common \
    sudo \
    unzip \
    wget \
"
#
#
#

RUN apt-get update
RUN apt-get -y --no-install-recommends install $PACKAGES
RUN rm -rf /var/lib/apt/lists/*

# set locales
RUN /usr/sbin/locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LC_ALL en_US.UTF-8

RUN echo 'root:password' | chpasswd

# local repository should be mounted here
VOLUME /piMediaServer
VOLUME /logz

# couchpotato:5050
# samba:800
# sickgear:8081
# syncthing:8384
EXPOSE 5050 800 8081 8384

WORKDIR /piMediaServer
