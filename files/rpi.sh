#!/bin/bash -x
trap exit ERR

# does all you need to start with piMediaServer on raspbian lite (jessie)

PACKAGES=(
    cryptsetup
    git
    htop
    iptraf
    lvm2
    mc
    vim
    rdiff-backup
    screen
    screen 
)
sudo apt-get -y install ${PACKAGES[*]}

sudo echo 'en_US.UTF-8' > /etc/locale.gen
sudo locale-gen
sudo dpkg-reconfigure --frontend=noninteractive locales
sudo update-locale LC_ALL='en_US.UTF-8'
sudo update-locale LC_LANG='en_US.UTF-8'
sudo update-locale LANGUAGE='en_US.UTF-8'

git clone git@gitlab.com:misacek/piMediaServer.git ~pi/piMediaServer
cd ~pi/piMediaServer
git checkout jessie-devel
