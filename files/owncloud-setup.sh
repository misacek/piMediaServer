#!/bin/bash -x
OCC='su - www-data -s /bin/bash -c /var/www/owncloud/occ'

OCC_STORAGE_CONFIG=${OCC_STORAGE_CONFIG:-'owncloud-storage-config'}

OC_USER=${OC_USER:-'misacek'}
OC_PASS=${OC_PASS:-'password'}
OC_GROUP=${OC_GROUP:-'rodinka'}

# normal user(s) and put into group
$OCC user:add --password-from-env --group ${OC_GROUP} ${OC_USER}

# enable external storage
$OCC app:enable files_external

# configure external storage
$OCC files_external:import ${OCC_STORAGE_CONFIG}


