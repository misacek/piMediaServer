function disk_space() {
        var oRequest = new XMLHttpRequest();
        var sURL  = "https://router.misackovo.eu:55443/transmission/web/df-h.txt";

        var currentTime = new Date();
        var hr = currentTime.getHours();
        var min = currentTime.getMinutes();
        // var sec = currentTime.getSeconds();

        oRequest.open("GET",sURL + '?nocache=' + currentTime.getTime(),false); // make a unique URL to disable browser side caching
        oRequest.setRequestHeader("User-Agent",navigator.userAgent);
        oRequest.send(null);

        document.getElementById('disk_space_container').innerHTML = '|Free: ' + oRequest.responseText + ' @' + hr + ':' + min;      
}

// execute disk_space after whole page with our disk_space_container has been loaded
window.onload = disk_space; 

// refresh free disk space every 5 minutes
setInterval(disk_space, 300000); 
