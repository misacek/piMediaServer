#!/bin/bash
#
# Download fresh ubuntu-14.04 from the cloud and boots it locally with known ip
# address and our ssh key. Password for user ubuntu is password.
#
#http://mojodna.net/2014/05/14/kvm-libvirt-and-ubuntu-14-04.html
#https://goo.gl/8U9E1

function trap_exit(){
    echo "error trapped, rerun with DEBUG=1 $0 to see what happened"
    exit
}

function download_image() {
    trap trap_exit ERR

    if [ ! -f "$POOL/$IMAGE" ];
    then
        wget -q -O "$POOL/$IMAGE" \
            http://cloud-images.ubuntu.com/trusty/current/trusty-server-cloudimg-amd64-disk1.img
        # make the original image bigged
        qemu-img resize "$POOL/$IMAGE" +30G
        virsh pool-refresh default
    else
        echo "Not downloading $POOL/$IMAGE as it exists already"
    fi
}

function create_iso() {
    trap trap_exit ERR

    cat <<EOF > /tmp/meta-data
instance-id: iid-local01;
local-hostname: ubuntu-14-04
EOF

    cat <<EOF > /tmp/user-data
#cloud-config
password: password
chpasswd: { expire: False }
ssh_pwauth: True

packages:
  - git
  - mc

ssh_authorized_keys:
  - ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAgEA1VrDJH83PbJNHibTsuxMKk4RwxFRUkt0AUse0xaf7tLOOqUumWBe3ri/AVzJgB499z2Q03hEXeWwPasW4zn/YyIwRKKwO9McWR1hpRdttdaFjov12ex0EgcmLJuiQpIfev1QuoTQk+4BOTP1SXZXqyaZ0wSiSwLxgF/rzlbQrwP2JwcDa/9p/+4TkxG/UxbOorcfAoMw4AM43dJO4/v3STx/OWQ7E2sqlBvxDE7VrpFjMizcimbmj9VlpjzaK1pkMKdedQfwPMgJ0Loe54PySEkUWTT7vwoWeahAMWlXQziZ+C4QCesvhZEJL7Qjr1zW4WBWUMXEy2DO4YMJeJ8EdwayuIaULOP1NQuLuNRDR8AVIShkIZkV5NZk/4vZqDM8WpeQ91PLfrV3pXuxVXxjT/lIkVYRMG4ZsK7RmWtU9O2NB9Iy8uVJ4GWYZqsllQymYv2fF3xJ9Smh58VUfZa1/qzyFtqqD8ZrTlPjuJtiquCv3Zrj9D2AEkBXduyMOMYhi9MNTx6IGHn+eaRPIFceuCIymtho1XtnBpFEdrtIXFe68bTqIMEaSht3CbyRKAdnou2KDBQ5hVqk26yqpO9Y/r3WDfFXDX6KGFhuFHNI3Kzz2uNgdzvmAI9yt3NXfbE9MELaWeZc1kpeJr8mEbV+T8fIh5jRjdkpX88bOSYcJiU= klic-na-vsechno-rsa

ssh_keys:
  dsa_private: |
    -----BEGIN DSA PRIVATE KEY-----
    MIIBugIBAAKBgQCFOen3qE2/d71juX6IHffumcc08VuqUvnZUkxkYASIOmwpfAfH
    TGJ5+jenIeR6tbodCJAwFp3IPyNlql6ufsYnDObvZPTXKa+JiKwgpUk0CJb2O5aF
    qWTLzYG8dGeG3+kZyQU5pEqARvu1ZQCXJshCCEbJZrNs+XeBJb5TSHsK8wIVAL4a
    ysilfv9OWUkto2Pxu8hHibcNAoGAKEruxRLd4SpWj7nxlTofir0iHZBCjyFHfugf
    VVu9jeTEqSqyuzRv7VyD0ywvTdcdso86+druyauDpHU4wRF8H5ihhR4+afJKQsl8
    ZKdQ4ySHnG4dPefuYsIcrRANq7N0SW6XwFubGphfHVaUYlxRpWcWT0LCGtot7aZL
    QS3xh6ECgYAQo9ZO6DXgmITzaOXAdWZGQpSF9xz8c97yvfWzVThg//uHcj9MYYJs
    qdq4GWmccgDijrE+Ad7qrPur2oJFmdrFC9+5org+70B2mQ1izY9C851R5warr6X0
    nOxcA7m7Wu2yHioAv2UBwlhWTZH4iPTEKQu9Fysk1lU3hgyOvhWWmQIUfqyHPET/
    avHuQWBnhTfd60wXGW8=
    -----END DSA PRIVATE KEY-----

  dsa_public: ssh-dss AAAAB3NzaC1kc3MAAACBAIU56feoTb93vWO5fogd9+6ZxzTxW6pS+dlSTGRgBIg6bCl8B8dMYnn6N6ch5Hq1uh0IkDAWncg/I2WqXq5+xicM5u9k9Ncpr4mIrCClSTQIlvY7loWpZMvNgbx0Z4bf6RnJBTmkSoBG+7VlAJcmyEIIRslms2z5d4ElvlNIewrzAAAAFQC+GsrIpX7/TllJLaNj8bvIR4m3DQAAAIAoSu7FEt3hKlaPufGVOh+KvSIdkEKPIUd+6B9VW72N5MSpKrK7NG/tXIPTLC9N1x2yjzr52u7Jq4OkdTjBEXwfmKGFHj5p8kpCyXxkp1DjJIecbh095+5iwhytEA2rs3RJbpfAW5samF8dVpRiXFGlZxZPQsIa2i3tpktBLfGHoQAAAIAQo9ZO6DXgmITzaOXAdWZGQpSF9xz8c97yvfWzVThg//uHcj9MYYJsqdq4GWmccgDijrE+Ad7qrPur2oJFmdrFC9+5org+70B2mQ1izY9C851R5warr6X0nOxcA7m7Wu2yHioAv2UBwlhWTZH4iPTEKQu9Fysk1lU3hgyOvhWWmQ== root@ubuntu-14-04

  rsa_private: |
    -----BEGIN RSA PRIVATE KEY-----
    MIIEowIBAAKCAQEAwmYDxHfZ2tpjd66D0jrDzerqeM9IFyVNNf+Z0kCdl74GuDNu
    oMOf+XO7hH11cZJ0zsptboMDXM2Mix5T/tf+pUF1fE2JWuh1BTclytz9H8bCK64/
    7zpxL++VaKoF2b6vSl8jxO5N2PrJf6Jge0EmvHDApjck+2K/7v4ZMlq28B4hzPn7
    qhi82iPBBbxE2uPPrdOoaOyamQAC/5MzkRlPOIc08OJTNAFFz61qKDmN4M03o0cE
    U5VAqEqRspOyo9X3nSmydsSXq4iAkYdrty+l44Y9bpQ08ZRFj252sXXw2AWkf84Q
    BWpIKC5qSPimSaY5ASsveT8Af240CJicjP/cNQIDAQABAoIBAAZ0XKupTxJ1LdhL
    cxUH3nh31FEukLY0BtxEIFdazeC77n0vN/0egbFGpnM/tyaONlKnZfrFXtc1+/Si
    jSdpBIt4IO5/pI/gKAKEUjzqGX10m/3XS0gmKJvu2IchHhCcxDUz83wWbX9G1Gno
    ekudXivjkoHyUsgF/1lwFmYVnP1b8+kZGI6hBfG/SvknlG6frudRqrjZhPGeqaX/
    GwFg2AJm1jYpt+5Qc/6e3R4cQ8ba5XGkABbi0s1iPSUIAb3W9koxddI4vIBEQdNR
    Gi9KF6R4ksjqKQtBYuER4QNzZA7jOxlaHmik5bop0ZeFHQ7VVvqsFxeMkTtfiVW2
    59gi1gECgYEA778OnswYza5fU+MzG6eyNd8IjchG/SUEDZobCeBoXr0lN4Ta6ahb
    b4Sry5zrjyFlk+emopcioPcez/fQ4JBNMQjsq6hBbckrON7vTjb9gODHkDxm2Z6B
    0RSfFjlrjM+NE+QpaafbNpeUGWaF/8Ug/Xx3VaZj20gRpVVYN5k1B58CgYEAz5Pr
    KwRm7PZx/RUPF4dqXL+q3SetZ2/LkvdDVs9MEfuEvcZj3zBO+iqriE/ZETRET284
    pho5A26yDCsYnK6+4MvMFW6um6YKNd2LO5KEJw/lWjKJXJHYUk70x18NRaW7J5kl
    noLRJH+3DPZdZ++IUDkMHSkDhioYPSQqF+N6G6sCgYARmmmsZzf7Ai6lriz2ft2q
    7vFl16q+s5UUmpgZpYyESEcPSIX6Wg48pfUnQiJZpA4dSgkGLyqvvvVYN83ybyX7
    z7fw8q27LeYqitPvXkJG0DpW8NhsaiMGAwmbTCQFdsfZCguwYk7KN178bnawTP0b
    H6RzeYAjfasuBJIxouIt6QKBgQCRSSi6LRSVGlDiIFE12LPbQtOmgtTdpuPHc4UQ
    R3sowvA/m5HfbwJ4GlGA0NJPNlPxfRkbORNakanQ1oEdeTlkQYAQKC3SBz7yQKPp
    CD2y0W9n2CNIRVQxF84/lQnHC56Yspof/dPARhuq2Oy7iG3SKHuQFVrAFspsUNmd
    arvWjQKBgCEqEYthBfg9gEPoKxUSUSjz+9FUYb69/v7bFYdThSWJSF1jtDTbIfIg
    Vu1e+sS3TUpiwuxl51GQmmMN896pBTg11h48QDv7TRGqGRmkebfwt0qotyzloiuU
    h5vOIS8Hc5CKvBYIlYd21OHLpmBtx5l8q/CqPdSJ7UtsXzLIdJwH
    -----END RSA PRIVATE KEY-----

  rsa_public: ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDCZgPEd9na2mN3roPSOsPN6up4z0gXJU01/5nSQJ2Xvga4M26gw5/5c7uEfXVxknTOym1ugwNczYyLHlP+1/6lQXV8TYla6HUFNyXK3P0fxsIrrj/vOnEv75VoqgXZvq9KXyPE7k3Y+sl/omB7QSa8cMCmNyT7Yr/u/hkyWrbwHiHM+fuqGLzaI8EFvETa48+t06ho7JqZAAL/kzORGU84hzTw4lM0AUXPrWooOY3gzTejRwRTlUCoSpGyk7Kj1fedKbJ2xJeriICRh2u3L6Xjhj1ulDTxlEWPbnaxdfDYBaR/zhAFakgoLmpI+KZJpjkBKy95PwB/bjQImJyM/9w1 root@ubuntu-14-04

EOF

    rm -f $POOL/configuration.iso
    genisoimage \
        -input-charset utf-8 \
        -output $POOL/configuration.iso \
        -volid cidata -joliet -rock \
        /tmp/user-data /tmp/meta-data &>/dev/null
    virsh pool-refresh default &>/dev/null
}

function virt_install() {
    trap trap_exit ERR

    rm -f "$IMAGE_DELTA"

    [ -f "$POOL/$IMAGE_DELTA" ] && rm -f "$POOL/$IMAGE_DELTA"
    virsh pool-refresh default
    qemu-img create -f qcow2 -b "$POOL/$IMAGE" "$POOL/$IMAGE_DELTA"
    virsh pool-refresh default

    if virsh dominfo "$1" &>/dev/null;
    then
        virsh destroy "$1" || : # ignore error if not running
        virsh undefine "$1"
    fi
    virt-install -r 1024 \
      -n "$1" \
      --network=network=default,mac=00:16:3e:ac:69:32 \
      --vcpus=2 \
      --autostart \
      --memballoon virtio \
      --boot hd \
      --disk vol=default/$IMAGE_DELTA,format=qcow2,bus=virtio \
      --disk vol=default/configuration.iso,bus=virtio
}

function make_it_comfy() {
    trap trap_exit ERR

    # wait 1min for the machine to come up
    while /bin/true;
    do
        if su q -c "ssh -q ubuntu@ubuntu hostname";
        then
            SUCCESS=1
            break
        else
            sleep 1
        fi
    done

    if [ -n "$SUCCESS" ];
    then
        su "$USER_WITH_SSH" -c "tar -C ~q -cf - .homesick | ssh -q ubuntu@ubuntu 'tar xf -'"
        su "$USER_WITH_SSH" -c "ssh -q ubuntu@ubuntu 'echo source .homesick/repos/homeshick/homeshick.sh > ~/.bashrc'"
        su "$USER_WITH_SSH" -c "ssh -q ubuntu@ubuntu 'homeshick link -f'"
        su "$USER_WITH_SSH" -c "ssh -q ubuntu@ubuntu 'ln -s ~/.homesick/repos ~/git'"
    fi
}

trap trap_exit ERR
[ -n "$DEBUG" ] && set -x

export VIRT_NAME=${VIRT_NAME:-'ubuntu-14.04'}
export POOL=/var/lib/libvirt/images
export IMAGE=trusty-server-cloudimg-amd64.img.orig
export IMAGE_DELTA=trusty-server-cloudimg-amd64.img-delta
export USER_WITH_SSH=${USER_WITH_SSH:-'q'}

case $1 in
    download)
        download_image
    ;;
    iso)
        create_iso
    ;;
    virt-install)
        virt_install "${VIRT_NAME}"
    ;;
    comfy)
        make_it_comfy
    ;;
    all)
        download_image
        create_iso
        virt_install "${VIRT_NAME}"
        make_it_comfy
    ;;
    *)
        echo "use $0 download|iso|virt-install|comfy|all"
    ;;
esac
