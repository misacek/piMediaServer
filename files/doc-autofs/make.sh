#!/bin/bash -x
rst2html \
    autofs.magic.rst \
    index.html \
    --stylesheet-path=stylesheets/nature.css \
    --link-stylesheet
