#!/bin/bash -x
export COMPONENTS=${COMPONENTS:-'Syncthing'}
export COMPONENTS_FILE=${COMPONENTS_FILE:-'/tmp/piMediaServer/components-chosen'}
export LOGDIR=/logz

# create file with components that will be installed
mkdir -p $(dirname ${COMPONENTS_FILE})
echo > ${COMPONENTS_FILE}
for c in $COMPONENTS; do echo $c >> ${COMPONENTS_FILE}; done

# run, it yay
sudo /piMediaServer/install.sh -dn
